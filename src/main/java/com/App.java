package com;

import com.las.annotation.EnableMirai;
import com.las.core.Bot;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动入口
 *
 * @author dullwolf
 */
//@EnableCQ
@EnableMirai(superQQ = "2547170055", botQQ = "1091569752", keyAuth = "dw0123456789")
@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
        Bot.run(App.class);
    }
}
