package com.las.nogu.commond;


import com.utils.PropertiesUtil;

public class Constant {

    /**
     * osu API临时会话存放最大时间
     */
    public static int MAX_SESSION_TIME = 24 * 60 * 60;
    public static int MIN_SESSION_TIME = 12 * 60 * 60;


    /**
     * osu授权登录以及一些标记
     */
    public static final String OSU_AUTH_TAG = "OSU:AUTH:TAG:";
    public static final String OSU_AUTH2_TAG = "OSU:AUTH2:TAG:";// 这个是客户端凭证 具体看API V2
    public static String OSU_LOGIN_TAG = "OSU:LOGIN:";
    public static String OSU_USER_ALLMODE = "OSU:USER:ALLMODE:";

    /**
     * session标记
     */
    public static final String MY_SESSION = "mySessionId";
    public static final String USER_INFO = "userInfo";

    public static String GPT_SESSION = "chat:gpt:session3";


    /**
     * osu授权web配置
     */
    public static final String CLIENT_ID = PropertiesUtil.readKey("CLIENT_ID");
    public static final String API_SERVER_KEY = PropertiesUtil.readKey("API_SERVER_KEY");
    public static final String API_AUTH_URL = PropertiesUtil.readKey("API_AUTH_URL");




}
