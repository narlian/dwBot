package com.las.nogu.controller.search;


import com.las.nogu.controller.BaseController;
import com.utils.osu.OsuApi2;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/search")
public class SearchController extends BaseController {

    private final Log log = LogFactory.getLog(SearchController.class);

    @RequestMapping("/osumap")
    public String osumap(HttpSession session, HttpServletRequest request) {
        Map<String, String> paraMap = getParaMap(request, true);
        StringBuilder filters = new StringBuilder();
        paraMap.forEach((k,v) -> filters.append(k).append("=").append(v).append("&"));
        log.info("搜索OSU歌谱参数：" + filters.toString());
        String result = OsuApi2.getInstanceByToken(getToken(session)).getBeatmaps(filters.toString());
        log.info("搜索结果：" + result);
        return result;
    }

}
