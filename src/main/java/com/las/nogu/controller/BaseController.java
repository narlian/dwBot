package com.las.nogu.controller;

import com.alibaba.fastjson.JSONObject;
import com.las.nogu.commond.Constant;
import com.utils.RedisUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BaseController {

    /**
     * 认证token
     *
     * @param session
     * @return
     */
    protected String getAuthToken(HttpSession session) {
        String sId = session.getAttribute(Constant.MY_SESSION).toString();
        return RedisUtils.get(Constant.OSU_AUTH_TAG + sId).toString();
    }

    /**
     * 客户端资源token
     *
     * @param session
     * @return
     */
    protected String getToken(HttpSession session) {
        String sId = session.getAttribute(Constant.MY_SESSION).toString();
        return RedisUtils.get(Constant.OSU_AUTH2_TAG + sId).toString();
    }

    /**
     * 获取用户OSU的ID
     *
     * @param session
     * @return
     */
    protected String getOsuUserId(HttpSession session) {
        Object o = session.getAttribute(Constant.USER_INFO);
        if (null != o) {
            String jsonStr = o.toString();
            JSONObject osuObj = JSONObject.parseObject(jsonStr);
            return osuObj.getString("id");
        } else {
            return "-1";
        }
    }

    protected Map<String, String> getParaMap(HttpServletRequest request,boolean isOne) {
        Map<String, String[]> map = request.getParameterMap();
        Map<String, String> dataMap = new HashMap<>();
        Set<String> set = map.keySet();
        for (String key : set) {
            String[] values = map.get(key);
            if (values.length > 1) {
                //说明里面至少有两个，讲数组内容变成字符串存进去 or 默认取第一个，看isOne状态
                if (isOne) {
                    dataMap.put(key, values[0]);
                } else {
                    dataMap.put(key, Arrays.toString(values));
                }
            } else {
                //否则就是 <= 1，但是这里不可能为0个，直接put即可
                dataMap.put(key, values[0]);
            }

        }
        return dataMap;
    }

}
