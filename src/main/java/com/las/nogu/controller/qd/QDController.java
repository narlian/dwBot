package com.las.nogu.controller.qd;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.las.bot.dao.QdFeedBackDao;
import com.las.bot.dao.QdScheduleDao;
import com.las.bot.dao.SovitsDao;
import com.model.QdFeedBack;
import com.model.QdSchedule;
import com.model.SovitsSetting;
import com.utils.HttpUtils;
import com.utils.IPUtil;
import com.utils.JsonUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/qd")
public class QDController {

    private final Log logger = LogFactory.getLog(QDController.class);

    @Autowired
    private SovitsDao sovitsDao;

    @Autowired
    private QdScheduleDao qdScheduleDao;

    @Autowired
    private QdFeedBackDao qdFeedBackDao;


    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping("/gpt")
    public String gpt(String ruleType, String command, String roleText) {
        try {
            String ruleStr = "";
            SovitsSetting setting = sovitsDao.findByName("gpt");
            SovitsSetting rule = sovitsDao.findByName(ruleType);
            if (ObjectUtil.isNotNull(rule)) {
                if ("custom".equals(ruleType)) {
                    String newRole = roleText == null ? "" : roleText;
                    ruleStr = rule.getCodeValue() + newRole;
                } else {
                    ruleStr = rule.getCodeValue();
                }
            }
            if (ObjectUtil.isNotNull(setting)) {
                String key = setting.getCodeValue();
                Map<String, Object> params = new HashMap<>();
                List<JSONObject> list = new ArrayList<>();
                JSONObject msgObj = new JSONObject();
                msgObj.put("role", "user");
                msgObj.put("content", command.trim() + ruleStr);
                list.add(msgObj);
                params.put("messages", list);
                params.put("model", "gpt-3.5-turbo");
                String jsonStr = JsonUtils.getJsonString(params);
                String result = HttpUtils.doPostByToken("https://openai.api2d.net/v1/chat/completions", key.trim(), jsonStr);
                if (StrKit.notBlank(result)) {
                    logger.info("gpt响应结果：" + result);
                    JSONObject object = JSONObject.parseObject(result);
                    JSONArray array = object.getJSONArray("choices");
                    if (CollectionUtil.isNotEmpty(array)) {
                        JSONObject msg = array.getJSONObject(0);
                        return msg.getJSONObject("message").getString("content");
                    }
                }
            }
        } catch (Exception e) {
            logger.info("请求QD的GPT接口失败");
            e.printStackTrace();
        }

        return "系统繁忙，请稍后再试";
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping("/sch")
    public String sch() {
        List<QdSchedule> scheduleList = qdScheduleDao.findAll();
        Map<String, List<QdSchedule>> map = new HashMap<>();
        Map<Integer, List<QdSchedule>> listMap = scheduleList.stream().collect(Collectors.groupingBy(QdSchedule::getWeek));
        listMap.forEach((k, v) -> map.put(k.toString(), v));
        return JsonUtils.getJsonString(map);
    }

    @RequestMapping("/upSch")
    public String upSch(Long id, String content) {
        try {
            QdSchedule schedule = qdScheduleDao.findById(id);
            if (ObjectUtil.isNotNull(schedule)) {
                schedule.setContent(content);
                schedule.setIndex(null);
                qdScheduleDao.saveOrUpdate(schedule);
                return "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "NONE";
    }


    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping("/feedBack")
    public String feedBack(HttpServletRequest request, String content) {
        try {
            if (StrKit.isBlank(content) || content.length() > 1000) {
                return "FAIL";
            }
            QdFeedBack feedBack = new QdFeedBack();
            feedBack.setIp(IPUtil.getRealIp(request));
            feedBack.setContent(content);
            feedBack.setCreateTime(new Date());
            qdFeedBackDao.saveOrUpdate(feedBack);
            return "SUCCESS";
        } catch (Exception e) {
            logger.info("请求接口失败");
            e.printStackTrace();
        }
        return "FAIL";
    }

}
