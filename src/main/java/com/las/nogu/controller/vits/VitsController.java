package com.las.nogu.controller.vits;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.las.bot.dao.VitsModelDao;
import com.las.bot.dao.VitsUserDao;
import com.las.utils.mirai.CmdUtil;
import com.utils.DateUtils;
import com.utils.JsonUtils;
import com.utils.RedisUtils;
import com.utils.StrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/vits")
public class VitsController {

    @Autowired
    private VitsUserDao vitsUserDao;

    @Autowired
    private VitsModelDao modelDao;


    @CrossOrigin(origins = "*", allowedHeaders="*")
    @RequestMapping("/hitokoto")
    public String hitokoto() throws Exception {
        Map<String,Object> resp = new HashMap<>();
        String day = DateUtils.formatYMD(new Date());
        Object obj = RedisUtils.get("qd:model:" + day);
        if (ObjectUtil.isNotNull(obj)) {
            JSONObject jsonObject = JsonUtils.getJsonObjectByJsonString(obj.toString());
            return jsonObject.toJSONString();
        }
        String txt = "你好，我叫秋蒂~模型还在开发中";
        boolean ja = StrUtils.isJapanese(txt);
        String tag = "[ZH]";
        if(ja){
            tag = "[JA]";
        }
        String content = tag + txt + tag;
        JSONObject param = new JSONObject();
        param.put("token","dc308041-82b0-4908-954d-b7765ff2d8c6");
        param.put("uId","2547170055");
        param.put("mId","20");
        param.put("rId","0");
        param.put("text",content);
        param.put("ar","0");
        param.put("domin","0");
        param.put("va","0");
        String result = HttpKit.post("http://vits.natapp1.cc/sovits/genByText", param.toJSONString());
        JSONObject obj2 = JSONObject.parseObject(result);
        if(obj2.getIntValue("code") == 0){
            String url = obj2.getString("data");
            resp.put("url",url);
        }
        resp.put("txt",txt);
        RedisUtils.set("qd:model:" + day, JSONObject.toJSONString(resp),30);
        return JSONObject.toJSONString(resp);
    }


}
