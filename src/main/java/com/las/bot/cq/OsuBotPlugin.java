package com.las.bot.cq;

import com.las.cq.event.message.CQGroupMessageEvent;
import com.las.cq.robot.CQPlugin;
import com.las.cq.robot.CoolQ;
import com.las.nogu.web.OsuWebSocket;
import com.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@Component
public class OsuBotPlugin extends CQPlugin {

    private static Logger logger = LoggerFactory.getLogger(OsuBotPlugin.class);

    @Autowired
    private OsuWebSocket osuBot;

    @Override
    public int onGroupMessage(CoolQ cq, CQGroupMessageEvent event) {
        long groupId = event.getGroupId();
        logger.info("群【" + groupId + "】消息：" + event.getMessage());
        if (!event.getMessage().contains("[CQ:")) {
            osuBot.sendOneMessage(String.valueOf(groupId), JsonUtils.getJsonString(event));
        }
        return MESSAGE_IGNORE;
    }
}
