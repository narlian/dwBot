package com.las.bot.cq;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.las.bot.common.Constant;
import com.las.cq.event.message.CQGroupMessageEvent;
import com.las.cq.robot.CQPlugin;
import com.las.cq.robot.CoolQ;
import com.las.cq.utils.CQCode;
import com.las.nogu.web.OsuWebSocket;
import com.utils.JsonUtils;
import com.utils.StrUtils;
import com.utils.youdao.RecordUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

@Component
public class AiBotPlugin extends CQPlugin {

    private static Logger logger = LoggerFactory.getLogger(AiBotPlugin.class);


    @Override
    public int onGroupMessage(CoolQ cq, CQGroupMessageEvent event) {
        try {
            long groupId = event.getGroupId();
            logger.info("群【" + groupId + "】消息：" + event.getMessage());
            String cmd = event.getMessage().toLowerCase();
            cmd = cmd.replaceAll("复读","").replaceAll("#","");
            String content = "";
            String model = "";
            String config = "";
            String index = "";
            if (cmd.startsWith("东海帝王ai")) {
                String txt = cmd.split("东海帝王ai")[1];
                content = RecordUtil.getTranslation(txt, "ja");
                model = "./model/JA/1/model.pth";
                config = "./model/JA/1/config.json";
                index = "2";
            }
            if (cmd.startsWith("在原七海ai")) {
                String txt = cmd.split("在原七海ai")[1];
                content = RecordUtil.getTranslation(txt, "ja");
                model = "./model/JA/0/model.pth";
                config = "./model/JA/0/config.json";
                index = "6";
            }
            if (!StrUtil.isBlank(content)) {
                String url = Constant.HTTPS + Constant.BUCKET_NAME + "." + Constant.OSS + getAvUrl(content, model, config, index) + ".wav.wav";
                String recordMsg = "[CQ:video,file="+url+"]";
                cq.sendGroupMsg(groupId, recordMsg, false);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return MESSAGE_IGNORE;
    }

    private String getAvUrl(String text, String m, String c, String r) throws Exception {
        String result = "";
        Socket socket = new Socket("3462ecf06c6e1626.natapp.cc", 12467);
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
        JSONObject object = new JSONObject();
        //写死宁宁的模型
        object.put("model", m);
        object.put("config", c);
        object.put("index", r);
        object.put("text", text);
        dos.writeUTF("#" + object.toJSONString());
        DataInputStream dis = new DataInputStream(socket.getInputStream());
        byte[] bs = new byte[10240];
        int len = dis.read(bs);
        result = new String(bs, 0, len);
        socket.close();
        return result;
    }
}
