package com.las.bot.cq;

import com.las.cq.event.request.CQFriendRequestEvent;
import com.las.cq.retdata.ApiRawData;
import com.las.cq.robot.CQPlugin;
import com.las.cq.robot.CoolQ;
import com.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AddFriendPlugin extends CQPlugin {

    private static Logger logger = LoggerFactory.getLogger(AddFriendPlugin.class);


    @Override
    public int onFriendRequest(CoolQ cq, CQFriendRequestEvent event) {
        ApiRawData rawData = cq.setFriendAddRequest(event.getFlag(), true, "");
        logger.info("加好友请求：" + JsonUtils.getJsonString(rawData));
        return MESSAGE_IGNORE;
    }
}
