package com.las.bot.cq;

import com.las.cq.event.message.CQDiscussMessageEvent;
import com.las.cq.event.message.CQGroupMessageEvent;
import com.las.cq.event.message.CQPrivateMessageEvent;
import com.las.cq.robot.CQPlugin;
import com.las.cq.robot.CoolQ;
import com.las.cq.utils.CQCode;
import org.springframework.stereotype.Component;

@Component
public class HelloPlugin extends CQPlugin {

    /**
     * 收到私聊消息时会调用这个方法
     *
     * @param cq    机器人对象，用于调用API，例如发送私聊消息 sendPrivateMsg
     * @param event 事件对象，用于获取消息内容、群号、发送者QQ等
     * @return 是否继续调用下一个插件，IGNORE表示继续，BLOCK表示不继续
     */
    @Override
    public int onPrivateMessage(CoolQ cq, CQPrivateMessageEvent event) {
        // 获取 发送者QQ 和 消息内容
        long userId = event.getUserId();
        String msg = event.getMessage();

        if (msg.equalsIgnoreCase("hi")) {
            // 调用API发送hello
            cq.sendPrivateMsg(userId, "hello", false);

            // 不执行下一个插件
            return MESSAGE_BLOCK;
        }
        // 继续执行下一个插件
        return MESSAGE_IGNORE;
    }

    @Override
    public int onDiscussMessage(CoolQ cq, CQDiscussMessageEvent event) {
        // 获取 发送者QQ 和 消息内容
        long userId = event.getUserId();
        String msg = event.getMessage();

        if (msg.equalsIgnoreCase("hi")) {
            // 调用API发送hello
            cq.sendDiscussMsg(userId, "hello", false);

            // 不执行下一个插件
            return MESSAGE_BLOCK;
        }
        // 继续执行下一个插件
        return MESSAGE_IGNORE;
    }

    @Override
    public int onGroupMessage(CoolQ cq, CQGroupMessageEvent event) {
        // 获取 消息内容 群号 发送者QQ
        String msg = event.getMessage();
        long groupId = event.getGroupId();
        long userId = event.getUserId();

        if (msg.equalsIgnoreCase("hi")) {
            // 回复内容为 at发送者 + hi
            String result = CQCode.at(userId) + "hello~（测试机器人）";

            // 调用API发送消息
            cq.sendGroupMsg(groupId, result, false);

            // 不执行下一个插件
            return MESSAGE_BLOCK;
        }

        // 继续执行下一个插件
        return MESSAGE_IGNORE;
    }
}
