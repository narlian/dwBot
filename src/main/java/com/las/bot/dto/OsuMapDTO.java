package com.las.bot.dto;


import com.model.OsuMap;


/**
 * 非数据库字段都弄DTO
 *
 */
public class OsuMapDTO extends OsuMap {


    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
