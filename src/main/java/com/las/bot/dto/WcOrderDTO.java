package com.las.bot.dto;

public class WcOrderDTO {

    private String key;

    private Double maxFx;

    private Double fx;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Double getMaxFx() {
        return maxFx;
    }

    public void setMaxFx(Double maxFx) {
        this.maxFx = maxFx;
    }

    public Double getFx() {
        return fx;
    }

    public void setFx(Double fx) {
        this.fx = fx;
    }

    @Override
    public String toString() {
        return "WcOrderDTO{" +
                "key='" + key + '\'' +
                ", maxFx=" + String.format("%.2f",maxFx) +
                ", fx=" + String.format("%.2f",fx) +
                '}';
    }
}
