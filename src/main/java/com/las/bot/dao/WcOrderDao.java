package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.WcGroup;
import com.model.WcOrder;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class WcOrderDao extends BaseDao<WcOrder> {

    public WcOrderDao() {
        super(WcOrder.class);
    }

    public WcOrder findByRecent(){
        WcOrder order = null;
        String sql = "select * from wc_order where is_end = 0 order by id desc limit 1";
        QueryRunner qr = getRunner();
        try {
            order = qr.query(sql, new BeanHandler<>(WcOrder.class, getProcessor()));
        } catch (SQLException ignored) {

        }
        return order;
    }

}
