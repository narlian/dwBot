package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.QaOption;
import com.model.WcGroup;
import com.model.WcGroupNum;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class WcGroupNumDao extends BaseDao<WcGroupNum> {
    public WcGroupNumDao() {
        super(WcGroupNum.class);
    }

    public List<WcGroupNum> findListByGroupName(String groupName) {
        List<WcGroupNum> list = new ArrayList<>();
        String sql = "select * from wc_group_num where group_name = ?";
        QueryRunner qr = getRunner();
        try {
            list = qr.query(sql, new BeanListHandler<>(WcGroupNum.class, getProcessor()), groupName);
        } catch (SQLException ignored) {

        }
        return list;
    }

}
