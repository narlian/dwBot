package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.QdFeedBack;
import org.springframework.stereotype.Component;

@Component
public class QdFeedBackDao extends BaseDao<QdFeedBack> {

    public QdFeedBackDao() {
        super(QdFeedBack.class);
    }
}


