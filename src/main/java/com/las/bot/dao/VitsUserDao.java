package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.SeMember;
import com.model.VitsUser;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class VitsUserDao extends BaseDao<VitsUser> {

    public VitsUserDao() {
        super(VitsUser.class);
    }


    public VitsUser findByUserAndCode(String userId, String code) {
        VitsUser obj = null;
        String sql = "select * from vits_user where user_id = ? and code = ?";
        QueryRunner qr = getRunner();
        try {
            obj = qr.query(sql, new BeanHandler<>(VitsUser.class, getProcessor()), userId, code);
        } catch (SQLException ignored) {

        }
        return obj;
    }

}
