package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.WxMsg;
import org.springframework.stereotype.Component;

@Component
public class WxMsgDao extends BaseDao<WxMsg> {

    public WxMsgDao() {
        super(WxMsg.class);
    }

}
