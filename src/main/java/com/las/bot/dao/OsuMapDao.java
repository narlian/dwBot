package com.las.bot.dao;

import com.alibaba.fastjson.JSONObject;
import com.model.OsuMap;
import com.utils.JsonUtils;
import com.las.dao.base.BaseDao;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class OsuMapDao extends BaseDao<OsuMap> {

    public OsuMapDao() {
        super(OsuMap.class);
    }

    public OsuMap findFirst(String sql, Object... params) {
        OsuMap osuMap = null;
        QueryRunner qr = getRunner();
        try {
            osuMap = qr.query(sql, new BeanHandler<>(OsuMap.class, getProcessor()), params);
        } catch (SQLException ignored) {

        }
        return osuMap;
    }

    public JSONObject findFirstObj(String sql, Object... params) {
        JSONObject obj = null;
        QueryRunner qr = getRunner();
        try {
            Map<String, Object> map = qr.query(sql, new MapHandler(getProcessor()), params);
            obj = JsonUtils.getJsonObjectByJsonString(JsonUtils.getJsonString(map));
        } catch (SQLException ignored) {

        }
        return obj;
    }

    public List<OsuMap> findList(String sql) {
        List<OsuMap> maps = new ArrayList<>();
        QueryRunner qr = getRunner();
        try {
            maps = qr.query(sql, new BeanListHandler<>(OsuMap.class, getProcessor()));
        } catch (SQLException ignored) {

        }
        return maps;
    }
}
