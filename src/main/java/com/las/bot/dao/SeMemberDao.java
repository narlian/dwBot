package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.CqMsgUrl;
import com.model.SeMember;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Component
public class SeMemberDao extends BaseDao<SeMember> {

    public SeMemberDao() {
        super(SeMember.class);
    }

    public SeMember findByUserId(long userId) {
        SeMember obj = null;
        String sql = "select * from se_member where user_id = ?";
        QueryRunner qr = getRunner();
        try {
            obj = qr.query(sql, new BeanHandler<>(SeMember.class, getProcessor()),userId);
        } catch (SQLException ignored) {

        }
        return obj;
    }

}
