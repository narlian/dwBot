package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.SeMember;
import com.model.SeReward;
import com.model.SovitsSetting;
import com.utils.DateUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Component
public class SovitsDao extends BaseDao<SovitsSetting> {

    public SovitsDao() {
        super(SovitsSetting.class);
    }

    public SovitsSetting findByName(String name) {
        SovitsSetting obj = null;
        String sql = "select * from sovits_setting where name = ?";
        QueryRunner qr = getRunner();
        try {
            obj = qr.query(sql, new BeanHandler<>(SovitsSetting.class, getProcessor()),name);
        } catch (SQLException ignored) {

        }
        return obj;
    }


}
