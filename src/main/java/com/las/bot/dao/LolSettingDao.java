package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.LolSetting;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class LolSettingDao extends BaseDao<LolSetting> {

    public LolSettingDao() {
        super(LolSetting.class);
    }

    public LolSetting findByName(String name) {
        LolSetting setting = null;
        String sql = "select * from lol_setting where name = ? limit 1";
        QueryRunner qr = getRunner();
        try {
            setting = qr.query(sql, new BeanHandler<>(LolSetting.class, getProcessor()), name);
        } catch (SQLException ignored) {

        }
        return setting;
    }
}
