package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.CqMsgUrl;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Component
public class CqMsgUrlDao extends BaseDao<CqMsgUrl> {

    public CqMsgUrlDao() {
        super(CqMsgUrl.class);
    }

    public List<CqMsgUrl> getIw233AllTotal() {
        List<CqMsgUrl> list = null;
        String sql = "select group_id,count(*) as user_id \n" +
                "from cq_msg_url\n" +
                "where user_id = -1 \n" +
                "group by group_id having(group_id) > 0 order by user_id desc";
        QueryRunner qr = getRunner();
        try {
            list = qr.query(sql, new BeanListHandler<>(CqMsgUrl.class, getProcessor()));
        } catch (SQLException ignored) {

        }
        return list;
    }


    public CqMsgUrl findIw233Rand() {
        CqMsgUrl obj = null;
        String sql = "select * from cq_msg_url where user_id = -1 and judge <> '合规' order by rand() limit 1";
        QueryRunner qr = getRunner();
        try {
            obj = qr.query(sql, new BeanHandler<>(CqMsgUrl.class, getProcessor()));
        } catch (SQLException ignored) {

        }
        return obj;
    }

    public Long getIw233Total() {
        long total = 0L;
        String sql = "select count(*) as total from cq_msg_url where user_id = -1 and judge <> '合规' ";
        QueryRunner qr = getRunner();
        try {
            Map<String, Object> map = qr.query(sql, new MapHandler(getProcessor()));
            total = Long.parseLong(map.get("total").toString());
        } catch (SQLException ignored) {

        }
        return total;
    }
}
