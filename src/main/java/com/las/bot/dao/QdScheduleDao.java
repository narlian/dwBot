package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.QaOption;
import com.model.QdSchedule;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class QdScheduleDao extends BaseDao<QdSchedule> {

    public QdScheduleDao() {
        super(QdSchedule.class);
    }

    public List<QdSchedule> findAll() {
        List<QdSchedule> list = new ArrayList<>();
        String sql = "select * from qd_schedule";
        QueryRunner qr = getRunner();
        try {
            list = qr.query(sql, new BeanListHandler<>(QdSchedule.class, getProcessor()));
        } catch (SQLException ignored) {

        }
        return list;
    }
}
