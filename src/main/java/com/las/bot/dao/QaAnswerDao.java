package com.las.bot.dao;

import com.model.QaAnswer;
import com.las.dao.base.BaseDao;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class QaAnswerDao extends BaseDao<QaAnswer> {

    public QaAnswerDao() {
        super(QaAnswer.class);
    }

    public QaAnswer findByQaIdAndNum(Object qId, Object num) {
        QaAnswer answer = null;
        String sql = "select * from qa_answer where qa_id = ? and answer_num = ? limit 1";
        QueryRunner qr = getRunner();
        try {
            answer = qr.query(sql, new BeanHandler<>(QaAnswer.class, getProcessor()), qId, num);
        } catch (SQLException ignored) {

        }
        return answer;
    }

    public QaAnswer findByQaIdAndScore(Object qId, Object score) {
        QaAnswer answer = null;
        String sql = "select * from qa_answer where qa_id = ? and ? between min_score and max_score limit 1";
        QueryRunner qr = getRunner();
        try {
            answer = qr.query(sql, new BeanHandler<>(QaAnswer.class, getProcessor()), qId, score);
        } catch (SQLException ignored) {

        }
        return answer;
    }

}
