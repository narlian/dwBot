package com.las.bot.dao;

import com.model.QaOption;
import com.las.dao.base.BaseDao;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class QaOptionDao extends BaseDao<QaOption> {

    public QaOptionDao() {
        super(QaOption.class);
    }

    public List<QaOption> findListByQaId(Object qId) {
        List<QaOption> list = new ArrayList<>();
        String sql = "select * from qa_option where qa_id = ? order by op_num asc";
        QueryRunner qr = getRunner();
        try {
            list = qr.query(sql, new BeanListHandler<>(QaOption.class, getProcessor()), qId);
        } catch (SQLException ignored) {

        }
        return list;
    }

    public QaOption findByQaIdAndNum(Object qId, Object opNum) {
        QaOption option = null;
        String sql = "select * from qa_option where qa_id = ? and op_num = ? limit 1";
        QueryRunner qr = getRunner();
        try {
            option = qr.query(sql, new BeanHandler<>(QaOption.class, getProcessor()), qId, opNum);
        } catch (SQLException ignored) {

        }
        return option;
    }

}
