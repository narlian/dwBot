package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.QdLived;
import org.springframework.stereotype.Component;

@Component
public class QdLivedDao extends BaseDao<QdLived> {

    public QdLivedDao() {
        super(QdLived.class);
    }


}
