package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.LolArea;
import com.model.LolSetting;
import com.model.LolTier;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class LolAreaDao extends BaseDao<LolArea> {

    public LolAreaDao() {
        super(LolArea.class);
    }

    public List<LolArea> findList() {
        List<LolArea> list = new ArrayList<>();
        String sql = "select * from lol_area";
        QueryRunner qr = getRunner();
        try {
            list = qr.query(sql, new BeanListHandler<>(LolArea.class, getProcessor()));
        } catch (SQLException ignored) {

        }
        return list;
    }


}
