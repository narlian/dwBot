package com.las.bot.dao;

import com.model.Qa;
import com.las.dao.base.BaseDao;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class QaDao extends BaseDao<Qa> {

    public QaDao() {
        super(Qa.class);
    }

    public List<Qa> findList() {
        List<Qa> qaList = new ArrayList<>();
        String sql = "select * from qa where state = 1 order by id asc";
        QueryRunner qr = getRunner();
        try {
            qaList = qr.query(sql, new BeanListHandler<>(Qa.class, getProcessor()));
        } catch (SQLException ignored) {

        }
        return qaList;
    }
}
