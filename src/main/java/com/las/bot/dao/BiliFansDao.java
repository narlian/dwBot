package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.BiliFans;
import com.model.CqMsg;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Map;

@Component
public class BiliFansDao extends BaseDao<BiliFans> {

    public BiliFansDao() {
        super(BiliFans.class);
    }

    public Long getTotal() {
        long total = 0L;
        String sql = "select count(distinct f.fid) total from bili_fans f";
        QueryRunner qr = getRunner();
        try {
            Map<String, Object> map = qr.query(sql, new MapHandler(getProcessor()));
            total = Long.parseLong(map.get("total").toString());
        } catch (SQLException ignored) {

        }
        return total;
    }

}
