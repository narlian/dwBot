package com.las.bot.dao;

import com.alibaba.fastjson.JSONObject;
import com.las.dao.base.BaseDao;
import com.model.CqMsg;
import com.utils.DateUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class CqMsgDao extends BaseDao<CqMsg> {

    public CqMsgDao() {
        super(CqMsg.class);
    }

    public List<JSONObject> findTop(long gId) {
        List<JSONObject> list = null;
        String firstWeekByNowTime = DateUtils.getFirstWeekByNowTime();
        Date firstWeekDate = DateUtils.formatToDate(DateUtils.HOUR_24, firstWeekByNowTime);
        Date lastWeekDate = DateUtils.addDay(firstWeekDate, 7);
        String startTime = DateUtils.formatYMD(firstWeekDate) + " 00:00:00";
        String endTime = DateUtils.formatYMD(lastWeekDate) + " 00:00:00";
        String sql = "select user_id,count(id) as total \n" +
                "from cq_msg \n" +
                "where type = 1 and group_id = ?\n" +
                "and create_time between ? and ?\n" +
                "group by user_id order by total desc";
        QueryRunner qr = getRunner();
        try {
            List<Map<String, Object>> query = qr.query(sql, new MapListHandler(getProcessor()), gId, startTime, endTime);
            list = JSONObject.parseArray(JSONObject.toJSONString(query), JSONObject.class);
        } catch (SQLException ignored) {

        }
        return list;
    }
}
