package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.ReWard;
import org.springframework.stereotype.Component;

@Component
public class ReWardDao extends BaseDao<ReWard> {

    public ReWardDao() {
        super(ReWard.class);
    }

}
