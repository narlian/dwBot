package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.LolSetting;
import com.model.LolTier;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class LolTierDao extends BaseDao<LolTier> {

    public LolTierDao() {
        super(LolTier.class);
    }

    public LolTier findByTierId(Integer tierId) {
        LolTier tier = null;
        String sql = "select * from lol_tier where tier = ? limit 1";
        QueryRunner qr = getRunner();
        try {
            tier = qr.query(sql, new BeanHandler<>(LolTier.class, getProcessor()), tierId);
        } catch (SQLException ignored) {

        }
        return tier;
    }
}
