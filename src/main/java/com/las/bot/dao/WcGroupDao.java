package com.las.bot.dao;

import com.las.dao.base.BaseDao;
import com.model.QaAnswer;
import com.model.WcGroup;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class WcGroupDao extends BaseDao<WcGroup> {

    public WcGroupDao() {
        super(WcGroup.class);
    }

    public WcGroup findByGroupName(String groupName){
        WcGroup group = null;
        String sql = "select * from wc_group where group_name = ? limit 1";
        QueryRunner qr = getRunner();
        try {
            group = qr.query(sql, new BeanHandler<>(WcGroup.class, getProcessor()),groupName);
        } catch (SQLException ignored) {

        }
        return group;
    }

}
