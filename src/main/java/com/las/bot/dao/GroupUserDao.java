package com.las.bot.dao;

import com.model.GroupUser;
import com.las.dao.base.BaseDao;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class GroupUserDao extends BaseDao<GroupUser> {

    public GroupUserDao() {
        super(GroupUser.class);
    }

    public GroupUser findByGIdUId(Long gId, Long uId) {
        String sql = "select * from group_user where group_id = ? and user_id = ? limit 1";
        GroupUser groupUser = null;
        QueryRunner qr = getRunner();
        try {
            groupUser = qr.query(sql, new BeanHandler<>(GroupUser.class, getProcessor()), gId, uId);
        } catch (SQLException ignored) {

        }
        return groupUser;
    }

    public boolean delByGId(Long gId) {
        String sql = "delete from group_user where group_id = ? ";
        int row = 0;
        QueryRunner qr = getRunner();
        try {
            row = qr.update(sql, gId);
        } catch (SQLException ignored) {

        }
        return row > 0;
    }

}
