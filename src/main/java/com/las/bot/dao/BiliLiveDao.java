package com.las.bot.dao;

import com.google.common.collect.Lists;
import com.las.dao.base.BaseDao;
import com.model.BiliLive;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;

@Component
public class BiliLiveDao extends BaseDao<BiliLive> {

    public BiliLiveDao() {
        super(BiliLive.class);
    }

    public List<BiliLive> findAll() {
        List<BiliLive> list = Lists.newArrayList();
        String sql = "select * from bili_live";
        QueryRunner qr = getRunner();
        try {
            list = qr.query(sql, new BeanListHandler<>(BiliLive.class, getProcessor()));
        } catch (SQLException ignored) {

        }
        return list;
    }

}
