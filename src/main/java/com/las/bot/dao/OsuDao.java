package com.las.bot.dao;

import com.alibaba.fastjson.JSONObject;
import com.model.Osu;
import com.utils.JsonUtils;
import com.las.dao.base.BaseDao;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Map;

@Component
public class OsuDao extends BaseDao<Osu> {

    public OsuDao() {
        super(Osu.class);
    }

    public Osu findFirst(String sql, Object... params) {
        Osu osu = null;
        QueryRunner qr = getRunner();
        try {
            osu = qr.query(sql, new BeanHandler<>(Osu.class, getProcessor()), params);
        } catch (SQLException ignored) {

        }
        return osu;
    }

    public JSONObject findFirstObj(String sql, Object... params) {
        JSONObject obj = null;
        QueryRunner qr = getRunner();
        try {
            Map<String, Object> map = qr.query(sql, new MapHandler(getProcessor()), params);
            obj = JsonUtils.getJsonObjectByJsonString(JsonUtils.getJsonString(map));
        } catch (SQLException ignored) {

        }
        return obj;
    }
}
