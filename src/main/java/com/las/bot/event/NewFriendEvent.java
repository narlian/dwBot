package com.las.bot.event;

import com.alibaba.fastjson.JSONObject;
import com.las.annotation.BotEvent;
import com.las.bot.common.Constant;
import com.las.config.AppConfigs;
import com.las.enums.MsgCallBackEnum;
import com.las.strategy.handle.AbstractBotMsgHandler;
import com.las.strategy.handle.NewFriendRequestsMsgHandler;
import com.las.utils.mirai.CmdUtil;
import com.las.utils.mirai.MiRaiUtil;
import org.apache.log4j.Logger;

import static com.las.bot.common.Constant.MY_TEST_GID;

@BotEvent(event = MsgCallBackEnum.NEW_FRIEND_REQUESTS_MSG)
public class NewFriendEvent extends AbstractBotMsgHandler {

    private static Logger logger = Logger.getLogger(NewFriendRequestsMsgHandler.class);

    @Override
    public void exec() {
        JSONObject object = getCqObj();
        logger.info(object.toJSONString());
        //添加好友
        MiRaiUtil.getInstance().agreeFriend(object);
        //发送消息给超管
        long superQQ = Long.parseLong(AppConfigs.superQQ);
        CmdUtil.sendMessage("好友添加：" + object.getLongValue("fromId"), superQQ, MY_TEST_GID, Constant.MESSAGE_TYPE_GROUP);
    }
}
