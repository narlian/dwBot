package com.las.bot.event;

import com.alibaba.fastjson.JSONObject;
import com.las.annotation.BotEvent;
import com.las.bot.common.Constant;
import com.las.config.AppConfigs;
import com.las.dao.FunDao;
import com.las.enums.MsgCallBackEnum;
import com.las.strategy.handle.AbstractBotMsgHandler;
import com.las.utils.mirai.CmdUtil;
import com.las.utils.mirai.MiRaiUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import static com.las.bot.common.Constant.MY_TEST_GID;

@BotEvent(event = MsgCallBackEnum.BOT_JOIN_GROUP_MSG)
public class BotJoinEvent extends AbstractBotMsgHandler {

    private static Logger logger = Logger.getLogger(BotJoinEvent.class);

    @Autowired
    private FunDao funDao;

    @Override
    public void exec() {
        JSONObject object = getCqObj();
        logger.info(object.toJSONString());
        //邀请进群
        //MiRaiUtil.getInstance().agreeGroup(object);
        long superQQ = Long.parseLong(AppConfigs.superQQ);
        CmdUtil.sendMessage("群添加：" + object.getJSONObject("group").getLongValue("id"), superQQ, MY_TEST_GID, Constant.MESSAGE_TYPE_GROUP);

    }

    private void deleteFun() {
        funDao.deleteAll();
    }
}
