package com.las.bot.event;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.las.annotation.BotEvent;
import com.las.bot.dao.CqMsgDao;
import com.las.enums.MsgCallBackEnum;
import com.las.strategy.handle.AbstractBotMsgHandler;
import com.model.CqMsg;
import com.utils.StrUtils;
import com.utils.ThreadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import java.util.Date;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 自定义QQ群里消息事件
 */
@BotEvent(event = MsgCallBackEnum.GROUP_MSG)
public class GroupEvent extends AbstractBotMsgHandler {

    private Logger logger = LoggerFactory.getLogger(GroupEvent.class);

    private ThreadPoolExecutor executor = ThreadUtil.getPool();

    @Autowired
    private CqMsgDao cqMsgDao;

    @Override
    public void exec() {
        String data;
        String msgData = getMsgData();
        if (StrUtil.isBlank(msgData)) {
            return;
        }
        msgData = msgData.trim();
        if (msgData.startsWith("＃")) {
            data = "#" + msgData.substring(1);
        } else {
            data = msgData;
        }
        // 后续可以对群聊消息做大数据分析
        exeCommand(data, getUserId(), getId(), getType());
        executor.execute(() -> {
            CqMsg cqMsg = new CqMsg();
            cqMsg.setUserId(getUserId());
            cqMsg.setGroupId(getId());
            cqMsg.setContent(data);
            cqMsg.setType(getType());
            cqMsg.setCqData(getCqObj().toJSONString());
            cqMsg.setCreateTime(new Date());
            cqMsgDao.saveOrUpdate(cqMsg);
        });


    }



}
