package com.las.bot.event;

import cn.hutool.core.util.StrUtil;
import com.las.annotation.BotEvent;
import com.las.bot.dao.CqMsgDao;
import com.las.enums.MsgCallBackEnum;
import com.las.strategy.handle.AbstractBotMsgHandler;
import com.model.CqMsg;
import com.utils.ThreadUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.concurrent.ThreadPoolExecutor;

@BotEvent(event = MsgCallBackEnum.TEMP_MSG)
public class TempEvent extends AbstractBotMsgHandler {

    private ThreadPoolExecutor executor = ThreadUtil.getPool();

    @Autowired
    private CqMsgDao cqMsgDao;

    @Override
    public void exec() {
        String data;
        String msgData = getMsgData();
        if (StrUtil.isBlank(msgData)) {
            return;
        }
        msgData = msgData.trim();
        if (msgData.startsWith("＃")) {
            data = "#" + msgData.substring(1);
        } else {
            data = msgData;
        }
        exeCommand(data, getUserId(), getId(), getType());
        executor.execute(() -> {
            CqMsg cqMsg = new CqMsg();
            cqMsg.setUserId(getUserId());
            cqMsg.setGroupId(getId());
            cqMsg.setContent(data);
            cqMsg.setType(getType());
            cqMsg.setCqData(getCqObj().toJSONString());
            cqMsg.setCreateTime(new Date());
            cqMsgDao.saveOrUpdate(cqMsg);
        });
    }

}
