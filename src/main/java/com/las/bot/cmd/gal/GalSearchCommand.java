package com.las.bot.cmd.gal;

import com.utils.SpiderHtml;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@BotCmd(funName = "GAL")
public class GalSearchCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(GalSearchCommand.class);


    public GalSearchCommand() {
        super("GS", "GALSearch", "GAL", "GAL搜索");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        //开始实现 GAL搜索 功能
        String searchName = null;
        if (args.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String s : args) {
                sb.append(s).append(" ");
            }
            searchName = sb.toString();
        }
        if (StrKit.notBlank(searchName)) {
            Set<String> keys = new HashSet<>();
            keys.add(searchName.trim());
            keys.add("i.tianshi.info");
            String info = SpiderHtml.spiderByKeyWithHtml("https://www.tianshi2.cc/search/" + searchName.trim(), keys);
            String[] split = info.split("\\n");
            int count = 1;
            for (String str : split) {
                if (count > 1) {
                    break;
                }
                if (str.contains("http://bbsimg.tianshi2.net/") || str.contains("tim.php")) {
                    logger.info("GAL匹配后：" + str);
                    int index = str.indexOf("tim.php?src=");
                    String url = str.substring(index, str.indexOf("alt") - 2);
                    ArrayList<String> urls = new ArrayList<>();
                    urls.add("https://tim.623456.xyz/" + url);

                    int index2 = str.indexOf("href=");
                    String href = str.substring(index2 + 6, str.indexOf(".html") + 5);
                    int index3 = str.indexOf("alt=");
                    String content = str.substring(index3 + 5, str.lastIndexOf("]") + 1);

                    keys = new HashSet<>();
                    keys.add("magnet:?");
                    String result = SpiderHtml.spiderByKeyWithHtml(href, keys);
                    String magnetInfo = result.split("\\n")[0];
                    int index4 = magnetInfo.indexOf("url");
                    String magnet = magnetInfo.substring(index4 + 5, magnetInfo.lastIndexOf("}") - 2);

                    CmdUtil.sendImgMessage(urls, userId, id, type);
                    String msg = content + "\n种子：" + magnet;
                    CmdUtil.sendMessage(msg, userId, id, type);

                    count++;
                }
            }
        } else {
            CmdUtil.sendMessage("请在指令后面带参数", userId, id, type);
        }
    }

}
