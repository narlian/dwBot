package com.las.bot.cmd.xf;

import cn.hutool.core.util.ObjectUtil;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.StrUtils;
import com.las.utils.mirai.CmdUtil;
import com.utils.RedisUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@BotCmd(funName = "新番搜索")
@Component(value = "xFSCommand")
public class XFSCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(XFSCommand.class);


    public XFSCommand() {
        super("新番", "番剧", "动漫", "新番搜索");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        Object obj = RedisUtils.get("xfs:" + userId);
        if (ObjectUtil.isNotNull(obj)) {
            CmdUtil.sendMessage("操作过于频繁，请30秒后再试~", userId, id, type);
            return;
        }
        RedisUtils.set("xfs:" + userId, userId, 30);
        StringBuilder sb = new StringBuilder();
        if (args.size() == 0) {
            CmdUtil.sendMessage("请在指令后面带番名", userId, id, type);
            return;
        }
        String searchName = args.get(0).toUpperCase();
        String url = "https://www.36dm.club/search.php?keyword=";
        Document document = Jsoup.parse(new URL(url + URLEncoder.encode(searchName, "UTF-8")), 30000);
        Elements list = document.getElementsByClass("alt1");
        int index = 0;
        if (args.size() > 1) {
            int n = 0;
            String number = args.get(1);
            if (StrUtils.isNumeric(number)) {
                n = Integer.parseInt(number);
            }
            if (n <= 0 || n > 5) {
                CmdUtil.sendMessage("参数不对，请检查", userId, id, type);
                return;
            }
            for (Element element : list) {
                if (index + 1 == n) {
                    Elements elements = element.getElementsByTag("a");
                    for (Element tag : elements) {
                        String href = tag.attr("href");
                        if (href.contains("show-")) {
                            String url2 = "https://www.36dm.club" + href;
                            sb.append("新番链接：").append(url2).append("\n");
                            Document document2 = Jsoup.parse(new URL(url2), 30000);
                            Element magnet = document2.getElementById("magnet");
                            String href2 = magnet.attr("href");
                            sb.append("迅雷种子：").append(href2).append("\n");
                            break;
                        }
                    }
                }
                index++;
            }
        } else {
            for (Element element : list) {
                if (index >= 5) {
                    break;
                }
                sb.append("序号").append(index + 1).append("、");
                sb.append(element.text()).append("\n");
                index++;
            }
            sb.append("\n紧接着指令后输入序号可获得种子链接~");
        }
        if (StrUtils.isNotBlank(sb.toString())) {
            CmdUtil.sendMessage(sb.toString(), userId, id, type);
        }

    }

}
