package com.las.bot.cmd.game;


import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;

import java.util.ArrayList;

@BotCmd(funName = "斗地主")
public class DdzRuleCommand extends BaseCommand {

    public DdzRuleCommand() {
        super("斗地主", "规则");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        String rule = "斗地主功能指令有：\n" +
                "1、我要报名\n" +
                "2、我要退房\n" +
                "3、我要出牌\n" +
                "4、我要看牌\n" +
                "[注]：牌面里的B是大王、X是小王、T是10\n" +
                "\n" +
                "例如我演示一遍功能如下：\n" +
                "#我要报名 随便输个房名\n" +
                "#我要退房 随便输个房名\n" +
                "#我要出牌 33\n" +
                "#我要出牌 34567\n" +
                "#我要看牌\n";
        CmdUtil.sendMessage(rule, userId, id, type);
    }


}
