package com.las.bot.cmd.translation;


import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.StrUtils;
import com.las.utils.mirai.CmdUtil;

import java.util.ArrayList;

@BotCmd(funName = "测试翻译", funWeight = 999)
public class TestRecordCommand extends BaseCommand {


    public TestRecordCommand() {
        super("复读", "");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        String content = null;
        if (command.startsWith("复读")) {
            content = command.substring(2);
        }
        if (StrUtils.isNotBlank(content)) {
            CmdUtil.sendMessage(content.trim(), userId, id, type);
        }
    }

}
