package com.las.bot.cmd.translation;


import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.OSSClient;
import com.las.bot.common.Constant;
import com.las.utils.JsonUtils;
import com.utils.*;
import com.utils.youdao.RecordUtil;
import com.utils.youdao.TTSUtil;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.net.Socket;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;

import java.util.HashMap;
import java.util.Map;

@BotCmd(funName = "翻译功能", funWeight = 0)
public class RecordCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(RecordCommand.class);

    private String[] all = {"日语翻译", "德语翻译", "俄语翻译", "法语翻译", "韩语翻译", "英语翻译", "意大利语翻译"};

    private String[] allAI = {"宁宁翻译", "七海翻译"};


    public RecordCommand() {
        super("翻译功能", "日语翻译", "德语翻译", "俄语翻译", "法语翻译", "韩语翻译", "英语翻译", "意大利语翻译", "宁宁翻译", "七海翻译");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        if (command.startsWith("翻译功能")) {
            String cStr = Arrays.toString(all);
            CmdUtil.sendMessage("目前支持的语言有：" + cStr + "\n\n输入指令：#日语翻译 你好", userId, id, type);
            CmdUtil.sendMessage("另外还有AI翻译：" + Arrays.toString(allAI) + "\n\n输入指令：#宁宁翻译 我喜欢你", userId, id, type);
            return;
        }
        Object obj = RedisUtils.get("funfy:" + userId);
        if (ObjectUtil.isNotNull(obj)) {
            CmdUtil.sendMessage("操作过于频繁，请30秒后再试~", userId, id, type);
            return;
        }
        try {
            String q = "";
            if (args.size() > 0) {
                q = args.get(0);
            }
            if (StrKit.notBlank(q)) {
                if (q.length() > 50) {
                    CmdUtil.sendMessage("翻译内容不能超过50字", userId, id, type);
                    return;
                }
                RedisUtils.set("funfy:" + userId, userId, 30);
                String langType = "";
                if (command.startsWith("日语翻译")) {
                    langType = "ja";
                }
                if (command.startsWith("德语翻译")) {
                    langType = "de";
                }
                if (command.startsWith("俄语翻译")) {
                    langType = "ru";
                }
                if (command.startsWith("法语翻译")) {
                    langType = "fr";
                }
                if (command.startsWith("韩语翻译")) {
                    langType = "ko";
                }
                if (command.startsWith("英语翻译")) {
                    langType = "en";
                }
                if (command.startsWith("意大利语翻译")) {
                    langType = "it";
                }

                String AI = "";
                String index = "";
                if (command.startsWith("宁宁翻译")) {
                    index = "0";
                    AI = "nene";

                }
                if (command.startsWith("七海翻译")) {
                    index = "6";
                    AI = "nana";
                }

                if (StrKit.notBlank(langType)) {
                    String content = RecordUtil.getTranslation(q, langType);
                    CmdUtil.sendMessage("译文：" + content, userId, id, type);
                    String url = TTSUtil.getUrl(content, langType);
                    if (type != 1) {
                        CmdUtil.sendMessage("在群里使用有QQ语音播放", userId, id, type);
                    } else {
                        CmdUtil.sendVoiceorImgMessage(url, userId, id, type, 1);
                    }
                } else {
                    if (StrKit.notBlank(AI)) {
                        String content = RecordUtil.getTranslation(q, "ja");
                        String newText = URLEncoder.encode(content,"UTF-8");
                        CmdUtil.sendMessage("译文：" + content, userId, id, type);
                        if (type != 1) {
                            CmdUtil.sendMessage("在群里使用有QQ语音播放", userId, id, type);
                        } else {
                            CmdUtil.sendMessage("AI语音正在合成ing，请耐心等待~", userId, id, type);
                            try {
                                if (StrKit.notBlank(content)) {
                                    String url = "http://vits.natapp1.cc/sovits/getAvByText?mId=1&rId=" + index + "&text=" + newText;
                                    File file = DownFileUtils.getFile(url);
                                    String objectName = StrUtils.getUUID() + Constant.BUCKET_MP3;
                                    String endpoint = Constant.HTTPS + Constant.OSS;
                                    String accessKeyId = Constant.BUCKET_ID;
                                    String accessKeySecret = Constant.BUCKET_KEY;
                                    String bucketName = Constant.BUCKET_NAME;
                                    // 上传到阿里云OSS
                                    OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
                                    ossClient.putObject(bucketName, objectName, file);
                                    ossClient.shutdown();
                                    String filePath = Constant.HTTPS + Constant.BUCKET_NAME + "." + Constant.OSS + objectName;
                                    // 最后删掉file文件
                                    if (file.exists()) {
                                        file.delete();
                                    }
                                    CmdUtil.sendVoiceorImgMessage(filePath, userId, id, type, 1);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        CmdUtil.sendMessage("AI还在训练模型ing", userId, id, type);
                    }
                }
            }
        } catch (Exception ignored) {
            logger.info(super.toString() + "执行时报错，命令内容:" + command);
        }
    }


}
