package com.las.bot.cmd.qa;

import com.las.annotation.BotCmd;
import com.las.bot.dao.QaDao;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import com.model.Qa;
import com.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

@BotCmd(funName = "问卷测试")
public class QaEndCommand extends BaseCommand {

    @Autowired
    private QaDao qaDao;

    public QaEndCommand() {
        super("结束测试", "终止测试");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        Object obj = RedisUtils.get("qa:tag:" + userId);
        if (null != obj) {
            String num = obj.toString().split("#")[0];
            Qa qa = qaDao.findById(num);
            RedisUtils.del("qa:tag:" + userId);
            RedisUtils.del("qa:score:" + userId);
            obj = RedisUtils.get("qa:tag:" + userId);
            if (null == obj) {
                CmdUtil.sendMessage("你当前的测试《" + qa.getTitle() + "》已结束", userId, id, type);
            }
        } else {
            CmdUtil.sendMessage("你未参加过测试", userId, id, type);
        }

    }

}
