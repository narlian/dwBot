package com.las.bot.cmd.qa;

import com.las.bot.common.Constant;
import com.las.bot.dao.QaDao;
import com.las.bot.dao.QaOptionDao;
import com.model.Qa;
import com.model.QaOption;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.StrUtils;
import com.las.utils.mirai.CmdUtil;
import com.utils.RedisUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@BotCmd(funName = "问卷测试")
public class QaCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(QaCommand.class);

    @Autowired
    private QaDao qaDao;

    @Autowired
    private QaOptionDao qaOptionDao;

    public QaCommand() {
        super("问卷测试", "测试");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        if (type == Constant.MESSAGE_TYPE_GROUP) {
            CmdUtil.sendMessage("请私聊机器人发送该指令", userId, id, type);
            return;
        }
        Object obj = RedisUtils.get("qa:tag:" + userId);
        if (null != obj) {
            String num = obj.toString().split("#")[0];
            Qa qa = qaDao.findById(num);
            CmdUtil.sendMessage("你当前的测试《" + qa.getTitle() + "》未做完\n可以输入指令：#结束测试", userId, id, type);
            return;
        }
        StringBuilder sb = new StringBuilder();
        List<Qa> qaList = qaDao.findList();
        List<Long> ids = qaList.stream().map(Qa::getId).collect(Collectors.toList());
        long n = -1L;
        if (args.size() > 0) {
            String number = args.get(0);
            if (StrUtils.isNumeric(number)) {
                n = Long.parseLong(number);
            }
            if (n <= 0) {
                CmdUtil.sendMessage("编号不对，请检查", userId, id, type);
                return;
            }
            if (!ids.contains(n)) {
                CmdUtil.sendMessage("编号不存在，请检查", userId, id, type);
                return;
            }
            // 确保参数没问题开始设置缓存标记，后面由非匹配指令去匹配
            List<QaOption> list = qaOptionDao.findListByQaId(n);
            QaOption option = list.get(0);
            Integer opNum = option.getOpNum();
            if (opNum != 1) {
                CmdUtil.sendMessage("选项1不存在，请检查", userId, id, type);
                return;
            }
            String opTitle = option.getOpTitle();
            sb.append(opNum).append("、").append(opTitle).append("\n");
            String opJson = option.getOpJson();
            String[] sp = opJson.split(",");
            for (int i = 0; i < sp.length; i++) {
                if (sp[i].contains("#")) {
                    String opStr = sp[i].split("#")[0];
                    String toStr = sp[i].split("#")[1];
                    sb.append(i + 1).append("、").append(opStr).append("(跳第").append(toStr).append("题)\n");
                } else {
                    String opStr = sp[i].split("-")[0];
                    String toStr = sp[i].split("-")[1];
                    sb.append(i + 1).append("、").append(opStr).append("(加").append(toStr).append("分)\n");
                }
            }
            sb.append("回复选项值：1/2/3/4\n");
            sb.append("\n测试时间默认是60分钟，超时会自动结束测试\n若不想测试也可以输入指令：#结束测试");
            boolean tag = RedisUtils.set("qa:tag:" + userId, n + "#" + opNum, 3600);
            if (!tag) {
                CmdUtil.sendMessage("服务出现异常，请重试", userId, id, type);
                return;
            }
            RedisUtils.set("qa:score:" + userId, 0, 3600);

        } else {
            // 没有参数则需要展示已上架的全部问卷
            sb.append("问卷测试题有：\n");
            for (Qa qa : qaList) {
                Long qaId = qa.getId();
                String name = qa.getTitle();
                sb.append("测试").append(qaId).append("：").append(name).append("\n");
            }
            sb.append("\n选择其中一个测试题开始测试吧！\n输入指令：#测试 上面出现的编号");

        }

        if (StrUtils.isNotBlank(sb.toString())) {
            CmdUtil.sendMessage(sb.toString(), userId, id, type);
        }

    }

}
