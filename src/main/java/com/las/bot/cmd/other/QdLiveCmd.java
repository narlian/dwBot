package com.las.bot.cmd.other;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.las.annotation.BotCmd;
import com.las.bot.common.Constant;
import com.las.bot.dao.QdLivedDao;
import com.las.cmd.BaseNonCommand;
import com.las.config.AppConfigs;
import com.las.utils.mirai.CmdUtil;
import com.model.QdLived;
import com.utils.JsonUtils;
import com.utils.RedisUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;

@BotCmd(funName = "秋蒂直播提醒", isMatch = false)
public class QdLiveCmd extends BaseNonCommand {

    private static Logger logger = Logger.getLogger(QdLiveCmd.class);

    @Autowired
    private QdLivedDao qdLivedDao;


    @Override
    public void execute(JSONObject obj, Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        String keyStr1 = "qd:live:24678";
        String keyStr2 = "qd:lived:24678";
        if (Constant.MESSAGE_TYPE_GROUP == type) {
            //每次有群里聊天信息，检查缓存redis判断秋蒂直播提醒
            Object key = RedisUtils.get(keyStr1);
            if (ObjectUtil.isNull(key)) {
                RedisUtils.set(keyStr1, "455899334", 5 * 60);
                String backMsg = "";
                ArrayList<String> coverList = new ArrayList<>();
                String roomId = "22163937";
                String apiUrl = "https://api.live.bilibili.com/xlive/web-room/v1/index/getInfoByRoom?room_id=" + roomId;
                String result = HttpKit.get(apiUrl);
                JSONObject roomObj = JsonUtils.getJsonObjectByJsonString(result);
                String maxNum = "";
                if (roomObj.getIntValue("code") == 0) {
                    JSONObject data = roomObj.getJSONObject("data");
                    //logger.info("秋蒂直播信息：" + data.toJSONString());
                    String name = "秋蒂Q";
                    JSONObject liveRoom = data.getJSONObject("room_info");
                    maxNum = data.getJSONObject("watched_show").getString("text_large");
                    int liveStatus = liveRoom.getIntValue("live_status");
                    if (liveStatus == 1) {
                        StringBuilder sb = new StringBuilder();
                        String title = liveRoom.getString("title");
                        coverList.add(liveRoom.getString("cover"));
                        sb.append(name).append("开播啦！").append("\n");
                        sb.append("标题：").append(title).append("\n");
                        sb.append("直播间：").append("https://live.bilibili.com/22163937").append("\n");
                        backMsg = sb.toString();
                    }
                }
                Object key2 = RedisUtils.get(keyStr2);
                Long[] gs = {316651609L, 797583470L};
                if (StrUtil.isNotBlank(backMsg)) {
                    if (ObjectUtil.isNull(key2)) {
                        RedisUtils.set(keyStr2, "455899334", 6 * 60 * 60);
                        for (Long group : gs) {
                            CmdUtil.sendMessage(backMsg, Long.parseLong(AppConfigs.superQQ), group, Constant.MESSAGE_TYPE_GROUP);
                            if (CollectionUtil.isNotEmpty(coverList)) {
                                CmdUtil.sendImgMessage(coverList, Long.parseLong(AppConfigs.superQQ), group, Constant.MESSAGE_TYPE_GROUP);
                            }
                        }
                    }
                } else {
                    //没有消息，有可能是监听中。还需要检查key2不为空
                    if (ObjectUtil.isNotNull(key2)) {
                        RedisUtils.del(keyStr2);
                        for (Long group : gs) {
                            CmdUtil.sendMessage("秋蒂Q下播啦~\n大概统计：" + maxNum, Long.parseLong(AppConfigs.superQQ), group, Constant.MESSAGE_TYPE_GROUP);
                        }
                        try {
                            int num = -1;
                            String numStr = maxNum.replaceAll("人看过", "");
                            num = Integer.parseInt(numStr);
                            if (num >= 0) {
                                QdLived qdLived = new QdLived();
                                qdLived.setNum(num);
                                qdLived.setTime(new Date());
                                qdLivedDao.saveOrUpdate(qdLived);
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

    }


}
