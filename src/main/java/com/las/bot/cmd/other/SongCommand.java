package com.las.bot.cmd.other;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.dto.music.Music163AlDTO;
import com.las.dto.music.Music163DTO;
import com.las.utils.NetEaseMusicUtil;
import com.las.utils.mirai.CmdUtil;
import com.las.utils.JsonUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@BotCmd(funName = "点歌功能")
public class SongCommand extends BaseCommand {

    private static Logger logger = Logger.getLogger(SongCommand.class);

    public SongCommand() {
        super("点歌", "");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        logger.info("自己的点歌功能");
        //开始实现点歌功能
        String songName = null;
        if (args.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String s : args) {
                sb.append(s.trim()).append("丨");
            }
            sb.delete(sb.lastIndexOf("丨"), sb.length());
            songName = sb.toString();
        }
        if (StrKit.notBlank(songName)) {
            Map<String, String> params = new HashMap<>();
            params.put("songName", songName);
            String search = HttpKit.post("http://vits.natapp1.cc/music/search", params, null);
            //String search = NetEaseMusicUtil.search(songName);
            logger.info("搜索云歌曲结果是：" + search);
            JSONObject object = JsonUtils.getJsonObjectByJsonString(search);
            assert object != null;
            if (null != object.get("result")) {
                JSONObject obj = JsonUtils.getJsonObject(object.get("result"));
                if (null != obj.get("songs")) {
                    JSONArray array = obj.getJSONArray("songs");
                    if (!array.isEmpty()) {
                        JSONObject song = JsonUtils.getJsonObject(array.get(0));//默认就取第一首歌
                        Music163DTO dto = new Music163DTO();
                        dto.setId(song.getString("id"));
                        dto.setAr(song.getJSONArray("ar"));
                        dto.setName(song.getString("name"));
                        Music163AlDTO alDTO = new Music163AlDTO();
                        alDTO.setPicUrl(song.getJSONObject("al").getString("picUrl"));
                        dto.setAl(alDTO);
                        CmdUtil.send163MusicMessage(dto, userId, id, type);
                    }
                } else {
                    String msg = songName + " 找不到该歌曲>.<";
                    CmdUtil.sendMessage(msg, userId, id, type);
                }
            } else {
                String msg = songName + " 找不到该歌曲>.<";
                CmdUtil.sendMessage(msg, userId, id, type);
            }
        } else {
            String msg = "请在指令后面带歌曲名>.<";
            CmdUtil.sendMessage(msg, userId, id, type);
        }
    }
}
