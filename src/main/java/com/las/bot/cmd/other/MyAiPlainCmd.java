package com.las.bot.cmd.other;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.bot.dao.SovitsDao;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import com.model.SovitsSetting;
import com.utils.HttpUtils;
import com.utils.JsonUtils;
import com.utils.ThreadUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

//@BotCmd(funName = "AI绘画", isMatch = false)
public class MyAiPlainCmd extends BaseCommand {

    private static Logger logger = Logger.getLogger(MyAiPlainCmd.class);

    private ThreadPoolExecutor executor = ThreadUtil.getPool();

    @Autowired
    private SovitsDao sovitsDao;

    public MyAiPlainCmd() {
        super("绘画", "绘图","HT");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        SovitsSetting setting = sovitsDao.findByName("gpt");
        if (ObjectUtil.isNotNull(setting)) {
            String key = setting.getCodeValue();
            executor.execute(() -> {
                Map<String, Object> params = new HashMap<>();
                params.put("size", "256x256");
                params.put("prompt", command.trim());
                params.put("response_format", "url");
                String jsonStr = JsonUtils.getJsonString(params);
                String result = HttpUtils.doPostByToken("https://openai.api2d.net/v1/images/generations", key.trim(), jsonStr);
                if (StrKit.notBlank(result)) {
                    logger.info("AI绘画响应结果：" + result);
                    JSONObject object = JSONObject.parseObject(result);
                    JSONArray array = object.getJSONArray("data");
                    if (CollectionUtil.isNotEmpty(array)) {
                        JSONObject msg = array.getJSONObject(0);
                        String content = msg.getString("url");
                        ArrayList<String> list = new ArrayList<>();
                        list.add(content);
                        CmdUtil.sendImgMessage(list, userId, id, type);
                    }
                }
            });
        }

    }


}
