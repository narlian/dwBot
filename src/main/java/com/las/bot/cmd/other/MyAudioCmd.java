package com.las.bot.cmd.other;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.bot.common.Constant;
import com.las.bot.dao.VitsModelDao;
import com.las.cmd.BaseNonCommand;
import com.las.utils.mirai.CmdUtil;
import com.model.VitsModel;
import com.utils.AiUtil;
import com.utils.DownFileUtils;
import com.utils.FileBase64Utils;
import com.utils.RedisUtils;
import com.utils.youdao.RecordUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

@BotCmd(funName = "AI语音", isMatch = false)
public class MyAudioCmd extends BaseNonCommand {

    private final Logger logger = Logger.getLogger(MyAudioCmd.class);

    @Autowired
    private VitsModelDao modelDao;

    @Override
    public void execute(JSONObject obj, Long userId, Long id, Integer type, String cmd, ArrayList<String> args) throws Exception {
        if (cmd.startsWith("#")) {
            cmd = cmd.replaceAll("#", "");
            String sc = "秋蒂说";
            String scj = "秋蒂日语说";
            String sc2 = "萝妮说";
            String scj2 = "萝妮日语说";
            int length = 0;
            boolean tag = false;
            String tagStr = "[ZH]";
            if (cmd.startsWith(sc) || cmd.startsWith(sc2)) {
                tag = true;
                length = sc.length();

            }
            if (cmd.startsWith(scj) || cmd.startsWith(scj2)) {
                tag = true;
                length = scj.length();
                tagStr = "[JA]";
            }
            if (tag) {
                String model;
                String config;
                String index = "0";
                int mId = 26;
                if (cmd.startsWith("秋蒂")) {
                    mId = 20;
                }
                VitsModel vitsModel = modelDao.findById(mId);
                model = vitsModel.getModelPath();
                config = vitsModel.getConfigPath();
                logger.info("触发AI秋蒂指令，开始复读");
                Object uObj = RedisUtils.get("aiaudio:" + userId);
                if (ObjectUtil.isNotNull(uObj)) {
                    CmdUtil.sendMessage("操作过于频繁，请30秒后再试~", userId, id, type);
                    return;
                }
                String q = cmd.substring(length);
                if (StrKit.notBlank(q)) {
                    if (q.length() > 50) {
                        CmdUtil.sendMessage("内容不能超过50字", userId, id, type);
                        return;
                    }
                    RedisUtils.set("aiaudio:" + userId, userId, 30);
                    if (tagStr.equals("[JA]")) {
                        q = RecordUtil.getTranslation(q, "ja");
                        //CmdUtil.sendMessage("译文：" + q, userId, id, type);
                    }
                    if (type != 1) {
                        CmdUtil.sendMessage("在群里使用有QQ语音播放", userId, id, type);
                    } else {
                        CmdUtil.sendMessage("AI语音正在合成ing，请耐心等待~", userId, id, type);
                        try {
                            String fId = AiUtil.getAvUrl(tagStr + q + tagStr, model, config, index);
                            if (StrKit.notBlank(fId)) {
                                String result = HttpUtil.post("http://vits.natapp1.cc/sovits/getBase64ById?fId=" + fId, (String) null);
                                JSONObject object = JSONObject.parseObject(result);
                                File file = new File(StrUtil.uuid());
                                FileBase64Utils.decodeBase64File(object.getString("data"), file.getAbsolutePath());
                                String url = DownFileUtils.upFileAndDel(file, Constant.BUCKET_MP3);
                                CmdUtil.sendVoiceorImgMessage(url, userId, id, type, 1);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }


        }
    }


}
