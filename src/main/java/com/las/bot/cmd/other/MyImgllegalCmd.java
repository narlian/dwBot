package com.las.bot.cmd.other;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.utils.ThreadUtil;
import com.jfinal.kit.HttpKit;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseNonCommand;
import com.las.utils.JsonUtils;
import com.las.utils.mirai.CmdUtil;
import com.las.utils.mirai.MiRaiUtil;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

@BotCmd(funName = "鉴定色图", isMatch = false)
public class MyImgllegalCmd extends BaseNonCommand {

    private static Logger logger = Logger.getLogger(MyImgllegalCmd.class);

    private ThreadPoolExecutor executor = ThreadUtil.getPool();

    @Override
    public void execute(JSONObject obj, Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        if (!command.contains("鉴定")) {
            return;
        }
        JSONArray msgChain = obj.getJSONArray("messageChain");
        if (CollectionUtil.isNotEmpty(msgChain)) {
            Long msgId = null;
            for (int i = 0; i < msgChain.size(); i++) {
                JSONObject jsonObject = msgChain.getJSONObject(i);
                String msgType = jsonObject.getString("type");
                if ("Source".equals(msgType)) {
                    msgId = jsonObject.getLong("id");
                    break;
                }
            }
            logger.info("开始鉴定色图功能>消息ID..." + msgId);
            if (null != msgId && msgId > 0) {
                for (int j = 0; j < msgChain.size(); j++) {
                    JSONObject jsonObject = msgChain.getJSONObject(j);
                    String msgType = jsonObject.getString("type");
                    if ("Image".equals(msgType)) {
                        String url = jsonObject.getString("url");
                        sendImageJudge(msgId, url, obj, userId, id);
                        break;
                    }
                }
            }
        }

    }

    private void sendImageJudge(Long msgId, String url, JSONObject cqMsg, Long userId, Long id) {
        int start = url.indexOf("//");
        int length = url.substring(0, start).length();
        String preFix = url.substring(0, length);
        String str = url.substring(length + 2);
        str = str.replaceAll("//", "/");
        String finalUrl = preFix + "//" + str;
        logger.info("鉴定色图最终url：" + finalUrl);
        executor.execute(() -> {
            Map<String, String> info = new HashMap<>();
            info.put("grant_type", "client_credentials");
            info.put("client_id", "TwGSLNbo6kYrOlApnKL5wZmN");
            info.put("client_secret", "ldUZ8h0lG3t49Ripj4URFZIW3TGcyeR4");
            String result = HttpKit.post("https://aip.baidubce.com/oauth/2.0/token", info, null);
            JSONObject object = JsonUtils.getJsonObjectByJsonString(result);
            String token = object.getString("access_token");

            info = new HashMap<>();
            info.put("access_token", token);
            info.put("imgUrl", finalUrl);
            String result2 = HttpKit.post("https://aip.baidubce.com/rest/2.0/solution/v1/img_censor/v2/user_defined", info, null);
            JSONObject object2 = JsonUtils.getJsonObjectByJsonString(result2);
            String judge = object2.getString("conclusion");

            String msg = "该图判定结果是：" + judge;
            Double v = null;
            String dataMsg = null;
            boolean tag = false;
            if ("不合规".equals(judge) || "疑似".equals(judge)) {
                v = object2.getJSONArray("data").getJSONObject(0).getDouble("probability");
                dataMsg = object2.getJSONArray("data").getJSONObject(0).getString("msg");
                String data = String.format("%.2f", v * 100) + "%";
                msg = msg + "\n违规原因：" + dataMsg;
                msg = msg + "\n鉴定值：" + data;
            }
            logger.info("判定结果是：" + msg);
            logger.info("消息类型：" + cqMsg.getString("type"));

            switch (cqMsg.getString("type")) {
                case "FriendMessage":
                    CmdUtil.sendMessage(msg, userId, id, com.las.common.Constant.MESSAGE_TYPE_PRIVATE);
                    break;
                case "GroupMessage":
                    // (具体数值范围后续读取群配置，例如0.6#是 后面的'是'表示撤回消息开启)
//                    String config = "0.6#是";
//                    String[] split = config.split("#");
//                    double limit = Double.parseDouble(split[0]);
//                    if (split[1].equals("是")) {
//                        tag = true;
//                    }
//                    if ("不合规".equals(judge)) {
//                        // 还需要判断鉴定值大于90%的色情色图才发到群里，以免刷屏
//                        if (null != v && v > limit) {
//                            sendJudge90Msg(msgId, msg, dataMsg, userId, id, tag);
//                        } else {
//                            // 0.2是定制的，不许改
//                            if (null != v && v > 0.2) {
//                                sendJudge20Msg(msg, dataMsg, userId, id);
//                            }
//                        }
//                    } else if ("疑似".equals(judge)) {
//                        if (null != v && v > 0.2) {
//                            //sendJudge20Msg(msg, dataMsg, userId, id);
//                            //疑似的不用撤回
//                            sendJudge90Msg(null, msg, dataMsg, userId, id, tag);
//                        }
//                    }

                    CmdUtil.sendAtMessage(msg, userId, userId, id, com.las.common.Constant.MESSAGE_TYPE_GROUP);

                    break;
                case "TempMessage":
                    CmdUtil.sendMessage(msg, userId, id, com.las.common.Constant.MESSAGE_TYPE_DISCUSS);
                    break;
                default:
                    break;
            }
        });
    }


    private void sendJudge90Msg(Long msgId, String msg, String dataMsg, Long userId, Long id, boolean tag) {
        if (dataMsg.contains("卡通女性性感") || dataMsg.contains("卡通色情") || dataMsg.contains("女性性感")) {
            CmdUtil.sendAtMessage(msg, userId, userId, id, com.las.common.Constant.MESSAGE_TYPE_GROUP);
        }

        if (dataMsg.contains("一般色情")) {
            CmdUtil.sendAtMessage(msg, userId, userId, id, com.las.common.Constant.MESSAGE_TYPE_GROUP);
        }
        if (tag && ObjectUtil.isNotNull(msgId)) {
            // 并且撤回群员消息，后续会读配置
            logger.info("色图来源消息ID是：" + msgId);
            MiRaiUtil.getInstance().reCall(msgId);
        }

    }


    private void sendJudge20Msg(String msg, String dataMsg, Long userId, Long id) {
        if (dataMsg.contains("卡通色情")) {
            CmdUtil.sendAtMessage(msg, userId, userId, id, com.las.common.Constant.MESSAGE_TYPE_GROUP);
        }

        if (dataMsg.contains("一般色情")) {
            CmdUtil.sendAtMessage(msg, userId, userId, id, com.las.common.Constant.MESSAGE_TYPE_GROUP);
        }

    }


}
