package com.las.bot.cmd.other;

import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

@BotCmd(funName = "菜单帮助")
public class HelpCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(HelpCommand.class);


    public HelpCommand() {
        super("help", "", "", "机器人", "菜单", "帮助", "？", "?");
    }

    @Override
    public void execute(Long userId, Long id, Integer type,String command, ArrayList<String> args) {
        CmdUtil.sendMessage("更多指令可以查看文档：https://gitee.com/dullwolf/dwBot", userId, id, type);
    }

}
