package com.las.bot.cmd.other;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.bot.dao.SovitsDao;
import com.las.cmd.BaseNonCommand;
import com.las.config.AppConfigs;
import com.las.nogu.commond.Constant;
import com.las.utils.mirai.CmdUtil;
import com.las.utils.mirai.MiRaiUtil;
import com.model.SovitsSetting;
import com.utils.HttpUtils;
import com.utils.JsonUtils;
import com.utils.RedisUtils;
import com.utils.ThreadUtil;
import com.utils.youdao.RecordUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

@BotCmd(funName = "AI聊天", isMatch = false)
public class MyAiCmd extends BaseNonCommand {

    private static Logger logger = Logger.getLogger(MyAiCmd.class);

    private ThreadPoolExecutor executor = ThreadUtil.getPool();

    @Autowired
    private SovitsDao sovitsDao;

    @Override
    public void execute(JSONObject obj, Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        logger.info("开始检查AT机器人聊天功能...");
        JSONArray msgChain = obj.getJSONArray("messageChain");
        if (CollectionUtil.isNotEmpty(msgChain)) {
            Long msgId = null;
            for (int i = 0; i < msgChain.size(); i++) {
                JSONObject jsonObject = msgChain.getJSONObject(i);
                String msgType = jsonObject.getString("type");
                if ("Source".equals(msgType)) {
                    msgId = jsonObject.getLong("id");
                    break;
                }
            }
            logger.info("AI聊天功能>消息ID..." + msgId);
            if (null != msgId && msgId > 0) {
                for (int j = 0; j < msgChain.size(); j++) {
                    JSONObject jsonObject = msgChain.getJSONObject(j);
                    String msgType = jsonObject.getString("type");
                    String target = jsonObject.getString("target");
                    if ("At".equals(msgType) && AppConfigs.botQQ.equals(target)) {
                        sendChatInfo(command, userId, id, type);
                        break;
                    }
                }
            }
        }

    }

    private void sendChatInfo(String command, Long userId, Long id, Integer type) {
        logger.info("机器人聊天接受信息：" + command);
        SovitsSetting setting = sovitsDao.findByName("gpt");
        SovitsSetting rule = sovitsDao.findByName("gptRule");
        if (ObjectUtil.isNotNull(setting) && ObjectUtil.isNotNull(rule)) {
            String key = setting.getCodeValue();
            String ruleStr = rule.getCodeValue();
            executor.execute(() -> {
                Map<String, Object> params = new HashMap<>();
                List<JSONObject> list = new ArrayList<>();
                JSONObject msgObj = new JSONObject();
                msgObj.put("role", "user");
                msgObj.put("content", command.trim() + ruleStr);
                list.add(msgObj);
                params.put("messages", list);
                params.put("model", "gpt-3.5-turbo");
                String jsonStr = JsonUtils.getJsonString(params);
                String result = HttpUtils.doPostByToken("https://openai.api2d.net/v1/chat/completions", key.trim(), jsonStr);
                if (StrKit.notBlank(result)) {
                    logger.info("gpt响应结果：" + result);
                    JSONObject object = JSONObject.parseObject(result);
                    JSONArray array = object.getJSONArray("choices");
                    if (CollectionUtil.isNotEmpty(array)) {
                        JSONObject msg = array.getJSONObject(0);
                        String content = msg.getJSONObject("message").getString("content");
                        CmdUtil.sendMessage(content, userId, id, type);
                    }
                }
            });
        }

    }


}
