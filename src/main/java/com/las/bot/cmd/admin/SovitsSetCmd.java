package com.las.bot.cmd.admin;

import cn.hutool.core.util.ObjectUtil;
import com.las.annotation.BotCmd;
import com.las.bot.common.Constant;
import com.las.bot.dao.VitsUserDao;
import com.las.cmd.BaseCommand;
import com.las.config.AppConfigs;
import com.las.utils.mirai.CmdUtil;
import com.model.VitsUser;
import com.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.UUID;

@BotCmd(funName = "VITS社区功能")
public class SovitsSetCmd extends BaseCommand {

    public SovitsSetCmd() {
        super("SOV", "");
    }

    @Autowired
    private VitsUserDao vitsUserDao;

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        String cmd = "";
        if (args.size() > 0) {
            cmd = args.get(0).trim();
        } else {
            //默认就是验证码
            cmd = "验证码";
        }
        if ("重置验证码".equals(cmd)) {
            String sessionCode = UUID.randomUUID().toString();
            RedisUtils.set("VITS:AUTH:USER:" + userId, sessionCode);
            if (type == Constant.MESSAGE_TYPE_GROUP) {
                CmdUtil.sendAtMessage(sessionCode, userId, userId, id, type);
            } else {
                CmdUtil.sendMessage(sessionCode, userId, id, type);
            }
        }
        if ("验证码".equals(cmd)) {
            String sessionCode = UUID.randomUUID().toString();
            Object obj = RedisUtils.get("VITS:AUTH:USER:" + userId);
            if (ObjectUtil.isNull(obj)) {
                RedisUtils.set("VITS:AUTH:USER:" + userId, sessionCode);
            } else {
                sessionCode = obj.toString();
            }
            if (type == Constant.MESSAGE_TYPE_GROUP) {
                CmdUtil.sendAtMessage(sessionCode, userId, userId, id, type);
            } else {
                CmdUtil.sendMessage(sessionCode, userId, id, type);
            }
        }
        if ("特权".equals(cmd)) {
            if (!String.valueOf(userId).equals(AppConfigs.superQQ)) {
                CmdUtil.sendMessage("必须是超管才可以操作", userId, id, type);
                return;
            }
            String qq = args.get(1);
            String code = args.get(2);
            String token = UUID.randomUUID().toString();
            VitsUser vitsUser = vitsUserDao.findByUserAndCode(qq, code);
            if (ObjectUtil.isNull(vitsUser)) {
                vitsUser = new VitsUser();
            }
            vitsUser.setUserId(qq);
            vitsUser.setCode(code);
            vitsUser.setToken(token);
            vitsUserDao.saveOrUpdate(vitsUser);
            if (type == Constant.MESSAGE_TYPE_GROUP) {
                CmdUtil.sendAtMessage(token, userId, userId, id, type);
            } else {
                CmdUtil.sendMessage(token, userId, id, type);
            }
        }

        if ("重置规则".equals(cmd)) {
            if (!String.valueOf(userId).equals(AppConfigs.superQQ)) {
                CmdUtil.sendMessage("必须是超管才可以操作", userId, id, type);
                return;
            }
            RedisUtils.del(com.las.nogu.commond.Constant.GPT_SESSION);
            CmdUtil.sendMessage("重置成功", userId, id, type);
        }

    }


}
