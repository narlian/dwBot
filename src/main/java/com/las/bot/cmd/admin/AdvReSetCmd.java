package com.las.bot.cmd.admin;

import com.las.annotation.BotCmd;
import com.las.bot.dao.GroupUserDao;
import com.las.cmd.BaseCommand;
import com.las.common.Constant;
import com.las.utils.mirai.CmdUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

//@BotCmd(funName = "群广告屏蔽", funWeight = 997)
public class AdvReSetCmd extends BaseCommand {

    private static Logger logger = Logger.getLogger(AdvReSetCmd.class);

    @Autowired
    private GroupUserDao groupUserDao;

    public AdvReSetCmd() {
        super("群广告屏蔽", "GC");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        if (Constant.MESSAGE_TYPE_GROUP == type) {
            boolean tag = groupUserDao.delByGId(id);
            if (tag) {
                CmdUtil.sendMessage("更新成功", userId, id, type);
            }
        }
    }


}
