package com.las.bot.cmd.admin;

import cn.hutool.core.collection.CollectionUtil;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.common.Constant;
import com.las.model.GroupFun;
import com.las.utils.mirai.CmdUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.las.bot.common.Constant.MY_TEST_GID;

@BotCmd(funName = "获取所有功能", funWeight = 998)
public class FunListCmd extends BaseCommand {

    private List<String> list = Arrays.asList("秋蒂直播提醒","群关闭功能", "群开启功能", "获取所有功能", "超管功能", "管理员功能","保存色图","测试翻译");

    public FunListCmd() {
        super("所有功能", "ALLFUN");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        if (Constant.MESSAGE_TYPE_GROUP == type) {
            List<GroupFun> groupFuns = getGroupFunDao().findListByGid(id);
            StringBuilder sb = new StringBuilder();
            if (CollectionUtil.isNotEmpty(groupFuns)) {
                groupFuns.stream().filter(gf -> !list.contains(gf.getGroupFun())).forEach(gf -> {
                    String groupFun = gf.getGroupFun();
                    Integer isEnable = gf.getIsEnable();
                    String str = isEnable == 1 ? "已开启" : "已关闭";
                    sb.append(groupFun).append("(").append(str).append("),编号：").append(gf.getId()).append("\n");
                });
            }
            CmdUtil.sendMessage(sb.toString(), userId, MY_TEST_GID, Constant.MESSAGE_TYPE_GROUP);
        }
    }
}
