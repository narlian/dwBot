package com.las.bot.cmd.admin;

import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.common.Constant;
import com.las.model.GroupFun;
import com.las.utils.mirai.CmdUtil;
import com.las.utils.StrUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@BotCmd(funName = "群关闭功能", funWeight = 998)
public class FunCloseCmd extends BaseCommand {


    public FunCloseCmd() {
        super("关闭", "CLOSE");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        if (Constant.MESSAGE_TYPE_GROUP == type) {
            if (args.size() > 0) {
                String number = args.get(0);
                if (StrUtils.isNumeric(number)) {
                    List<GroupFun> groupFunList = getGroupFunDao().findListByGid(id);
                    List<Long> ids = groupFunList.stream().map(GroupFun::getId).collect(Collectors.toList());
                    long num = Long.parseLong(number);
                    if (!ids.contains(num)) {
                        CmdUtil.sendMessage("编号：" + num + "，不存在", userId, id, type);
                        return;
                    }
                    GroupFun groupFun = new GroupFun();
                    groupFun.setId(Long.parseLong(number));
                    groupFun.setIsEnable(0);
                    groupFun.setGroupId(id);
                    getGroupFunDao().saveOrUpdate(groupFun);
                    CmdUtil.sendMessage("关闭成功", userId, id, type);
                }
            }
        }
    }
}
