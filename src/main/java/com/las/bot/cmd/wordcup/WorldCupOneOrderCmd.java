package com.las.bot.cmd.wordcup;

import cn.hutool.core.collection.CollectionUtil;
import com.google.common.collect.Lists;
import com.las.annotation.BotCmd;
import com.las.bot.dto.WcOrderDTO;
import com.las.cmd.BaseCommand;
import com.las.config.AppConfigs;
import com.las.utils.mirai.CmdUtil;
import com.utils.worldcup.WorldCupOneUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@BotCmd(funName = "世界杯")
public class WorldCupOneOrderCmd extends BaseCommand {


    public WorldCupOneOrderCmd() {
        super("单场下注", "DC下注");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String cmd, ArrayList<String> args) throws Exception {
        if (!userId.equals(Long.parseLong(AppConfigs.superQQ))) {
            CmdUtil.sendMessage("必须是超管才可以操作", userId, id, type);
            return;
        }
        if (args.size() > 2) {
            double[] scoreA = new double[2];
            double[] scoreB = new double[args.size() - 2];
            String[] scoreBStr = new String[args.size() - 2];
            double z1 = Double.parseDouble(args.get(0));
            scoreA[0] = z1;
            double z2 = Double.parseDouble(args.get(1));
            scoreA[1] = z2;
            for (int i = 2; i < args.size(); i++) {
                String bfStr = args.get(i);
                String[] split = bfStr.split("#");
                String bf = split[0];
                scoreBStr[i - 2] = bf;
                double z3 = Double.parseDouble(split[1]);
                scoreB[i - 2] = z3;
            }

            List<WcOrderDTO> orderList = Lists.newArrayList();
            int size = 10;
            for (int i = 1; i <= size; i++) {
                for (int j = 1; j <= size; j++) {
                    for (int k = 1; k <= size; k++) {
                        Map<String, Object> order = WorldCupOneUtils.getInstance().getOrder(i, j, k, scoreA, scoreB, scoreBStr);
                        double totalCost = Double.parseDouble(order.get("cost").toString()) * 3;
                        double wCost = Double.parseDouble(order.get("wCost").toString());
                        double dCost = Double.parseDouble(order.get("dCost").toString());
                        double lCost = Double.parseDouble(order.get("lCost").toString());
                        double fx = (totalCost - (wCost + dCost + lCost)) / 3;
                        double mid = wCost < lCost ? wCost : lCost;
                        double last = mid < dCost ? mid : dCost;
                        double mfx = ((totalCost * 1.0) / 3) - last;
                        WcOrderDTO wcOrderDTO = new WcOrderDTO();
                        wcOrderDTO.setKey(i + "-" + j + "-" + k);
                        wcOrderDTO.setMaxFx(mfx);
                        wcOrderDTO.setFx(fx);
                        orderList.add(wcOrderDTO);
                    }
                }
            }
            List<WcOrderDTO> collect = orderList.stream().filter(wcOrderDTO -> (wcOrderDTO.getFx() < 0)).sorted((o1, o2) -> {
                int v1 = (int) (o1.getMaxFx() * 100.0);
                int v2 = (int) (o2.getMaxFx() * 100.0);
                return v1 - v2;
            }).collect(Collectors.toList());
            if (CollectionUtil.isNotEmpty(collect)) {
                WcOrderDTO wcOrderDTO = collect.get(0);
                String key = wcOrderDTO.getKey();
                String[] sp = key.split("-");
                int win = Integer.parseInt(sp[0]) * 100;
                int lost = Integer.parseInt(sp[1]) * 100;
                int pp = Integer.parseInt(sp[2]) * 100;
                Map<String, Object> orderMap = WorldCupOneUtils.getInstance().getOrder(win, lost, pp, scoreA, scoreB, scoreBStr);
                String reStr = orderMap.get("result").toString();
                Double maxFx = wcOrderDTO.getMaxFx() * 100;
                reStr += "\n最大亏损：" + String.format("%.2f", maxFx) + "元";
                CmdUtil.sendMessage(reStr, userId, id, type);
            } else {
                CmdUtil.sendMessage("不存在赢钱方案", userId, id, type);
            }

        }

    }


}
