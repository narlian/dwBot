package com.las.bot.cmd.osu;


import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

@BotCmd(funName = "OSU")
public class OsuHelpCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(com.las.bot.cmd.other.HelpCommand.class);


    public OsuHelpCommand() {
        super("osu", "", "");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        ArrayList<String> urls = new ArrayList<>();
        urls.add("https://dullwolf.oss-accelerate.aliyuncs.com/588119445038170112.png");
        CmdUtil.sendImgMessage(urls, userId, id, type);
        CmdUtil.sendMessage("更多指令可以查看文档：https://gitee.com/dullwolf/dwBot", userId, id, type);
    }

}
