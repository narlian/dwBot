package com.las.bot.cmd.osu;


import com.utils.osu.OSUApi;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

@BotCmd(funName = "OSU")
public class OsuHomeCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(OsuHomeCommand.class);

    @Autowired
    private OSUApi osuApi;

    public OsuHomeCommand() {
        super("home", "", "", "taiko", "mania", "ctb");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        String osuName = null;
        int mode = 0;
        if (command.startsWith("taiko")) {
            mode = 1;
        } else if (command.startsWith("ctb")) {
            mode = 2;
        } else if (command.startsWith("mania")) {
            mode = 3;
        }
        if (args.size() > 0) {
            //osuName = args.get(0);
            StringBuilder sb = new StringBuilder();
            for (String s : args) {
                sb.append(s).append(" ");
            }
            osuName = sb.toString();
        }
        osuName = osuApi.reset(userId, id, type, osuName);
        if (StrKit.notBlank(osuName)) {
            int ll = 0, pc = 0, tth = 0;
            String ranks = "↑0", accs = "+0", pps = "+0", rankeds = "+0";
            String msg = osuApi.sendOsuApi(osuName, ll, pc, tth, ranks, accs, pps, rankeds, mode, false);
            if (null != msg) {
                CmdUtil.sendMessage(msg, userId, id, type);
            }
        }
    }


}
