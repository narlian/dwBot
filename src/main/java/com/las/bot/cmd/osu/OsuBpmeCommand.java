package com.las.bot.cmd.osu;

import cn.hutool.core.collection.CollectionUtil;
import com.utils.JsonUtils;
import com.utils.osu.ApiUtil;
import com.utils.osu.OSUApi;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.utils.osu.ModUtil.getModeName;

@BotCmd(funName = "OSU")
public class OsuBpmeCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(OsuBpmeCommand.class);

    @Autowired
    private OSUApi osuApi;

    public OsuBpmeCommand() {
        super("bpme", "","", "tp", "mp", "cp");
    }


    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        String osuName = null;
        int mode = 0;
        if (command.startsWith("tp")) {
            mode = 1;
        } else if (command.startsWith("cp")) {
            mode = 2;
        } else if (command.startsWith("mp")) {
            mode = 3;
        }
        if (args.size() > 0) {
            osuName = args.get(0);
        }
        osuName = osuApi.reset(userId, id, type, osuName);
        if (StrKit.notBlank(osuName)) {
            List<Map> info = JsonUtils.getArrayByJson(ApiUtil.getBpApi(osuName, mode, 100), Map.class);
            if (CollectionUtil.isNotEmpty(info)) {
                StringBuilder sb = new StringBuilder();
                sb.append("玩家 ").append(osuName).append(" Top10的BP列表（快去下载爆他BP）：\n");
                sb.append("Mode：").append(getModeName(mode)).append("\n");
                for (int i = 0; i < 10; i++) {
                    if (info.size() < i + 1) {
                        break;
                    }
                    Map map = info.get(i);
                    sb.append("歌谱：'https://osu.ppy.sh/b/").append(map.get("beatmap_id").toString()).append("'");
                    double pp = Double.parseDouble(map.get("pp").toString());
                    sb.append(" PP：").append(Math.round(pp)).append("\n");
                }
                CmdUtil.sendMessage(sb.toString(), userId,id, type);
            } else {
                CmdUtil.sendMessage("玩家 " + osuName + " 没有BP数据记录！", userId,id, type);
            }
        }
    }


}
