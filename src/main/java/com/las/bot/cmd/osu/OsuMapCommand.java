package com.las.bot.cmd.osu;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.las.bot.dto.OsuMapDTO;
import com.model.OsuMap;
import com.utils.DateUtils;
import com.utils.JsonUtils;
import com.utils.osu.ApiUtil;
import com.utils.osu.OSUApi;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static com.utils.osu.ModUtil.acc;
import static com.utils.osu.ModUtil.getModeName;

@BotCmd(funName = "OSU")
public class OsuMapCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(OsuMapCommand.class);

    @Autowired
    private OSUApi osuApi;

    public OsuMapCommand() {
        super("map", "", "", "tm", "mm", "cm");
    }


    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        String osuName = null;
        int mode = 0;
        if (command.startsWith("tm")) {
            mode = 1;
        } else if (command.startsWith("cm")) {
            mode = 2;
        } else if (command.startsWith("mm")) {
            mode = 3;
        }
        if (args.size() > 0) {
            osuName = args.get(0);
        }
        osuName = osuApi.reset(userId, id, type, osuName);
        if (StrKit.notBlank(osuName)) {
            List<JSONObject> info = JsonUtils.getArrayByJson(ApiUtil.getBpApi(osuName, mode, 5), JSONObject.class);
            if (CollectionUtil.isNotEmpty(info)) {
                CmdUtil.sendMessage("正在为您找图ing...", userId, id, type);
                int i = (int) (Math.random() * info.size());
                JSONObject object = info.get(i);
                int mods = object.getInteger("enabled_mods");
                String beatmapId = object.getString("beatmap_id");
                double bpMe = object.getDouble("pp");
                String json = ApiUtil.getScoreWithMod(mods, mode, beatmapId);
                if (StrKit.notBlank(json)) {
                    List<JSONObject> list = JsonUtils.getArrayByJson(json, JSONObject.class);
                    if (CollectionUtil.isNotEmpty(list)) {
                        List<OsuMap> maps = osuApi.getOsuMaps(mode, bpMe, list);
                        if (CollectionUtil.isNotEmpty(maps)) {
                            int r = (int) (Math.random() * maps.size());
                            OsuMap osuMap = maps.get(r);
                            StringBuilder sb = new StringBuilder();
                            sb.append(osuName).append(" 您好，蠢狼找到了一张").append(getModeName(mode)).append("的歌谱！\n");
                            sb.append("歌谱：").append("https://osu.ppy.sh/b/").append(osuMap.getBid()).append("\n");
                            sb.append("作家：").append(osuMap.getCreator()).append("\n");

                            //新增需求，获取最新的版本和纠正难度
                            Long bid = osuMap.getBid();
                            OsuMapDTO mapDTO = osuApi.getMapByBid(bid);
                            if (ObjectUtil.isNotNull(mapDTO)) {
                                sb.append("难度：").append(acc(String.valueOf(mapDTO.getDifficultyRating()))).append(" ");
                                sb.append("[").append(mapDTO.getVersion()).append("]\n");
                            } else {
                                sb.append("难度：").append(acc(String.valueOf(osuMap.getDifficultyRating()))).append("\n");
                            }

                            String[] split = osuMap.getBpm().toString().split("\\.");
                            sb.append("BPM：").append(split[0]).append("\n");
                            int length = osuMap.getTotalLength();
                            sb.append("时长：").append(length / 60).append("分").append(length % 60).append("秒").append("\n");
                            sb.append("发布日期：").append(DateUtils.format(osuMap.getLastUpdated(), DateUtils.DAY)).append("\n");
                            sb.append("理想PP值：").append(osuMap.getMaxPp()).append("\n");
                            sb.append("[注]：尽可能FC，acc无限接近100%，就可以达到理想PP值咯！加油吧！");
                            CmdUtil.sendMessage(sb.toString(), userId, id, type);
                        } else {
                            CmdUtil.sendMessage("蠢狼机器人的资源不足，大家多点用recent指令吧！", userId, id, type);
                        }
                    }
                } else {
                    CmdUtil.sendMessage("ppy网络出现异常，请再试一次！", userId, id, type);
                }
            } else {
                CmdUtil.sendMessage("玩家 " + osuName + " 模式：" + getModeName(mode) + "的BP列表一个都没有(没有BP数据，无法推荐图)", userId, id, type);
            }
        }
    }


}
