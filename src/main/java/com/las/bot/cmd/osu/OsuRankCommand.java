package com.las.bot.cmd.osu;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.las.bot.common.Constant;
import com.utils.RedisUtils;
import com.utils.osu.OSUApi;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.utils.osu.ModUtil.getModeName;


@BotCmd(funName = "OSU")
public class OsuRankCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(OsuRankCommand.class);

    @Autowired
    private OSUApi osuApi;

    public OsuRankCommand() {
        super("rank", "", "", "tk", "mk", "ck");
    }


    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        if (Constant.MESSAGE_TYPE_GROUP != type) {
            CmdUtil.sendMessage("请在群里使用该命令", userId, id, type);
        } else {
            String osuName = null;
            int mode = 0;
            if (command.startsWith("tk")) {
                mode = 1;
            } else if (command.startsWith("ck")) {
                mode = 2;
            } else if (command.startsWith("mk")) {
                mode = 3;
            }
            if (args.size() > 0) {
                osuName = args.get(0);
            }
            osuName = osuApi.reset(userId, id, type, osuName);
            if (StrKit.notBlank(osuName)) {
                Set<Object> qq = RedisUtils.hkeys(Constant.OSU_USEGROUPTAG + id);
                if (CollectionUtil.isNotEmpty(qq)) {
                    List<String> osuNames = new ArrayList<>();
                    Set<Map.Entry<Object, Object>> entries = RedisUtils.hmget(Constant.OSU_USEGROUPTAG + id).entrySet();
                    entries.forEach(obj -> osuNames.add(obj.getValue().toString()));
                    StringBuilder sb = new StringBuilder();
                    for (String s : osuNames) {
                        sb.append("'").append(s).append("',");
                    }
                    sb.delete(sb.lastIndexOf(","), sb.length());
                    logger.info("查询OSU RANK拼接群里使用的所以玩家：" + sb.toString());
                    if (CollectionUtil.isNotEmpty(osuNames)) {
                        //开始查询数据库
                        JSONObject rankInfo = osuApi.getOsuRank(osuName, mode, sb.toString());
                        if (null != rankInfo) {
                            String rank = rankInfo.get("rownum").toString();
                            String total = rankInfo.get("total").toString();
                            String rs = "玩家 " + osuName + " 在本群排名第 " + rank.split("\\.")[0] + " 名\n\n" +
                                    "这个群总共有" + total + "人在玩 " + getModeName(mode) + " 模式\n\n" +
                                    "【注】总人数是指在群里使用过蠢狼机器人的才算";
                            CmdUtil.sendMessage(rs, userId, id, type);
                        } else {
                            CmdUtil.sendMessage(osuName + " 您好，请先用指令home 游戏名(其他模式：taiko/mania/ctb)", userId, id, type);
                        }
                    }
                }
            }
        }
    }


}
