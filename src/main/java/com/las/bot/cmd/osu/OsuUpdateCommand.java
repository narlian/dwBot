package com.las.bot.cmd.osu;


import com.utils.osu.OSUApi;
import com.jfinal.kit.StrKit;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

@BotCmd(funName = "OSU")
public class OsuUpdateCommand extends BaseCommand {

    private Logger logger = LoggerFactory.getLogger(OsuUpdateCommand.class);

    @Autowired
    private OSUApi osuApi;

    public OsuUpdateCommand() {
        super("update", "","", "ut", "um", "uc");
    }


    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) {
        String osuName = null;
        int mode = 0;
        if (command.startsWith("ut")) {
            mode = 1;
        } else if (command.startsWith("uc")) {
            mode = 2;
        } else if (command.startsWith("um")) {
            mode = 3;
        }
        if (args.size() > 0) {
            osuName = args.get(0);
        }
        osuName = osuApi.reset(userId, id, type, osuName);
        if (StrKit.notBlank(osuName)) {
            int ll = 0, pc = 0, tth = 0;
            String ranks = "↑0", accs = "+0", pps = "+0", rankeds = "+0";
            String msg = osuApi.sendOsuApi(osuName, ll, pc, tth, ranks, accs, pps, rankeds, mode, true);
            if (null != msg) {
                CmdUtil.sendMessage(msg, userId,id, type);
            }
        }
    }


}
