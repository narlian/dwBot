package com.las.bot.cmd.jisu;

import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;

import java.util.ArrayList;

@BotCmd(funName = "疫情")
public class YiQingTipCmd extends BaseCommand {

    public YiQingTipCmd() {
        super("疫情","新冠","病毒");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String cmd, ArrayList<String> args) throws Exception {
        CmdUtil.sendMessage("指令格式：[国家地区]疫情，例如：广州疫情、中国疫情、美国疫情、上海疫情、东京疫情...",userId,id,type);
    }
}
