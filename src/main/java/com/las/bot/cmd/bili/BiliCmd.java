package com.las.bot.cmd.bili;

import com.alibaba.fastjson.JSONObject;
import com.las.annotation.BotCmd;
import com.las.cmd.BaseNonCommand;
import com.las.utils.mirai.CmdUtil;
import com.utils.bilibili.BilibiliVideoUtil;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BotCmd(funName = "B站功能", isMatch = false)
public class BiliCmd extends BaseNonCommand {

    private static Logger logger = Logger.getLogger(BiliCmd.class);


    @Override
    public void execute(JSONObject obj, Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        logger.info("接受哔哩哔哩分享的数据：" + command);
        if (command.contains("https://b23.tv")) {
            String pattern = "b23.tv\\/([\\w]+)";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(command);
            if (m.find()) {
                String videoId = m.group(1);
                String url = "https://b23.tv/" + videoId; // B站视频链接
                Document document = Jsoup.parse(new URL(url), 30000);
                String info = document.toString();
                int index = info.indexOf("og:url");
                command = info.substring(index, index + 100);
            }
        }
        if (command.contains("https://www.bilibili.com/video")) {
            String pattern = "video\\/([\\w]+)";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(command);
            if (m.find()) {
                StringBuilder sb = new StringBuilder();
                ArrayList<String> coverList = new ArrayList<>();
                String videoId = m.group(1);
                logger.info("Q蒂派啾咪~，视频的ID是：" + videoId);
                String videoUrl = "https://www.bilibili.com/video/" + videoId; // B站视频链接
                String apiUrl = "https://api.bilibili.com/x/web-interface/view?bvid=" + BilibiliVideoUtil.getBvid(videoUrl); // API接口链接
                String json = BilibiliVideoUtil.getJson(apiUrl); // 获取API返回的JSON数据
                String coverUrl = BilibiliVideoUtil.getCoverUrl(json); // 获取视频封面URL
                String title = BilibiliVideoUtil.getTitle(json);
                long playCount = BilibiliVideoUtil.getPlayCount(json); // 获取视频播放量
                long likeCount = BilibiliVideoUtil.getLikeCount(json); // 获取视频点赞数
                long coinCount = BilibiliVideoUtil.getCoinCount(json);
                logger.info("视频的封面URL是：" + coverUrl);
                coverList.add(coverUrl);
                JSONObject owner = BilibiliVideoUtil.getOwner(json);
                String author = owner.getString("name");
                sb.append("视频：").append(videoId).append("\n");
                sb.append("作者：").append(author).append("\n");
                sb.append("标题：").append(title).append("\n");
                sb.append("播放量：").append(playCount).append("\n");
                sb.append("点赞数：").append(likeCount).append("\n");
                sb.append("硬币数：").append(coinCount).append("\n");
                CmdUtil.sendMessage(sb.toString(), userId, id, type);
                CmdUtil.sendImgMessage(coverList, userId, id, type);
            }
        }
    }


}
