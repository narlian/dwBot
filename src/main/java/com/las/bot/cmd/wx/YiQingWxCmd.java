package com.las.bot.cmd.wx;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.OSSClient;
import com.las.annotation.BotCmd;
import com.las.annotation.WxCmd;
import com.las.bot.common.Constant;
import com.las.cmd.BaseNonCommand;
import com.las.cmd.BaseWxCommand;
import com.las.utils.HttpUtils;
import com.las.utils.StrUtils;
import com.las.utils.WxCmdUtil;
import com.las.utils.mirai.CmdUtil;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WxCmd
public class YiQingWxCmd extends BaseWxCommand {

    private final Logger logger = Logger.getLogger(YiQingWxCmd.class);

    @Override
    public void execute(JSONObject msgData, String wxId, String cmd) throws Exception {
        if (cmd.endsWith("疫情")) {
            cmd = cmd.replaceAll("疫情", "");
            cmd = cmd.replaceAll("#", "");
            String api = "https://c.m.163.com/ug/api/wuhan/app/data/list-total";
            String result = HttpUtils.doGet(api);
            if ("中国".equals(cmd)) {
                //timestamp
                JSONObject data = JSONObject.parseObject(result).getJSONObject("data");
                JSONObject json = data.getJSONObject("chinaTotal");
                json.put("lastUpdateTime", data.getString("lastUpdateTime"));
                sendMsg(wxId, json, "中国");
            } else {
                JSONArray array = JSONObject.parseObject(result).getJSONObject("data").getJSONArray("areaTree");
                List<JSONObject> all = getAll(array, new ArrayList<>());
                for (JSONObject json : all) {
                    String name = json.getString("name");
                    if (name.startsWith(cmd.trim())) {
                        logger.info("疫情统计信息：" + json.toJSONString());
                        sendMsg(wxId, json, name);
                        break;
                    }
                }
            }
        }
    }

    private void sendMsg(String wxId, JSONObject json, String name) {
        StringBuilder sb = new StringBuilder();
        JSONObject total = json.getJSONObject("total");
        JSONObject today = json.getJSONObject("today");
        JSONObject extData = json.getJSONObject("extData");
        String updateTime = json.getString("lastUpdateTime");
        sb.append(name).append("疫情：").append("\n");
        sb.append("累计确诊：").append(total.getIntValue("confirm")).append("\n");
        sb.append("累计死亡：").append(total.getIntValue("dead")).append("\n");
        int nowAll = total.getIntValue("confirm") - total.getIntValue("heal") - total.getIntValue("dead");
        sb.append("现有确诊：").append(nowAll).append("\n\n");
        sb.append("新增确诊：").append(Math.max(today.getIntValue("confirm"), 0)).append("\n");
        sb.append("新增无症状：").append(Math.max(extData.getIntValue("incrNoSymptom"), 0)).append("\n");
        sb.append("新增死亡：").append(Math.max(today.getIntValue("dead"), 0)).append("\n");
        sb.append("更新时间：").append(updateTime);
        //sb.append("\n数据来自网易：").append("c.m.163.com");
        WxCmdUtil.sendMsg(wxId,sb.toString());


    }


    private List<JSONObject> getAll(JSONArray array, List<JSONObject> list) {
        for (int i = 0; i < array.size(); i++) {
            JSONObject obj = array.getJSONObject(i);
            String name = obj.getString("name");
            if (name.startsWith("未明确") || name.startsWith("境外")) {
                continue;
            }
            list.add(obj);
            JSONArray children = obj.getJSONArray("children");
            if (CollectionUtil.isEmpty(children)) {
                list.add(obj);
            } else {
                getAll(children, list);
            }
        }
        return list;
    }
}
