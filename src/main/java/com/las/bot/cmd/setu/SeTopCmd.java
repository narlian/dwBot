package com.las.bot.cmd.setu;

import cn.hutool.core.collection.CollectionUtil;
import com.las.annotation.BotCmd;
import com.las.bot.dao.SeRewardDao;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import com.model.SeReward;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@BotCmd(funName = "抽卡功能")
public class SeTopCmd extends BaseCommand {

    private static Logger logger = LoggerFactory.getLogger(SeTopCmd.class);

    @Autowired
    private SeRewardDao seRewardDao;


    public SeTopCmd() {
        super("色图统计", "色图排行");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        List<SeReward> topList = seRewardDao.findTop();
        if (CollectionUtil.isNotEmpty(topList)) {
            StringBuilder sb = new StringBuilder();
            sb.append("本周获奖名单前五名：\n");
            for (SeReward seReward : topList) {
                Long qq = seReward.getUserId();
                String reward = seReward.getSeData();
                sb.append("QQ：").append(qq).append("，获得").append(reward).append(" 日元\n");
            }
            CmdUtil.sendMessage(sb.toString(), userId, id, type);
        } else {
            CmdUtil.sendMessage("暂无数据", userId, id, type);
        }
    }


}
