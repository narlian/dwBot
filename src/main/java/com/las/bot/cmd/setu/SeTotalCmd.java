package com.las.bot.cmd.setu;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.las.annotation.BotCmd;
import com.las.bot.common.Constant;
import com.las.bot.dao.CqMsgUrlDao;
import com.las.bot.dao.SeMemberDao;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;
import com.model.CqMsgUrl;
import com.model.SeMember;
import com.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@BotCmd(funName = "抽卡功能")
public class SeTotalCmd extends BaseCommand {

    private String[] rarity = {"E", "D", "C", "B", "A", "S", "SS", "SSS"};

    private Double[] rw = {0.0, 0.0, 0.0, 0.0, 0.005, 0.012, 0.025, 0.034};

    @Autowired
    private CqMsgUrlDao cqMsgUrlDao;

    @Autowired
    private SeMemberDao seMemberDao;


    public SeTotalCmd() {
        super("随机抽卡", "动漫抽卡", "二次元抽卡", "来份抽卡", "全部抽卡");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        List<CqMsgUrl> totalList = cqMsgUrlDao.getIw233AllTotal();
        Long total = cqMsgUrlDao.getIw233Total();

        StringBuilder sb2 = new StringBuilder();
        sb2.append("数据库目前总共有 ").append(total).append(" 张\n");
        for (int i = 0; i < totalList.size(); i++) {
            CqMsgUrl cqMsgUrl = totalList.get(i);
            if (cqMsgUrl.getGroupId().intValue() > 0 && i > 3) {
                sb2.append("稀有度：").append(rarity[i]).append("，有").append(cqMsgUrl.getUserId()).append("张(");
                MathContext mathContext = new MathContext(4, RoundingMode.HALF_UP);
                BigDecimal a = new BigDecimal(cqMsgUrl.getUserId());
                BigDecimal b = new BigDecimal(total);
                BigDecimal result = a.divide(b, mathContext);
                BigDecimal reward = new BigDecimal(rw[i]);
                double lastReward = reward.divide(result, mathContext).doubleValue();
                sb2.append("每张赏金≈").append(lastReward).append("日元)\n");
            }
        }
        // 检查用户是否充了会员
        SeMember member = seMemberDao.findByUserId(userId);
        if (ObjectUtil.isNull(member)) {
            sb2.append("您不是会员，无法抽卡！");
        } else {
            Date now = new Date();
            if (now.after(member.getTime())) {
                sb2.append("您的会员已过期！");
            } else {
                sb2.append("您的会员过期时间：").append(DateUtils.format(member.getTime()));
            }
        }
        sb2.append("\n抽卡网站：http://osu.natapp1.cc/luckCard/index.html?uId=").append(userId);
        if (StrUtil.isNotBlank(sb2.toString())) {
            if (type == Constant.MESSAGE_TYPE_GROUP) {
                CmdUtil.sendAtMessage(sb2.toString(), userId, userId, id, type);
            } else {
                CmdUtil.sendMessage(sb2.toString(), userId, id, type);
            }
        }

    }


}
