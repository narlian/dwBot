package com.las.bot.cmd.setu;

import com.las.annotation.BotCmd;
import com.las.cmd.BaseCommand;
import com.las.utils.mirai.CmdUtil;

import java.util.ArrayList;

@BotCmd(funName = "随机色图")
public class SeLookCmd extends BaseCommand {

    public SeLookCmd() {
        super("我的色图", "随机色图");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        String url = "http://osu.natapp1.cc/setu/index.html";
        CmdUtil.sendMessage("网站：" + url, userId, id, type);
    }


}
