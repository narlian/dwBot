package com.las.bot.cmd.setu;

import cn.hutool.core.util.ObjectUtil;
import com.las.annotation.BotCmd;
import com.las.bot.dao.SeMemberDao;
import com.las.cmd.BaseCommand;
import com.las.config.AppConfigs;
import com.las.utils.mirai.CmdUtil;
import com.model.SeMember;
import com.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;

@BotCmd(funName = "抽卡功能")
public class SeRechargeCmd extends BaseCommand {

    private static Logger logger = LoggerFactory.getLogger(SeRechargeCmd.class);

    @Autowired
    private SeMemberDao seMemberDao;

    public SeRechargeCmd() {
        super("色图充值", "色图会员", "色图续费");
    }

    @Override
    public void execute(Long userId, Long id, Integer type, String command, ArrayList<String> args) throws Exception {
        if (!userId.equals(Long.parseLong(AppConfigs.superQQ))) {
            CmdUtil.sendMessage("必须是超管才可以操作", userId, id, type);
            return;
        }
        if (args.size() > 0) {
            String param = args.get(0);
            String[] split = param.split("-");
            if (split.length == 2) {
                long qq = Long.parseLong(split[0]);
                int day = Integer.parseInt(split[1]);
                SeMember member = seMemberDao.findByUserId(qq);
                Date now = new Date();
                if (ObjectUtil.isNull(member)) {
                    member = new SeMember();
                    member.setUserId(qq);
                    Date date = DateUtils.addDay(now, day);
                    member.setTime(date);
                } else {
                    Date time = member.getTime();
                    // 还要比较该时间是否过期了
                    if (now.after(time)) {
                        Date date = DateUtils.addDay(now, day);
                        member.setTime(date);
                    } else {
                        Date date = DateUtils.addDay(time, day);
                        member.setTime(date);
                    }
                }
                seMemberDao.saveOrUpdate(member);
                CmdUtil.sendMessage("充值成功", userId, id, type);
            }
        }
    }


}
