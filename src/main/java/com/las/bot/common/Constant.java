package com.las.bot.common;


public class Constant {

    public static final int SUCCESS_CODE = 0;
    public static final int FAILED_CODE = 10400;
    public static final int LOGIN_CODE = 10406;
    public static final String SUCCESS = "success";
    public static final String FAILED = "failed";


    /**
     * 阿里云存储所用配置信息
     */
    public static final String HTTPS = "https://";
    public static final String OSS = "oss-accelerate.aliyuncs.com/";
    public static final String BUCKET_NAME = "dullwolf";
    public static final String BUCKET_KEY = "8hEQiz0HZWnGRsF2xTFTI5V0JfvEf6";
    public static final String BUCKET_ID = "LTAI6LEV4LoAvu51";
    public static final String BUCKET_MP3 = ".mp3";
    public static final String BUCKET_PNG = ".png";
    public static final String IMG_PNG = ".png";

    /**
     * 有道云KEY
     */
    public static final String YOUDAO_URL = "https://openapi.youdao.com/api";
    public static final String YOUDAO_TTS_URL = "https://openapi.youdao.com/ttsapi";
    public static final String APP_KEY = "5ce1226e103d03da";
    public static final String APP_SECRET = "wUN7z3uYetLGwzHqSPwfpyqfQJbMSxn4";

    /**
     * 用户标记（针对OSU功能）
     */
    public static final String OSU_USERTAG = "osuTag:";
    public static final String OSU_USERNAME = "osuUserName";

    /**
     * 用户群标记（针对OSU功能）
     */
    public static final String OSU_USEGROUPTAG = "osuGroupTag:";

    /**
     * SETU标记
     */
    public static final String SETU_FREE_TAG = "setuFreeTag:";
    public static final String SETU_FREE_TIME = "setuFreeTime:";


    /**
     * CQ消息类型
     */
    public static final int MESSAGE_TYPE_PRIVATE = 0;
    public static final int MESSAGE_TYPE_GROUP = 1;
    public static final int MESSAGE_TYPE_DISCUSS = 2;

    /**
     * OSU模式名称
     */
    public static final String OSU_MODE_STD = "Std";
    public static final String OSU_MODE_TAIKO = "Taiko";
    public static final String OSU_MODE_CTB = "CtB";
    public static final String OSU_MODE_MANIA = "Mania";
    
    public static final Long MY_TEST_GID = 621259968L;


}
