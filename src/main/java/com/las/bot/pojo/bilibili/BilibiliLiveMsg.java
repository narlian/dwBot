package com.las.bot.pojo.bilibili;

public class BilibiliLiveMsg {

    private Integer code;
    private String message;
    private String msg;
    private BilibiliRoom data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public BilibiliRoom getData() {
        return data;
    }

    public void setData(BilibiliRoom data) {
        this.data = data;
    }
}
