package com.las.bot.pojo.bilibili;

import com.las.bot.pojo.bilibili.room.Room;

import java.util.List;

public class BilibiliRoom {

    private List<Room> room;

    public List<Room> getRoom() {
        return room;
    }

    public void setRoom(List<Room> room) {
        this.room = room;
    }
}
