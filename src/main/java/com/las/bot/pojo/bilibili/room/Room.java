package com.las.bot.pojo.bilibili.room;

public class Room {

    //消息内容
    private String text;
    //用户名
    private String nickname;
    //用户ID
    private Long uid;
    //发弹幕时间
    private String timeline;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getTimeline() {
        return timeline;
    }

    public void setTimeline(String timeline) {
        this.timeline = timeline;
    }

    @Override
    public String toString() {
        return "Room{" +
                "text='" + text + '\'' +
                ", nickname='" + nickname + '\'' +
                ", uid=" + uid +
                ", timeline='" + timeline + '\'' +
                '}';
    }
}
