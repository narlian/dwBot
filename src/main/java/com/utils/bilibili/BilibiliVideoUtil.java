package com.utils.bilibili;

import com.alibaba.fastjson.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class BilibiliVideoUtil {


    // 从B站视频链接中提取BV号
    public static String getBvid(String videoUrl) {
        int startIndex = videoUrl.lastIndexOf('/') + 1;
        int endIndex = videoUrl.lastIndexOf('?');
        if (endIndex == -1) {
            endIndex = videoUrl.length();
        }
        return videoUrl.substring(startIndex, endIndex);
    }

    // 获取API返回的JSON数据
    public static String getJson(String apiUrl) throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(apiUrl);
        URLConnection conn = url.openConnection();
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }
        reader.close();
        return result.toString();
    }

    // 从API返回的JSON数据中提取视频播放量
    public static long getPlayCount(String json) {
        int index = json.indexOf("\"view\":") + "\"view\":".length();
        int endIndex = json.indexOf(",", index);
        return Long.parseLong(json.substring(index, endIndex));
    }

    // 从API返回的JSON数据中提取视频封面URL
    public static String getCoverUrl(String json) {
        int index = json.indexOf("\"pic\":") + "\"pic\":\"".length();
        int endIndex = json.indexOf("\"", index);
        return json.substring(index, endIndex);
    }

    // 从API返回的JSON数据中提取视频点赞数
    public static long getLikeCount(String json) {
        int index = json.indexOf("\"like\":") + "\"like\":".length();
        int endIndex = json.indexOf(",", index);
        return Long.parseLong(json.substring(index, endIndex));
    }

    // 从API返回的JSON数据中提取视频硬币数
    public static long getCoinCount(String json) {
        int index = json.indexOf("\"coin\":") + "\"coin\":".length();
        int endIndex = json.indexOf(",", index);
        return Long.parseLong(json.substring(index, endIndex));
    }

    public static JSONObject getOwner(String json) {
        return JSONObject.parseObject(json).getJSONObject("data").getJSONObject("owner");
    }

    public static String getTitle(String json) {
        int index = json.indexOf("\"title\":") + "\"title\":".length();
        int endIndex = json.indexOf(",", index);
        return json.substring(index, endIndex);
    }

}
