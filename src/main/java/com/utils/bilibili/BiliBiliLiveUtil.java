package com.utils.bilibili;

import com.las.bot.pojo.bilibili.BilibiliLiveMsg;
import com.utils.JsonUtils;
import com.jfinal.kit.HttpKit;

import java.util.HashMap;
import java.util.Map;

public class BiliBiliLiveUtil {

    public static BilibiliLiveMsg getLiveMsg(String roomId,String token) {
        Map<String, String> info = new HashMap<>();
        info.put("roomid", roomId);
        info.put("csrf_token", token);
        info.put("csrf", token);
        String json = HttpKit.post("https://api.live.bilibili.com/ajax/msg", info, null);
        return JsonUtils.getObjectByJson(json, BilibiliLiveMsg.class);
    }

}
