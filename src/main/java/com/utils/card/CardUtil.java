package com.utils.card;

import java.util.*;

public class CardUtil {

    //静态常量
    private static final String[] hs = {"♦", "♣", "♥", "♠"};
    private static final String[] ds = {"3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A", "2"};

    //private static final String[] hs = {"♦", "♣", "♥", "♠"};
    //private static final String[] ds = {"3"};

    //成员变量
    private ArrayList<Integer> play1Num = new ArrayList<>();
    private ArrayList<Integer> play2Num = new ArrayList<>();
    private ArrayList<Integer> play3Num = new ArrayList<>();
    private ArrayList<Integer> diPaiNum = new ArrayList<>();


    private Map<Integer, String> play1 = new LinkedHashMap<>();
    private Map<Integer, String> play2 = new LinkedHashMap<>();
    private Map<Integer, String> play3 = new LinkedHashMap<>();

    private ArrayList<String> card1 = new ArrayList<>();
    private ArrayList<String> card2 = new ArrayList<>();
    private ArrayList<String> card3 = new ArrayList<>();


    public void initCard() {
        dealCards(1);//默认第一个就是地主
        addCard(card1, play1);
        addCard(card2, play2);
        addCard(card3, play3);

    }

    public ArrayList<String> getCard(int host) {
        if (host == 1) {
            return card1;
        } else if (host == 2) {
            return card2;
        } else {
            return card3;
        }
    }

    private void addCard(ArrayList<String> pCardList, Map<Integer, String> playMap) {
        Set<Map.Entry<Integer, String>> set = playMap.entrySet();
        for (Map.Entry<Integer, String> entry : set) {
            //还需要将花色的内容改为空字符串
            String cardStr = entry.getValue().replaceAll("♦", "");
            cardStr = cardStr.replaceAll("♣", "");
            cardStr = cardStr.replaceAll("♥", "");
            cardStr = cardStr.replaceAll("♠", "");
            pCardList.add(cardStr);
        }
    }

    private void dealCards(int host) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < getMap().size(); i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        for (int i = 0; i < list.size(); i++) {
            Integer number = list.get(i);
            if (i >= getMap().size() - 3) {
                diPaiNum.add(number);
            } else {
                if (i % 3 == 0) {
                    play1Num.add(number);
                } else if (i % 3 == 1) {
                    play2Num.add(number);
                } else {
                    play3Num.add(number);
                }
            }
        }
        switch (host) {
            case 1:
                play1Num.addAll(diPaiNum);
                break;
            case 2:
                play2Num.addAll(diPaiNum);
                break;
            case 3:
                play3Num.addAll(diPaiNum);
                break;
        }

        //Collections.sort(diPaiNum);
        Collections.sort(play1Num);
        Collections.sort(play2Num);
        Collections.sort(play3Num);


        Map<Integer, String> cardMap = getMap();

        for (Integer number : play1Num) {
            play1.put(number, cardMap.get(number));
        }

        for (Integer number : play2Num) {
            play2.put(number, cardMap.get(number));
        }

        for (Integer number : play3Num) {
            play3.put(number, cardMap.get(number));
        }


    }

    private Map<Integer, String> getMap() {
        int count = -1;
        Map<Integer, String> map = new HashMap<>();
        map.put(++count, "B");
        map.put(++count, "X");
        for (int i = ds.length - 1; i >= 0; i--) {
            for (int j = 0; j < hs.length; j++) {
                String str = hs[j] + ds[i];
                map.put(++count, str);
            }
        }
        return map;
    }
}
