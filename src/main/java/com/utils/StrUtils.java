package com.utils;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrUtils {


    /**
     * 不是空数据，注意：空格不是空数据
     *
     * @param string
     * @return
     */
    public static boolean isNotEmpty(String string) {
        return string != null && !string.equals("");
    }


    /**
     * 字符串是否匹配某个正则
     *
     * @param string
     * @param regex
     * @return
     */
    public static boolean match(String string, String regex) {
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    /**
     * 这个字符串是否是全是数字
     *
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        if (str == null)
            return false;
        for (int i = str.length(); --i >= 0; ) {
            int chr = str.charAt(i);
            if (chr < 48 || chr > 57)
                return false;
        }
        return true;
    }

    /**
     * 是否是邮件的字符串
     *
     * @param email
     * @return
     */
    public static boolean isEmail(String email) {
        return Pattern.matches("\\w+@(\\w+.)+[a-z]{2,3}", email);
    }


    /**
     * 是否是中国地区手机号码
     *
     * @param phoneNumber
     * @return
     */
    public static boolean isMobileNumber(String phoneNumber) {
        return Pattern.matches("^(1[3,4,5,7,8,9])\\d{9}$", phoneNumber);
    }


    /**
     * 生成一个新的UUID
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 判断是否包含日文
     */
    public static boolean isJapanese(String str) {
        if(!isNotEmpty(str)) return false;
        boolean result = false;
        for (int i = 0; i < str.length(); i++) {
            Character.UnicodeScript ub = Character.UnicodeScript.of(str.charAt(i));
            //日文分平假名和片假名
            if(ub == Character.UnicodeScript.HIRAGANA || ub == Character.UnicodeScript.KATAKANA) {
                result = true;
                break;
            }
        }
        return result;
    }


}
