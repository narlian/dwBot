package com.utils.youdao;

import com.aliyun.oss.OSSClient;
import com.las.bot.common.Constant;
import com.utils.IdUtil;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;


public class TTSUtil {

    private static Logger logger = LoggerFactory.getLogger(TTSUtil.class);


    public static String getUrl(String q, String langType) {
        Map<String,String> params = new HashMap<>();
        String salt = String.valueOf(System.currentTimeMillis());
        params.put("langType", langType);
        String signStr = Constant.APP_KEY + q + salt + Constant.APP_SECRET;
        String sign = getDigest(signStr);
        params.put("appKey", Constant.APP_KEY);
        params.put("q", q);
        params.put("salt", salt);
        params.put("sign", sign);
        try {
            return requestForHttp(params);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String requestForHttp(Map<String, String> params) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(Constant.YOUDAO_TTS_URL);
        List<NameValuePair> paramsList = new ArrayList<>();
        for (Map.Entry<String, String> en : params.entrySet()) {
            String key = en.getKey();
            String value = en.getValue();
            paramsList.add(new BasicNameValuePair(key, value));
        }
        httpPost.setEntity(new UrlEncodedFormEntity(paramsList,"UTF-8"));
        CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
        try{
            Header[] contentType = httpResponse.getHeaders("Content-Type");
            logger.info("Content-Type:" + contentType[0].getValue());
            if("audio/mp3".equals(contentType[0].getValue())){
                //如果响应是wav
                HttpEntity httpEntity = httpResponse.getEntity();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                httpResponse.getEntity().writeTo(baos);
                byte[] result = baos.toByteArray();
                EntityUtils.consume(httpEntity);

                //下一步传送给阿里云存储生成URL链接
                String endpoint = Constant.HTTPS + Constant.OSS;
                String accessKeyId = Constant.BUCKET_ID;
                String accessKeySecret = Constant.BUCKET_KEY;
                String bucketName = Constant.BUCKET_NAME;
                String objectName = IdUtil.nextId() + Constant.BUCKET_MP3;
                OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
                ossClient.putObject(bucketName, objectName, new ByteArrayInputStream(result));
                ossClient.shutdown();
                return Constant.HTTPS + Constant.BUCKET_NAME + "." + Constant.OSS + objectName;
            }else{
                HttpEntity httpEntity = httpResponse.getEntity();
                String json = EntityUtils.toString(httpEntity,"UTF-8");
                EntityUtils.consume(httpEntity);
                logger.info(json);
            }
        }finally {
            try{
                if(httpResponse!=null){
                    httpResponse.close();
                }
            }catch(IOException e){
                logger.info("## release resouce error ##" + e);
            }
        }
        return null;
    }

    /**
     * 生成加密字段
     */
    private static String getDigest(String string) {
        if (string == null) {
            return null;
        }
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        byte[] btInput = string.getBytes(StandardCharsets.UTF_8);
        try {
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            mdInst.update(btInput);
            byte[] md = mdInst.digest();
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (byte byte0 : md) {
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

}