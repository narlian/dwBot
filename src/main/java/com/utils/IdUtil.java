package com.utils;


import com.utils.snowkey.SnowflakeIdWorker;

public class IdUtil {

    /**
     * 获取全局唯一ID
     * @return
     */
    public static String nextId() {
        SnowflakeIdWorker snowflakeIdWorker = SnowflakeIdWorker.getInstance();
        return String.valueOf(snowflakeIdWorker.nextId());
    }


}
