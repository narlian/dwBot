package com.utils.lol;

import com.google.common.collect.Lists;
import com.utils.JsonUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LOLUtils {

    private static final int MAX_TIME = 15000;
    private static String userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36";
    private static String cookie;

    private static class SingletonHolder {
        private static final LOLUtils INSTANCE = new LOLUtils(cookie);

        static void setToken(String t) {
            cookie = t;
        }
    }

    private LOLUtils(String t) {
        cookie = t;
    }

    public static LOLUtils getInstanceByCookie(String cookie) {
        LOLUtils.SingletonHolder.setToken(cookie);
        return LOLUtils.SingletonHolder.INSTANCE;
    }

    private static CloseableHttpClient getClient() {
        RequestConfig defaultRequestConfig = RequestConfig.custom()
                .setSocketTimeout(MAX_TIME)
                .setConnectTimeout(MAX_TIME)
                .setConnectionRequestTimeout(MAX_TIME)
                .build();
        return HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
    }


    /**
     * 搜索LOL玩家信息
     * @param name LOL玩家名
     * @return
     */
    public String searchPlayer(String name) {
        String caller = "wegame.pallas.web.LolBattle";
        String referer = "https://www.wegame.com.cn/helper/lol/search/index.html";
        String url = "https://www.wegame.com.cn/api/v1/wegame.pallas.game.LolBattle/SearchPlayer";
        Map<String,Object> param = new HashMap<>();
        param.put("nickname",name);
        param.put("from_src","lol_helper");
        String jsonText = JsonUtils.getJsonString(param);
        CloseableHttpClient httpClient = getClient();
        HttpPost httpPost = new HttpPost(url);
        CloseableHttpResponse response = null;
        String result = "";
        try {
            StringEntity entity = new StringEntity(jsonText, ContentType.APPLICATION_JSON);
            httpPost.setEntity(entity);
            httpPost.setHeader("cookie",cookie);
            httpPost.setHeader("User-Agent",userAgent);
            httpPost.setHeader("trpc-caller",caller);
            httpPost.setHeader("Referer",referer);
            response = httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                result = EntityUtils.toString(response.getEntity());
            }
        } catch (IOException ignored) {

        } finally {
            try {
                httpPost.releaseConnection();
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public String getUserInfo(String openId,int area) {
        String caller = "wegame.pallas.web.LolBattle";
        String referer = "https://www.wegame.com.cn/helper/lol/search/index.html";
        String url = "https://www.wegame.com.cn/api/v1/wegame.pallas.game.LolBattle/GetBattleReport";
        Map<String,Object> param = new HashMap<>();
        param.put("account_type",2);
        param.put("id",openId);
        param.put("area",area);
        List<Integer> sids = Lists.newArrayList(255);
        param.put("sids",sids);
        param.put("from_src","lol_helper");
        String jsonText = JsonUtils.getJsonString(param);
        CloseableHttpClient httpClient = getClient();
        HttpPost httpPost = new HttpPost(url);
        CloseableHttpResponse response = null;
        String result = "";
        try {
            StringEntity entity = new StringEntity(jsonText, ContentType.APPLICATION_JSON);
            httpPost.setEntity(entity);
            httpPost.setHeader("cookie",cookie);
            httpPost.setHeader("User-Agent",userAgent);
            httpPost.setHeader("trpc-caller",caller);
            httpPost.setHeader("Referer",referer);
            response = httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                result = EntityUtils.toString(response.getEntity());
            }
        } catch (IOException ignored) {

        } finally {
            try {
                httpPost.releaseConnection();
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


}
