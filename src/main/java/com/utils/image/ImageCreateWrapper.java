package com.utils.image;


import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageCreateWrapper {

    public static Builder build() {
        return new Builder();
    }


    public static class Builder {
        private static final int BASE_ADD_H = 400;

        /**
         * 生成的图片创建参数
         */
        private ImgCreateOptions options = new ImgCreateOptions();


        /**
         * 输出的结果
         */
        private BufferedImage result;

        private final int addH = 1000;


        /**
         * 实际填充的内容高度
         */
        private int contentH;


        /**
         * 设置背景图
         *
         * @param bgColor
         * @return
         */
        public Builder setBgColor(Color bgColor) {
            options.setBgColor(bgColor);
            return this;
        }


        public Builder setBgImg(BufferedImage bgImg) {
            options.setBgImg(bgImg);
            return this;
        }


        public Builder setImgW(int w) {
            options.setImgW(w);
            return this;
        }

        public Builder setFont(Font font) {
            options.setFont(font);
            return this;
        }

        public Builder setFontName(String fontName) {
            Font font = options.getFont();
            options.setFont(new Font(fontName, font.getStyle(), font.getSize()));
            return this;
        }

        public Builder setFontColor(Color fontColor) {
            options.setFontColor(fontColor);
            return this;
        }

        public Builder setFontSize(Integer fontSize) {
            Font font = options.getFont();
            options.setFont(new Font(font.getName(), font.getStyle(), fontSize));
            return this;
        }

        public Builder setLeftPadding(int leftPadding) {
            options.setLeftPadding(leftPadding);
            return this;
        }

        public Builder setTopPadding(int topPadding) {
            options.setTopPadding(topPadding);
            contentH = topPadding;
            return this;
        }

        public Builder setBottomPadding(int bottomPadding) {
            options.setBottomPadding(bottomPadding);
            return this;
        }

        public Builder setLinePadding(int linePadding) {
            options.setLinePadding(linePadding);
            return this;
        }

        public Builder setAlignStyle(String style) {
            return setAlignStyle(ImgCreateOptions.AlignStyle.getStyle(style));
        }

        public Builder setAlignStyle(ImgCreateOptions.AlignStyle alignStyle) {
            options.setAlignStyle(alignStyle);
            return this;
        }


        public Builder drawContent(String content) {
            String[] strs = content.split("\\n");
            if (strs.length == 0) { // empty line
                strs = new String[1];
                strs[0] = " ";
            }

            int fontSize = options.getFont().getSize();
            int lineNum = calLineNum(strs, options.getImgW(), options.getLeftPadding(), fontSize);
            // 填写内容需要占用的高度
            int height = lineNum * (fontSize + options.getLinePadding());

            if (result == null) {
                result = GraphicUtil.createImg(options.getImgW(),
                        Math.max(height + options.getTopPadding() + options.getBottomPadding(), BASE_ADD_H),
                        null);
            } else if (result.getHeight() < contentH + height + options.getBottomPadding()) {
                // 超过原来图片高度的上限, 则需要扩充图片长度
                result = GraphicUtil.createImg(options.getImgW(),
                        result.getHeight() + Math.max(height + options.getBottomPadding(), BASE_ADD_H),
                        result);
            }


            // 绘制文字
            Graphics2D g2d = GraphicUtil.getG2d(result);
            int index = 0;
            for (String str : strs) {
                GraphicUtil.drawContent(g2d, str,
                        contentH + (fontSize + options.getLinePadding()) * (++index)
                        , options);
            }
            g2d.dispose();

            contentH += height;
            return this;
        }

        /**
         * 计算总行数
         *
         * @param strs     字符串列表
         * @param w        生成图片的宽
         * @param padding  渲染内容的左右边距
         * @param fontSize 字体大小
         * @return
         */
        private int calLineNum(String[] strs, int w, int padding, int fontSize) {
            // 每行的字符数
            double lineFontLen = Math.floor((w - (padding << 1)) / (double) fontSize);


            int totalLine = 0;
            for (String str : strs) {
                totalLine += Math.ceil(str.length() / lineFontLen);
            }

            return totalLine;
        }




        public BufferedImage asImage() {
            int realH = contentH + options.getBottomPadding();
            BufferedImage bf = new BufferedImage(options.getImgW(), realH, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2d = bf.createGraphics();
            if (options.getBgImg() == null) {
                g2d.fillRect(0, 0, options.getImgW(), realH);
            } else {
                g2d.drawImage(options.getBgImg(), 0, 0, options.getImgW(), realH, null);
            }
            g2d.setBackground(options.getBgColor());
            g2d.drawImage(result, 0, 0, null);
            g2d.dispose();
            return bf;
        }

    }

}
