package com.utils.osu;

import com.jfinal.kit.StrKit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Set;


public class Spider {

    private static String guoHtml(String s) {
        return s.replaceAll("<[.[^<]]*>", "");
    }


    private static BufferedReader getBr(String srcUrl) {
        BufferedReader br = null;
        try {
            URL url = new URL(srcUrl);
            URLConnection urlConn = url.openConnection();
            urlConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            br = new BufferedReader(new InputStreamReader(urlConn.getInputStream(),"UTF-8"));
        } catch (Exception ignored) {
        }
        return br;
    }


    public static String spiderByKey(String srcUrl,Set<String> keys) {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = getBr(srcUrl);
            if(null != br){
                String line;
                int size = keys.size();
                try {
                    while ((line = br.readLine()) != null) {
                        if(0 == size){
                            break;
                        }
                        if(StrKit.isBlank(line)){
                            continue;
                        }
                        for (String key: keys) {
                            if(line.contains(key)){
                                size --;
                                sb.append(guoHtml(line.trim())).append("\n");
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        br.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        } catch (Exception ignored) {

        }
        return sb.toString();
    }
}
