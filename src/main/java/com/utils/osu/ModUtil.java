package com.utils.osu;


import com.las.bot.common.Constant;

import java.util.LinkedHashMap;

public class ModUtil {

    /**
     * 根据mode数值获取音游模式
     *
     * @param mode 模式数值
     * @return
     */
    public static String getModeName(int mode) {
        String modes = "";
        if (0 == mode) {
            modes = Constant.OSU_MODE_STD;
        } else if (1 == mode) {
            modes = Constant.OSU_MODE_TAIKO;
        } else if (2 == mode) {
            modes = Constant.OSU_MODE_CTB;
        } else if (3 == mode) {
            modes = Constant.OSU_MODE_MANIA;
        }
        return modes;
    }

    /**
     * acc准确率优化小数点后两位
     *
     * @param s 准确率字符串
     * @return
     */
    public static String acc(String s) {
        return String.format("%.2f", Double.parseDouble(s));
    }

    /**
     * OSU 中音符300 100 50 的 想加得到总音符值
     *
     * @param t300 300音符
     * @param t100 100音符
     * @param t50  50音符
     * @return
     */
    public static Integer add(String t300, String t100, String t50) {
        return Integer.parseInt(t300) + Integer.parseInt(t100) + Integer.parseInt(t50);
    }

    /**
     * 当排行数值以及分数值过大，采取每三位数隔开逗号。例如：1,000,000
     *
     * @param str1 数值字符串
     * @return
     */
    public static String fen(String str1) {
        str1 = new StringBuilder(str1).reverse().toString();//先将字符串颠倒顺序
        StringBuilder str2 = new StringBuilder();
        for (int i = 0; i < str1.length(); i++) {
            if (i * 3 + 3 > str1.length()) {
                str2.append(str1.substring(i * 3));
                break;
            }
            str2.append(str1, i * 3, i * 3 + 3).append(",");
        }
        if (str2.toString().endsWith(",")) {
            str2 = new StringBuilder(str2.substring(0, str2.length() - 1));
        }
        //最后再将顺序反转过来
        return new StringBuilder(str2.toString()).reverse().toString();
    }

    public static LinkedHashMap<String, String> convertMOD(Integer bp) {
        String modBin = Integer.toBinaryString(bp);
        //反转mod
        modBin = new StringBuffer(modBin).reverse().toString();
        LinkedHashMap<String, String> mods = new LinkedHashMap<>();
        char[] c = modBin.toCharArray();
        if (bp != 0) {
            for (int i = c.length - 1; i >= 0; i--) {
                //字符串中第i个字符是1,意味着第i+1个mod被开启了
                if (c[i] == '1') {
                    switch (i) {
                        case 0:
                            mods.put("NF", "nofail");
                            break;
                        case 1:
                            mods.put("EZ", "easy");
                            break;
                        case 3:
                            mods.put("HD", "hidden");
                            break;
                        case 4:
                            mods.put("HR", "hardrock");
                            break;
                        case 5:
                            mods.put("SD", "suddendeath");
                            break;
                        case 6:
                            mods.put("DT", "doubletime");
                            break;
                        case 8:
                            mods.put("HT", "halftime");
                            break;
                        case 9:
                            mods.put("NC", "nightcore");
                            break;
                        case 10:
                            mods.put("FL", "flashlight");
                            break;
                        case 12:
                            mods.put("SO", "spunout");
                            break;
                        case 14:
                            mods.put("PF", "perfect");
                            break;
                    }
                }
            }
            if (mods.keySet().contains("NC")) {
                mods.remove("DT");
            }
            if (mods.keySet().contains("PF")) {
                mods.remove("SD");
            }
        } else {
            mods.put("None", "None");
        }
        return mods;
    }

}
