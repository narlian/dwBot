package com.utils.osu;

import com.utils.HttpUtils;

public class OsuApi2 {

    private static final String header = "Authorization";
    private static String token;

    private static class SingletonHolder {
        private static final OsuApi2 INSTANCE = new OsuApi2(token);

        static void setToken(String t) {
            token = t;
        }
    }

    private OsuApi2(String t) {
        token = t;
    }

    public static OsuApi2 getInstanceByToken(String token) {
        SingletonHolder.setToken(token);
        return SingletonHolder.INSTANCE;
    }

    public String getMeInfo(String mode) {
        return HttpUtils.doGetByToken("https://osu.ppy.sh/api/v2/me/" + mode, header, token);
    }

    public String getBeatmaps(String filters) {
        return HttpUtils.doGetByToken("https://osu.ppy.sh/api/v2/beatmapsets/search?" + filters, header, token);
    }

    public String getBackgrounds() {
        return HttpUtils.doGetByToken("https://osu.ppy.sh/api/v2/seasonal-backgrounds", header, token);
    }

}
