package com.model;


import com.las.annotation.Column;
import com.las.annotation.Table;

import java.util.Date;

@Table("osu_map")
public class OsuMap {

    /**
     * id
     */
    private Long id;

    /**
     * sid
     */
    private Long sid;

    /**
     * title
     */
    private String title;

    /**
     * artist
     */
    private String artist;

    /**
     * creator
     */
    private String creator;

    /**
     * user_id
     */
    @Column("user_id")
    private Long userId;

    /**
     * bpm
     */
    private Double bpm;

    /**
     * bid
     */
    private Long bid;

    /**
     * mode
     */
    private Integer mode;

    /**
     * difficulty_rating
     */
    @Column("difficulty_rating")
    private Double difficultyRating;

    /**
     * total_length
     */
    @Column("total_length")
    private Integer totalLength;

    /**
     * cs
     */
    private Double cs;

    /**
     * drain
     */
    private Double drain;

    /**
     * accuracy
     */
    private Double accuracy;

    /**
     * ar
     */
    private Double ar;

    /**
     * last_updated
     */
    @Column("last_updated")
    private Date lastUpdated;

    /**
     * status
     */
    private String status;

    /**
     * max_pp
     */
    @Column("max_pp")
    private Double maxPp;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getBpm() {
        return bpm;
    }

    public void setBpm(Double bpm) {
        this.bpm = bpm;
    }

    public Long getBid() {
        return bid;
    }

    public void setBid(Long bid) {
        this.bid = bid;
    }

    public Integer getMode() {
        return mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }

    public Double getDifficultyRating() {
        return difficultyRating;
    }

    public void setDifficultyRating(Double difficultyRating) {
        this.difficultyRating = difficultyRating;
    }

    public Integer getTotalLength() {
        return totalLength;
    }

    public void setTotalLength(Integer totalLength) {
        this.totalLength = totalLength;
    }

    public Double getCs() {
        return cs;
    }

    public void setCs(Double cs) {
        this.cs = cs;
    }

    public Double getDrain() {
        return drain;
    }

    public void setDrain(Double drain) {
        this.drain = drain;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public Double getAr() {
        return ar;
    }

    public void setAr(Double ar) {
        this.ar = ar;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getMaxPp() {
        return maxPp;
    }

    public void setMaxPp(Double maxPp) {
        this.maxPp = maxPp;
    }
}
