package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

import java.util.Date;

@Table("se_reward")
public class SeReward {

    private Long id;

    @Column("user_id")
    private Long userId;

    @Column("img_id")
    private String imgId;

    @Column("se_data")
    private String seData;

    @Column("rarity_data")
    private String rarityData;

    @Column("img_reward")
    private Double imgReward;

    @Column("img_probability")
    private Double imgProbability;

    @Column("create_time")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getImgId() {
        return imgId;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }

    public String getSeData() {
        return seData;
    }

    public void setSeData(String seData) {
        this.seData = seData;
    }

    public String getRarityData() {
        return rarityData;
    }

    public void setRarityData(String rarityData) {
        this.rarityData = rarityData;
    }

    public Double getImgReward() {
        return imgReward;
    }

    public void setImgReward(Double imgReward) {
        this.imgReward = imgReward;
    }

    public Double getImgProbability() {
        return imgProbability;
    }

    public void setImgProbability(Double imgProbability) {
        this.imgProbability = imgProbability;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
