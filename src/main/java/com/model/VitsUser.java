package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("vits_user")
public class VitsUser {

    private Long id;

    @Column("user_id")
    private String userId;

    private String code;

    private String token;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
