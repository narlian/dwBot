package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("cq_msg_url")
public class CqMsgUrl {

    private String id;

    private String url;

    @Column("group_id")
    private Long groupId;

    @Column("user_id")
    private Long userId;

    private String judge;

    @Column("judge_data")
    private String judgeData;

    private Double op;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getJudge() {
        return judge;
    }

    public void setJudge(String judge) {
        this.judge = judge;
    }

    public String getJudgeData() {
        return judgeData;
    }

    public void setJudgeData(String judgeData) {
        this.judgeData = judgeData;
    }

    public Double getOp() {
        return op;
    }

    public void setOp(Double op) {
        this.op = op;
    }
}
