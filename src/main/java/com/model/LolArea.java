package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("lol_area")
public class LolArea {

    private Integer id;

    @Column("area_name")
    private String areaName;

    @Column("area_type")
    private String areaType;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaType() {
        return areaType;
    }

    public void setAreaType(String areaType) {
        this.areaType = areaType;
    }
}
