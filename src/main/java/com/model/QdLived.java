package com.model;

import com.las.annotation.Table;

import java.util.Date;

@Table("qd_lived")
public class QdLived {

    private Long id;

    private Integer num;

    private Date time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
