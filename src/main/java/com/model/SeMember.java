package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

import java.util.Date;

@Table("se_member")
public class SeMember {

    private Long id;

    @Column("user_id")
    private Long userId;

    private Date time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
