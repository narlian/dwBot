package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("wc_group")
public class WcGroup {

    private Integer id;

    @Column("group_name")
    private String groupName;

    @Column("group_all")
    private String groupAll;

    @Column("group_event")
    private String groupEvent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupAll() {
        return groupAll;
    }

    public void setGroupAll(String groupAll) {
        this.groupAll = groupAll;
    }

    public String getGroupEvent() {
        return groupEvent;
    }

    public void setGroupEvent(String groupEvent) {
        this.groupEvent = groupEvent;
    }
}
