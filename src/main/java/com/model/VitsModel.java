package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("vits_model")
public class VitsModel {

    private Long id;

    @Column("model_name")
    private String modelName;

    @Column("model_path")
    private String modelPath;

    @Column("config_path")
    private String configPath;

    private String lang;

    private String roles;

    @Column("one_role")
    private Integer oneRole;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelPath() {
        return modelPath;
    }

    public void setModelPath(String modelPath) {
        this.modelPath = modelPath;
    }

    public String getConfigPath() {
        return configPath;
    }

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Integer getOneRole() {
        return oneRole;
    }

    public void setOneRole(Integer oneRole) {
        this.oneRole = oneRole;
    }
}
