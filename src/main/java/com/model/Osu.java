package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

import java.util.Date;

@Table("osu")
public class Osu {

    /**
     * id
     */
    private Long id;

    /**
     * user_id
     */
    @Column("user_id")
    private Long userId;

    /**
     * username
     */
    private String username;

    /**
     * level
     */
    private Integer level;

    /**
     * pp
     */
    private Double pp;

    /**
     * acc
     */
    private Double acc;

    /**
     * pc
     */
    private Integer pc;

    /**
     * tth
     */
    private Integer tth;

    /**
     * rank
     */
    private Integer rank;

    /**
     * rankscore
     */
    private String rankscore;

    /**
     * mode
     */
    private Integer mode;

    /**
     * create_time
     */
    @Column("create_time")
    private Date createTime;

    /**
     * update_time
     */
    @Column("update_time")
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Double getPp() {
        return pp;
    }

    public void setPp(Double pp) {
        this.pp = pp;
    }

    public Double getAcc() {
        return acc;
    }

    public void setAcc(Double acc) {
        this.acc = acc;
    }

    public Integer getPc() {
        return pc;
    }

    public void setPc(Integer pc) {
        this.pc = pc;
    }

    public Integer getTth() {
        return tth;
    }

    public void setTth(Integer tth) {
        this.tth = tth;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getRankscore() {
        return rankscore;
    }

    public void setRankscore(String rankscore) {
        this.rankscore = rankscore;
    }

    public Integer getMode() {
        return mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
