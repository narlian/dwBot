package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("wc_order")
public class WcOrder {

    private Long id;

    private String event;

    @Column("source_a")
    private String sourceA;

    @Column("source_b")
    private String sourceB;

    @Column("is_end")
    private Integer isEnd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getSourceA() {
        return sourceA;
    }

    public void setSourceA(String sourceA) {
        this.sourceA = sourceA;
    }

    public String getSourceB() {
        return sourceB;
    }

    public void setSourceB(String sourceB) {
        this.sourceB = sourceB;
    }

    public Integer getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(Integer isEnd) {
        this.isEnd = isEnd;
    }
}
