package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("qa_option")
public class QaOption {

    private Long id;

    @Column("qa_id")
    private Long qaId;

    @Column("op_num")
    private Integer opNum;

    @Column("op_title")
    private String opTitle;

    @Column("op_json")
    private String opJson;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQaId() {
        return qaId;
    }

    public void setQaId(Long qaId) {
        this.qaId = qaId;
    }

    public Integer getOpNum() {
        return opNum;
    }

    public void setOpNum(Integer opNum) {
        this.opNum = opNum;
    }

    public String getOpTitle() {
        return opTitle;
    }

    public void setOpTitle(String opTitle) {
        this.opTitle = opTitle;
    }

    public String getOpJson() {
        return opJson;
    }

    public void setOpJson(String opJson) {
        this.opJson = opJson;
    }
}
