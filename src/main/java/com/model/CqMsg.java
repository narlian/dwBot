package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

import java.util.Date;

@Table("cq_msg")
public class CqMsg {

    private Long id;

    private String content;

    @Column("cq_data")
    private String cqData;

    @Column("user_id")
    private Long userId;

    @Column("group_id")
    private Long groupId;

    private Integer type;

    @Column("create_time")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCqData() {
        return cqData;
    }

    public void setCqData(String cqData) {
        this.cqData = cqData;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
