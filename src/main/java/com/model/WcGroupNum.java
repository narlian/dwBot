package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("wc_group_num")
public class WcGroupNum {

    private Integer id;

    @Column("group_name")
    private String groupName;

    @Column("group_all")
    private String groupAll;

    @Column("group_team")
    private String groupTeam;

    @Column("group_num1")
    private Double groupNum1;

    @Column("group_num2")
    private Double groupNum2;

    @Column("group_num3")
    private Double groupNum3;

    @Column("group_num4")
    private Double groupNum4;

    @Column("group_num5")
    private Double groupNum5;

    @Column("group_num6")
    private Double groupNum6;

    @Column("group_num7")
    private Double groupNum7;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupAll() {
        return groupAll;
    }

    public void setGroupAll(String groupAll) {
        this.groupAll = groupAll;
    }

    public String getGroupTeam() {
        return groupTeam;
    }

    public void setGroupTeam(String groupTeam) {
        this.groupTeam = groupTeam;
    }

    public Double getGroupNum1() {
        return groupNum1;
    }

    public void setGroupNum1(Double groupNum1) {
        this.groupNum1 = groupNum1;
    }

    public Double getGroupNum2() {
        return groupNum2;
    }

    public void setGroupNum2(Double groupNum2) {
        this.groupNum2 = groupNum2;
    }

    public Double getGroupNum3() {
        return groupNum3;
    }

    public void setGroupNum3(Double groupNum3) {
        this.groupNum3 = groupNum3;
    }

    public Double getGroupNum4() {
        return groupNum4;
    }

    public void setGroupNum4(Double groupNum4) {
        this.groupNum4 = groupNum4;
    }

    public Double getGroupNum5() {
        return groupNum5;
    }

    public void setGroupNum5(Double groupNum5) {
        this.groupNum5 = groupNum5;
    }

    public Double getGroupNum6() {
        return groupNum6;
    }

    public void setGroupNum6(Double groupNum6) {
        this.groupNum6 = groupNum6;
    }

    public Double getGroupNum7() {
        return groupNum7;
    }

    public void setGroupNum7(Double groupNum7) {
        this.groupNum7 = groupNum7;
    }
}
