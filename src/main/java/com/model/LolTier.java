package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("lol_tier")
public class LolTier {

    private Integer id;

    private Integer tier;

    @Column("tier_name")
    private String tierName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTier() {
        return tier;
    }

    public void setTier(Integer tier) {
        this.tier = tier;
    }

    public String getTierName() {
        return tierName;
    }

    public void setTierName(String tierName) {
        this.tierName = tierName;
    }
}
