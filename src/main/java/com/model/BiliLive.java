package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("bili_live")
public class BiliLive {

    private Long id;

    @Column("live_site")
    private String liveSite;

    private String title;

    @Column("desc_str")
    private String descStr;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLiveSite() {
        return liveSite;
    }

    public void setLiveSite(String liveSite) {
        this.liveSite = liveSite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescStr() {
        return descStr;
    }

    public void setDescStr(String descStr) {
        this.descStr = descStr;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
