package com.model;

import com.las.annotation.Column;
import com.las.annotation.Table;

@Table("group_user")
public class GroupUser {

    private Long id;

    @Column("group_id")
    private Long groupId;

    @Column("user_id")
    private Long userId;

    private String permission;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
