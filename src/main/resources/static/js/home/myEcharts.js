
// 指定图表的配置项和数据
var option = {
    title: {
        text: '90天前排名数据',
        backgroundColor:'#515281',
        textStyle: {
            fontWeight: 'normal',
            color: '#8DB6DB',
        },
    },
    tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    xAxis: {
        type: 'category',
        inverse:true,
        data: ['7天前', '6天前', '5天前', '4天前', '3天前', '2天前', '1天前'],
        axisLine: {
            lineStyle: {
                color: 'rgba(65, 62, 62, 0.5)'
            }
        }

    },
    yAxis: {
        type: 'value',
        inverse:true,//纵坐标数据倒置，因为排名越大是越落后的意思
        axisLabel:{//修改坐标系字体颜色
            show:true,
            textStyle:{
                color:"#8DB6DB"
            }
        },
        splitLine: { // 自定义网格线样式
            lineStyle: {
                type: 'dashed',
                color: 'rgba(65, 62, 62, 0.5)'
            }
        },
        min:10000,
        max:90000
    },
    series: [
        {
            name: 'osu',
            type: 'line',
            symbol: 'none',
            data: [

            ],
            lineStyle: {// 设置线条的style等
                normal: {
                    color: "#db912e", // 折线线条颜色
                },
            }
        },
    ],
};
var xArr = [];
// 读取缓存数据加载option
for (let i = 90; i > 0; i--) {
    if(i-1 === 89){
        xArr.push('现在');
    }else {
        xArr.push((90 - i)+'天前');
    }
}
option.xAxis.data = xArr;


// 指定图表的配置项和数据
var option2 = {
    title: {
        text: '90天前排名数据',
        backgroundColor:'#515281',
        textStyle: {
            fontWeight: 'normal',
            color: '#8DB6DB',
        },
    },
    tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    xAxis: {
        type: 'category',
        inverse:true,
        data: ['7天前', '6天前', '5天前', '4天前', '3天前', '2天前', '1天前'],
        axisLine: {
            lineStyle: {
                color: 'rgba(65, 62, 62, 0.5)'
            }
        }

    },
    yAxis: {
        type: 'value',
        inverse:true,//纵坐标数据倒置，因为排名越大是越落后的意思
        axisLabel:{//修改坐标系字体颜色
            show:true,
            textStyle:{
                color:"#8DB6DB"
            }
        },
        splitLine: { // 自定义网格线样式
            lineStyle: {
                type: 'dashed',
                color: 'rgba(65, 62, 62, 0.5)'
            }
        },
        min:10000,
        max:90000
    },
    series: [
        {
            name: 'taiko',
            type: 'line',
            symbol: 'none',
            data: [

            ],
            lineStyle: {// 设置线条的style等
                normal: {
                    color: "#db912e", // 折线线条颜色
                },
            }
        },
    ],
};
var xArr2 = [];
// 读取缓存数据加载option
for (let i = 90; i > 0; i--) {
    if(i-1 === 89){
        xArr2.push('现在');
    }else {
        xArr2.push((90 - i)+'天前');
    }
}
option2.xAxis.data = xArr2;




// 指定图表的配置项和数据
var option3 = {
    title: {
        text: '90天前排名数据',
        backgroundColor:'#515281',
        textStyle: {
            fontWeight: 'normal',
            color: '#8DB6DB',
        },
    },
    tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    xAxis: {
        type: 'category',
        inverse:true,
        data: ['7天前', '6天前', '5天前', '4天前', '3天前', '2天前', '1天前'],
        axisLine: {
            lineStyle: {
                color: 'rgba(65, 62, 62, 0.5)'
            }
        }

    },
    yAxis: {
        type: 'value',
        inverse:true,//纵坐标数据倒置，因为排名越大是越落后的意思
        axisLabel:{//修改坐标系字体颜色
            show:true,
            textStyle:{
                color:"#8DB6DB"
            }
        },
        splitLine: { // 自定义网格线样式
            lineStyle: {
                type: 'dashed',
                color: 'rgba(65, 62, 62, 0.5)'
            }
        },
        min:10000,
        max:90000
    },
    series: [
        {
            name: 'mania',
            type: 'line',
            symbol: 'none',
            data: [

            ],
            lineStyle: {// 设置线条的style等
                normal: {
                    color: "#db912e", // 折线线条颜色
                },
            }
        },
    ],
};
var xArr3 = [];
// 读取缓存数据加载option
for (let i = 90; i > 0; i--) {
    if(i-1 === 89){
        xArr3.push('现在');
    }else {
        xArr3.push((90 - i)+'天前');
    }
}
option3.xAxis.data = xArr3;



// 指定图表的配置项和数据
var option4 = {
    title: {
        text: '90天前排名数据',
        backgroundColor:'#515281',
        textStyle: {
            fontWeight: 'normal',
            color: '#8DB6DB',
        },
    },
    tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    xAxis: {
        type: 'category',
        inverse:true,
        data: ['7天前', '6天前', '5天前', '4天前', '3天前', '2天前', '1天前'],
        axisLine: {
            lineStyle: {
                color: 'rgba(65, 62, 62, 0.5)'
            }
        }

    },
    yAxis: {
        type: 'value',
        inverse:true,//纵坐标数据倒置，因为排名越大是越落后的意思
        axisLabel:{//修改坐标系字体颜色
            show:true,
            textStyle:{
                color:"#8DB6DB"
            }
        },
        splitLine: { // 自定义网格线样式
            lineStyle: {
                type: 'dashed',
                color: 'rgba(65, 62, 62, 0.5)'
            }
        },
        min:10000,
        max:90000
    },
    series: [
        {
            name: 'ctb',
            type: 'line',
            symbol: 'none',
            data: [

            ],
            lineStyle: {// 设置线条的style等
                normal: {
                    color: "#db912e", // 折线线条颜色
                },
            }
        },
    ],
};
var xArr4 = [];
// 读取缓存数据加载option
for (let i = 90; i > 0; i--) {
    if(i-1 === 89){
        xArr4.push('现在');
    }else {
        xArr4.push((90 - i)+'天前');
    }
}
option4.xAxis.data = xArr4;
