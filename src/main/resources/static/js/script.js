
let modeSwitch = document.querySelector('.mode-switch');
modeSwitch.addEventListener('click', function () {
    document.documentElement.classList.toggle('light');
    modeSwitch.classList.toggle('active');
});

// 定义导航栏菜单的数组
const list = ['/','/search','/osucup','/mio','/shopping','/'];

$(document).ready(function () {
    $("#sidebar-list li").bind('click',function () {
        let i = $(this).index();
        location.href = list[i];
    })
});

