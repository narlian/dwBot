//JS
const container = document.querySelector('.demo4>.container');

(function (doc) {

    var $ = function (el) {
        try {
            var item = doc.querySelectorAll(el);
            return item.length == 1 ? item[0] : item;
        } catch (err) {
            console.log(err)
        }
    }

    var info = console.info;

    var activeIndexEle = $('.active-index');

    var empile = new Empile(container,{
        waitForTransition: true,
            isClickSlide: true,
            navigation: {
                nextEl: $('.demo4 .btn-right'),
                prevEl: $('.demo4 .btn-left'),
            },
            on: {
                //卡片切换时执行
                slideChange: function () {
                    //设置'当前显示的是第[xxx]张'元素的文本内容
                    activeIndexEle.textContent = this.activeIndex + 1;
                },
                transitionEnd: function (empile) {
                    console.info('end')
                },
                transitionStart: function (empile) {
                    console.info('start')
                },
                clickPrevBtn: function (ev) {
                    console.info(ev.target);
                },
                clickNextBtn: function (ev) {
                    console.info(ev)
                },
                clickSlideBtn: function (ev) {
                    console.info(ev);
                }
            },
        autoplay: { 
            delay: 6000, 
            docHiddenOff: { delay: 4000, } 
        }, 
        navigation: {
            nextEl: $('.demo4 .btn-right'),
            prevEl: $('.demo4 .btn-left'),
        },
        css: function (coord, absCoord) {
            var zIndex = 100 - absCoord,
                opacity = Math.pow(.6, absCoord).toFixed(3),
                scale = 'scale(' + Math.pow(.9, absCoord).toFixed(2) + ')',
                translateX = 'translateX(' + 95 * coord + 'px)',
                transformOrigin = 'center bottom';
    
            var transform = [translateX, scale].join(' ');
            return {
                'z-index': zIndex,
                opacity: opacity,
                'transform-origin': transformOrigin,
                transform: transform,
            }
        },
    })


})(document);

$(function () {

    $('#btLeft,#btRight').on('swipeRight',function (ev) {
        console.log(ev);
    })

})