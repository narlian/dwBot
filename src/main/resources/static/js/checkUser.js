var loginErrorMsg = "获取信息失败，请重新刷新or登录";
function checkUser() {
    let tag = false;
    $.ajax({
        type: "GET", //请求方式
        async: false, // fasle表示同步请求，true表示异步请求
        url: "/getUserInfo",//请求地址
        success: function (resp) { //请求成功
            if (null != resp && resp !== '') {
                tag = true;
            }
        },
        error: function (e) {  //请求失败，包含具体的错误信息
            console.log(e.status);
            console.log(e.responseText);
        }
    });
    return tag;

}