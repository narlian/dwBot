﻿// index.js


$(function () {




//十二、MV视频播放器---------------------------------------------------------------------------------------------------
    //数组 保存 MV.url
    var arrSongs = ["http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
        "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
        "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"]

    //爱情大师 - 崔子格       匆匆那年 - 王菲        喜欢你 - 邓紫棋
    $("#mvList li.mv_rcmd").click(function () {
        var _index_songs = $(this).index();
        video[0].src = arrSongs[_index_songs];
        video[0].play();
        $(".playPause").css({"backgroundPosition": "-54px -10px"});
    });
    $("#mv_rank_list ul li").click(function () {
        var index_mv_rcmd2 = $(this).index();
        video[0].src = arrSongs[index_mv_rcmd2];
        video[0].play();
        $(".playPause").css({"backgroundPosition": "-54px -10px"});
    });


    //MV视频播放器
    var video = $("#video");
    //video[0].volume="0.5";
    //播放暂停
    $(".playPause").click(function () {
        if (video[0].paused) {		//video 要带上 [0]
            video[0].play();
            $(this).css({"backgroundPosition": "-54px -10px"});
        } else {
            video[0].pause();
            $(this).css({"backgroundPosition": "-12px -10px"});
        }
        return false;
    });

    //获取总时长 与 获取当前播放时间
    video.on("loadedmetadata", function () {
        var dur = changeTime(video[0].duration);
        $(".duration").text(dur);
    });
    video.on("timeupdate", function () {
        var cur = changeTime(video[0].currentTime);
        $(".current").text(cur);

        var cur_pos = video[0].currentTime; //当前播放时间
        var dur_max = video[0].duration; //总时长，两者相除 求比例
        var scale = 100 * cur_pos / dur_max;
        $(".timeBar").css("width", scale + "%");
    });

    //把获取的时间(单位为毫秒) 转化成秒 方法
    function changeTime(time) {
        iNum = parseInt(time);
        var iM = toZero(Math.floor(time / 60));
        var iS = toZero(Math.floor(time % 60));
        return iM + ":" + iS;
    }

    //补零
    function toZero(num) {
        return num <= 9 ? "0" + num : "" + num;
    }

    //点击或拖动进度条
    var timeDrag = false;
    /* 拖动状态 */
    $(".progressBar").mousedown(function (e) {
        timeDrag = true;
        updatebar(e.pageX);
    });
    $(document).mouseup(function (e) {
        if (timeDrag) {
            timeDrag = false;
            updatebar(e.pageX);
        }
    });
    $(document).mousemove(function (e) {
        if (timeDrag) {
            updatebar(e.pageX);
        }
    });

    //进度条 操作
    var updatebar = function (x) {
        var progress = $(".progressBar");
        var maxduration = video[0].duration; //总时间
        var position = x - progress.offset().left; //点击或拖动的位置
        var percentage = 100 * position / progress.width();

        //限制拖动范围
        if (percentage > 100) {
            percentage = 100;
        }
        if (percentage < 0) {
            percentage = 0;
        }

        //拖动滚动条 currenttime 相应变化
        $(".timeBar").css("width", percentage + "%");
        video[0].currentTime = maxduration * percentage / 100;
    };


    //缓冲加载条
    var startBuffer = function () {
        // var maxduration = video[0].duration;
        // var currentBuffer = video[0].buffered.end(0);
        // var percentage = 100 * currentBuffer / maxduration;
        // $(".bufferBar").css("width", percentage + "%");
        //
        // if (currentBuffer < maxduration) {
        //     setTimeout(startBuffer, 500);
        // }
    };
    setTimeout(startBuffer, 500);


    //静音按钮
    $(".muted").click(function () {
        if (video[0].muted) {
            $(this).css({"backgroundPosition": "-88px -10px"});
            video[0].muted = false;
        } else {
            $(this).css({"backgroundPosition": "-126px -10px"});
            video[0].muted = true;
        }
        return false;
    });

    //音量条 点击或拖动
    var timeDrag2 = false;
    /* 拖动状态 */
    $(".volumeBar").mousedown(function (e) {
        timeDrag2 = true;
        updatebar2(e.pageX);
    });
    $(document).mouseup(function (e) {
        if (timeDrag2) {
            timeDrag2 = false;
            updatebar2(e.pageX);
        }
    });
    $(document).mousemove(function (e) {
        if (timeDrag2) {
            updatebar2(e.pageX);
        }
    });

    //音量条 操作
    var updatebar2 = function (x) {
        var progress = $(".volumeBar");
        var maxduration = video[0].duration; //总时间
        var position = x - progress.offset().left; //点击或拖动的位置
        var percentage = 100 * position / progress.width();

        //限制范围
        if (percentage > 100) {
            percentage = 100;
        }
        if (percentage < 0) {
            percentage = 0;
        }

        //拖动滚动条 currenttime 相应变化
        $(".volume").css("width", percentage + "%");
        video[0].volume = percentage / 100;
    };

    //全屏
    $(".fullscreen").on("click", function () {
        //兼容写法
        video[0].RequestFullScreen = video[0].RequestFullScreen || video[0].webkitRequestFullScreen || video[0].mozRequestFullScreen;
        //调用全屏方法
        video[0].RequestFullScreen();
        return false;
    });
});