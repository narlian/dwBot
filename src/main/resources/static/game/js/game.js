var aTime;
// 音乐声量默认设置50%
var x = 50;
// 滚动条变化次数需要的变量
var c = 0;
// 作谱器移动轨道px值
var pl = 0;
// 总音符数
var totalsp = -1;

window.onload = function () {
    // 防止有延迟，页面加载完毕事先加载音乐
    getBpm();
    getMusic(true);
};

document.getElementById("test").onclick = function () {
    alert("初始化需要耐心等待哦！");
    var totalsd = parseInt(music.duration);
    console.log('总时间：' + totalsd);
    totalsp = parseInt(totalsd / aTime);
    console.log('总音符：' + totalsp);
    // 临时测试
    //totalsp = 200;
    if (totalsp > 0) {
        var song = document.getElementById("song");
        song.innerHTML = "<p class=\"line lineE\"></p>\n" +
            "        <p class=\"line lineE\"></p>\n" +
            "        <p class=\"line lineE\"></p>\n" +
            "        <p class=\"line lineE\"></p>";

        var song2 = document.getElementById("song2");
        song2.innerHTML = "<p class=\"line lineE\"></p>\n" +
            "        <p class=\"line lineE\"></p>\n" +
            "        <p class=\"line lineE\"></p>\n" +
            "        <p class=\"line lineE\"></p>";
        for (let i = 0; i < totalsp; i++) {
            var z = (Math.random() > 0.5) ? 1 : 0;
            if (z === 0) {
                song.innerHTML += "<p class=\"line lineL\"></p>";
                song2.innerHTML += "<p class=\"line lineL\"></p>";
            } else {
                song.innerHTML += "<p class=\"line lineR\"></p>";
                song2.innerHTML += "<p class=\"line lineR\"></p>";
            }
        }
        // 之后拼接空白line
        for (let j = 0; j < 4; j++) {
            song.innerHTML += "<p class=\"line lineS\"></p>";
        }

        var p = document.getElementsByClassName("line");
        for (var i = 0; i < p.length; i++) {
            p[i].style.animationDuration = aTime + 's';
        }
        // 之所以+8 是因为前4s节拍需要休息 + 最后4s ending音符需要休息
        pl = ((parseInt(totalsp) + 8) * 2) + 1;


    }
    totalP(totalsp)
};

function totalP(totalsp) {
    // 计算所有song音符点
    var p = document.getElementById("song2").childNodes;
    if (totalsp === -1) {
        pl = p.length + 8;
        console.log("init -> " + pl);
    } else {
        console.log("init2 -> " + pl);
    }
    var tw = (pl - 1) / 2 * 52;
    var song = document.getElementById("song2");
    var fp = ((song.children.length + 4) * 2) + 1;
    //console.log(pl + " -- " + fp);
    if (fp === pl) {
        document.getElementById("song").style = 'width: ' + tw + 'px;';
    } else {
        console.log(pl + " --> " + song.children.length);
    }
    document.getElementById('songDD').scrollLeft = (((pl * 26) - tw) * c) * 2;
    c++;
    pl -= 1;
}

function removeP() {
    var song = document.getElementById("song2");
    //console.log(song.children.length);
    if (song.children.length === 0) {
        console.log("游戏结束");
        return;
    }
    //console.log("移除时间和动画时间一致？" + aTime);
    var gd = document.getElementsByClassName("gd");
    var child = gd[0].children;
    gd[0].removeChild(child[3]);

    //之后将隐藏域的最后一个P添加轨道上
    var lastP = song.lastElementChild;
    song.removeChild(lastP);
    var gdDiv = document.getElementById("gd");
    var oldHtml = gdDiv.innerHTML;
    gdDiv.innerHTML = '';
    gdDiv.appendChild(lastP);
    gdDiv.innerHTML += oldHtml;
    totalP(totalsp);
}


function getBpm() {
    var bpm = document.getElementById("bpm").innerText;
    var bp = parseInt(bpm);
    aTime = 60000 / bp / 1000 / 3;
    //document.getElementById("dTime").innerText = aTime;
    var p = document.getElementsByClassName("line");
    for (var i = 0; i < p.length; i++) {
        p[i].style.animationDuration = aTime + 's';
    }
}

document.getElementById("startTime").onclick = function () {
    //getMusic(false);
    music.volume = parseFloat(x / 200) * 1;
    music.play();
    //之后需要移除P标签以及新增P标签
    setInterval("removeP()", (aTime * 1000));
    //setInterval("removeP2()", (aTime * 1000));
};

Array.prototype.remove = function (val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};


var music = document.getElementById("mymusic");
var musicindex = 0;//音乐索引
var musics = ["audio.ogg", "test2.mp3", "test3.mp3"];//音乐数组

//读取音乐
function getMusic(flag) {
    music.src = "music/" + musics[musicindex];//改变音乐的SRC
    music.load();
    if (flag) {
        //alert("111");
        music.oncanplay = function () {
            totalP(totalsp);
        }
    }

}