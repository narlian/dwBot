package com.las.bot;

import cn.hutool.core.collection.CollectionUtil;
import com.google.common.collect.Lists;
import com.las.bot.dto.WcOrderDTO;
import com.utils.worldcup.WorldCupOneUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestWorldCup2 {

    public static double[] scoreA = {2.13, 6.0};
    public static double[] scoreB = {26, 8.8, 3.3};
    public static String[] scoreBStr = {"2-2", "1-1", "0-0"};

    public static void main(String[] args) {
        List<WcOrderDTO> orderList = Lists.newArrayList();
        int size = 10;
        for (int i = 1; i <= size; i++) {
            for (int j = 1; j <= size; j++) {
                for (int k = 1; k <= size; k++) {
                    Map<String, Object> order = WorldCupOneUtils.getInstance().getOrder(i, j, k, scoreA, scoreB, scoreBStr);
                    double totalCost = Double.parseDouble(order.get("cost").toString()) * 3;
                    double wCost = Double.parseDouble(order.get("wCost").toString());
                    double dCost = Double.parseDouble(order.get("dCost").toString());
                    double lCost = Double.parseDouble(order.get("lCost").toString());
                    double fx = (totalCost - (wCost + dCost + lCost)) / 3;
                    double mid = wCost < lCost ? wCost : lCost;
                    double last = mid < dCost ? mid : dCost;
                    double mfx = ((totalCost * 1.0) / 3) - last;
                    WcOrderDTO wcOrderDTO = new WcOrderDTO();
                    wcOrderDTO.setKey(i + "-" + j + "-" + k);
                    wcOrderDTO.setMaxFx(mfx);
                    wcOrderDTO.setFx(fx);
                    orderList.add(wcOrderDTO);
                }
            }
        }
        List<WcOrderDTO> collect = orderList.stream().filter(wcOrderDTO -> (wcOrderDTO.getFx() < 0)).sorted((o1, o2) -> {
            int v1 = (int)(o1.getMaxFx() * 100.0);
            int v2 = (int)(o2.getMaxFx() * 100.0);
            return v1 - v2;
        }).collect(Collectors.toList());
        if (CollectionUtil.isNotEmpty(collect)) {
            WcOrderDTO wcOrderDTO = collect.get(0);
            String key = wcOrderDTO.getKey();
            String[] sp = key.split("-");
            int win = Integer.parseInt(sp[0]) * 100;
            int lost = Integer.parseInt(sp[1]) * 100;
            int pp = Integer.parseInt(sp[2]) * 100;
            Map<String, Object> orderMap = WorldCupOneUtils.getInstance().getOrder(win, lost, pp, scoreA, scoreB, scoreBStr);
            System.out.println(orderMap.get("result"));
            Double maxFx = wcOrderDTO.getMaxFx() * 100;
            System.out.println("最大亏损：" + String.format("%.2f", maxFx) + "元");
        } else {
            System.out.println("不存在赢钱方案");
        }


    }


}
