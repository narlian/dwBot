package com.las.bot;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Demo2 {

    public static void main(String[] args) throws Exception {
        InputStream is = Demo2.class.getClassLoader().getResourceAsStream("static/color.txt");
        System.out.println(is);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while (null != (line = br.readLine())) {
            System.out.println("颜色代码：" + line);
            if (line.startsWith("#")) {
                String hex = line.trim();
                int[] rgb = hex2RGB(hex);
                System.out.println(rgb[0] + "-" + rgb[1] + "-" + rgb[2]);
                String filePath = "D:/work_test/testcolor/" + hex + ".png";
                pureColorPictures(50, 50, rgb[0], rgb[1], rgb[2], filePath);
            }
        }
    }

    //生成颜色底图
    private static void pureColorPictures(int width, int height, int red, int green, int blue, String filePath) throws Exception {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        //取得图形
        Graphics g = img.getGraphics();
        //设置颜色
        g.setColor(new Color(red, green, blue));
        //填充
        g.fillRect(0, 0, img.getWidth(), img.getHeight());
        //在d盘创建个文件
        File file = new File(filePath);//以png方式写入，可改成jpg其他图片
        ImageIO.write(img, "png", file);
    }


    /**
     * 16进制颜色字符串转换成rgb
     *
     * @param hexStr
     * @return rgb
     */
    public static int[] hex2RGB(String hexStr) {
        if (hexStr != null && !"".equals(hexStr) && hexStr.length() == 7) {
            int[] rgb = new int[3];
            rgb[0] = Integer.valueOf(hexStr.substring(1, 3), 16);
            rgb[1] = Integer.valueOf(hexStr.substring(3, 5), 16);
            rgb[2] = Integer.valueOf(hexStr.substring(5, 7), 16);
            return rgb;
        }
        return null;
    }

}
