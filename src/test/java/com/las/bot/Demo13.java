package com.las.bot;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.las.bot.common.Constant;
import com.las.utils.mirai.CmdUtil;
import com.utils.DownFileUtils;
import com.utils.FileBase64Utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import static com.las.bot.common.Constant.BUCKET_MP3;

public class Demo13 {

    public static void main(String[] args) throws Exception {
        String fId = getAvUrl("[JA]バレンタインデーおめでとうございます、会長[JA]");
        if (StrKit.notBlank(fId)) {
            String result = HttpUtil.post("http://vits.natapp1.cc/sovits/getBase64ById?fId=" + fId, (String) null);
            JSONObject object = JSONObject.parseObject(result);
            File file = new File(StrUtil.uuid());
            FileBase64Utils.decodeBase64File(object.getString("data"),file.getAbsolutePath());
            String url = DownFileUtils.upFileAndDel(file,BUCKET_MP3);
            System.out.println(url);
        }
    }

    public static String getAvUrl(String text) throws Exception {
        String result = "";
        Socket socket = new Socket("e36ff7293101f071.natapp.cc", 12468);
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
        JSONObject object = new JSONObject();
        object.put("model", "./model/roni/G_latest.pth");
        object.put("config", "./model/roni/config.json");
        object.put("index", "0");
        String encode = Base64.encode(text.getBytes(StandardCharsets.UTF_8));
        object.put("text", encode);
        dos.writeUTF("#" + object.toJSONString());
        DataInputStream dis = new DataInputStream(socket.getInputStream());
        byte[] bs = new byte[10240];
        int len = dis.read(bs);
        String str = new String(bs, 0, len);
        socket.close();
        if (StrUtil.isNotBlank(str)) {
            return str;
        }
        return result;
    }

}
