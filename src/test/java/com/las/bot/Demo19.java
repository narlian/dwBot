package com.las.bot;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Demo19 {

    public static void main(String[] args) {
        String pattern = "video\\/([\\w]+)";
        String text = "你看下这个视频 https://www.bilibili.com/video/BV1914y1U7iJ/?vd_source=1eba640e72f6ac79339c4ca9c70d3671 超好看。";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(text);
        if (m.find()) {
            String videoId = m.group(1);
            System.out.println("Q蒂派啾咪~，视频的ID是：" + videoId);
        } else {
            System.out.println("Q蒂派啾咪~，未找到视频ID。");
        }
    }

}
