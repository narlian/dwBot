package com.las.bot;


import cn.hutool.core.collection.CollectionUtil;
import com.google.common.collect.Lists;
import com.model.WcOrder;
import com.utils.DateUtils;
import com.utils.JsonUtils;
import com.utils.SpiderHtml;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.net.URL;
import java.util.Date;
import java.util.List;

public class Demo10 {

    public static void main(String[] args) throws Exception {
        int index = 2;
        String ymd = DateUtils.formatYMD(new Date());
        String url = "https://trade.500.com/jczq/?playid=354&g=2&vtype=nspf&date=" + ymd;
        Document document = Jsoup.parse(new URL(url), 30000);
        List<Element> list = document.getElementsByClass("betbtn-row itm-rangB1");
        List<Element> teamList = document.getElementsByClass("team");
        Element team1 = teamList.get(index + 1);
        String team1Str = SpiderHtml.guoHtml(team1.text()).trim();
        Element team2 = teamList.get(index + 2);
        String team2Str = SpiderHtml.guoHtml(team2.text()).trim();
        List<String> teamNames = Lists.newArrayList();
        if (team1Str.contains("VS") && team2Str.contains("VS")) {
            String[] tsp = team1Str.split("VS");
            String s1 = tsp[0].trim().replaceAll("\\d+", "").replaceAll("\\[", "").replaceAll("]", "");
            String s2 = tsp[1].trim().replaceAll("\\d+", "").replaceAll("\\[", "").replaceAll("]", "");
            teamNames.add(s1);
            teamNames.add(s2);

            String[] tsp2 = team2Str.split("VS");
            String s3 = tsp2[0].trim().replaceAll("\\d+", "").replaceAll("\\[", "").replaceAll("]", "");
            String s4 = tsp2[1].trim().replaceAll("\\d+", "").replaceAll("\\[", "").replaceAll("]", "");
            teamNames.add(s3);
            teamNames.add(s4);
        }

        String teamEvent = "";
        if (CollectionUtil.isNotEmpty(teamNames)) {
            Element element1 = list.get(index);
            String c1 = SpiderHtml.guoHtml(element1.text());
            String[] sp1 = c1.split(" ");
            double v1 = Double.parseDouble(sp1[0]);
            double max1 = 0.0;
            double min1 = 0.0;
            double mid1 = 0.0;
            if (sp1.length == 3) {
                double v2 = Double.parseDouble(sp1[2]);
                max1 = v1 > v2 ? v1 : v2;
                min1 = v1 < v2 ? v1 : v2;
                mid1 = Double.parseDouble(sp1[1]);
                if (v1 > v2) {
                    //team1的名字 1 和 2 对调
                    teamEvent = teamNames.get(1) + "VS" + teamNames.get(0) + "#";
                } else {
                    teamEvent = teamNames.get(0) + "VS" + teamNames.get(1) + "#";
                }
            }

            Element element2 = list.get(index + 1);
            String c2 = SpiderHtml.guoHtml(element2.text());
            String[] sp2 = c2.split(" ");
            double vv1 = Double.parseDouble(sp2[0]);
            double max2 = 0.0;
            double min2 = 0.0;
            double mid2 = 0.0;
            if (sp2.length == 3) {
                double vv2 = Double.parseDouble(sp2[2]);
                max2 = vv1 > vv2 ? vv1 : vv2;
                min2 = vv1 < vv2 ? vv1 : vv2;
                mid2 = Double.parseDouble(sp2[1]);
                if (vv1 > vv2) {
                    //team2的名字 对调
                    teamEvent += teamNames.get(3) + "VS" + teamNames.get(2);
                } else {
                    teamEvent += teamNames.get(2) + "VS" + teamNames.get(3);
                }
            }
            if (max1 > 0 && max2 > 0) {
                String sourceA = min1 + "," + max1 + "," + min2 + "," + max2;
                String sourceB = mid1 + "," + mid1 + "," + mid2 + "," + mid2;
                WcOrder wcOrder = new WcOrder();
                wcOrder.setId(null);
                wcOrder.setSourceA(sourceA);
                wcOrder.setSourceB(sourceB);
                wcOrder.setEvent(teamEvent);
                System.out.println(JsonUtils.getJsonString(wcOrder));
            }
        }


    }
}
