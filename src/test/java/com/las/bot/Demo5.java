package com.las.bot;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.las.bot.cmd.admin.AdvCloseCmd;
import com.utils.JsonUtils;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class Demo5 {

    private static Logger logger = Logger.getLogger(Demo5.class);

    public static void main(String[] args) {
        Map<String, String> info = new HashMap<>();
        info.put("grant_type", "client_credentials");
        info.put("client_id", "Pp81KdwxQxkCwYH6Sy4zjyUa");
        info.put("client_secret", "3nxhba0OWrpHsLOPkXbRWNIQAcf0uKbN");
        String result = HttpKit.post("https://aip.baidubce.com/oauth/2.0/token", info, null);
        JSONObject object = JsonUtils.getJsonObjectByJsonString(result);
        String token = object.getString("access_token");

        info = new HashMap<>();
        info.put("access_token", token);
        info.put("text", "加我");
        String result2 = HttpKit.post("https://aip.baidubce.com/rest/2.0/solution/v1/text_censor/v2/user_defined", info, null);
        logger.info("广告屏蔽判定结果：" + result2);
        JSONObject object2 = JsonUtils.getJsonObjectByJsonString(result2);
        String judge = object2.getString("conclusion");

        String msg = "判定是：" + judge;
        Double v = null;
        boolean tag = false;
        if ("不合规".equals(judge) || "疑似".equals(judge)) {
            JSONArray jsonArray = object2.getJSONArray("data");
            v = jsonArray.getJSONObject(0).getJSONArray("hits").getJSONObject(0).getDouble("probability");
            String data = String.format("%.2f", v * 100) + "%";
            msg = msg + "\n违规原因：" + object2.getJSONArray("data").getJSONObject(0).getString("msg");
            msg = msg + "\n鉴定值：" + data;
        }
        logger.info("判定结果是：" + msg);

        if ("不合规".equals(judge) || "疑似".equals(judge)) {
            String config = "0.7#是";
            String[] split = config.split("#");
            double limit = Double.parseDouble(split[0]);
            if (split[1].equals("是")) {
                tag = true;
            }
            if (null != v && v > limit) {
                logger.info("sendJudge90Msg");
                //sendJudge90Msg(-1, msg, userId, id, tag);
            } else {
                // 0.2是定制的，不许改
                if (null != v && v > 0.2) {
                    logger.info("sendJudge20Msg");
                    //sendJudge20Msg(msg, userId, id);
                }
            }
        }
    }

}
