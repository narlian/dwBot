package com.las.bot;

import com.las.bot.cmd.other.SeLeGaoCmd;
import com.utils.legao.Mode;
import com.utils.legao.MosaicMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Clanner on 2018/10/6.
 */
public class Demo {

    private static Logger logger = LoggerFactory.getLogger(Demo.class);

    public static void main(String[] args) {

        String path = System.getProperty("user.dir");
        System.out.println(path + File.separator + "709293432");
        String sourcePath = path + File.separator + "709293432";

        URL url = SeLeGaoCmd.class.getClassLoader().getResource("static/selg");
        assert url != null;
        File[] arr = new File(url.getPath()).listFiles();
        if (arr != null && arr.length > 0) {
            File sourceFile = arr[4];
            logger.info("读取源文件：" + sourceFile.getAbsolutePath());
            int rate = 100;
            String outPath = "D:\\work_test\\YUI-tree-gray-5.png";
            MosaicMaker mosaicMaker = new MosaicMaker(sourcePath, sourceFile.getAbsolutePath(), outPath, rate);
            mosaicMaker.setMode(Mode.RGB);
            try {
                mosaicMaker.make();
                logger.info(mosaicMaker.getDefaultW() + " - " + mosaicMaker.getDefaultH());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
