package com.las.bot;

import com.google.common.collect.Lists;
import com.utils.bfs.Graph;

import java.text.DecimalFormat;
import java.util.*;

public class TestWorldCup {

    /**
     * A组
     */
    private static String group = "A";

    /**
     * A组队伍让球指数 "卡塔尔0.95", "塞内加尔0.92", "荷兰0.90", "厄瓜多尔 0.87"
     * A组队伍进球指数 "卡塔尔0.97", "塞内加尔1.01", "荷兰0.79", "厄瓜多尔 0.83"
     * A组队伍欧洲指数 "卡塔尔3.48", "塞内加尔5.46", "荷兰1.62", "厄瓜多尔 2.07"
     * A组队伍亚洲指数 "卡塔尔1.12", "塞内加尔1.04", "荷兰0.82", "厄瓜多尔 0.75"
     */
    private static List<Double> groupANum1 = Lists.newArrayList(0.95, 0.92, 0.90, 0.87);
    private static List<Double> groupANum2 = Lists.newArrayList(0.97, 1.01, 0.79, 0.83);
    private static List<Double> groupANum3 = Lists.newArrayList(3.48, 5.46, 1.62, 2.07);
    private static List<Double> groupANum4 = Lists.newArrayList(1.12, 1.04, 0.82, 0.75);


    /**
     * A组队伍
     */
    private static List<String> groupA = Lists.newArrayList("卡塔尔", "塞内加尔", "荷兰", "厄瓜多尔");

    /**
     * A组成绩
     */
    private static double[] scoreA;

    /**
     * 所有选项集合
     */
    private static LinkedList<String[]> list = new LinkedList<>();

    /**
     * 每个路线对应事件
     */
    private static Map<String, String> map = new HashMap<>();

    /**
     * 事件标签
     */
    private static String[] eventA = new String[]{"卡塔尔VS厄瓜多尔", "塞内加尔VS荷兰", "卡塔尔VS塞内加尔", "荷兰VS厄瓜多尔", "荷兰VS卡塔尔", "厄瓜多尔VS塞内加尔"};

    /**
     * 每个队伍胜利事件
     */
    private static Map<String, Integer> winMap = new HashMap<>();


    public static void main(String[] args) {
        List<String> allLine = new ArrayList<>();
        //初始化根节点
        Graph theGraph = new Graph();
        String[] opArr = new String[]{"#"};
        list.add(opArr);
        //初始化事件分支
        for (int i = 0; i < eventA.length; i++) {
            String label = eventA[i];
            cg(label, (i + 1), group);
        }
        //根节点以及中间节点
        for (int i = 0; i < list.size(); i++) {
            if (i + 1 == list.size()) {
                break;
            }
            String[] a1 = list.get(i);
            String[] a2 = list.get(i + 1);
            addEdge(theGraph, a1, a2);
        }
        String[] last = list.getLast();
        //叶子节点
        for (String aLast : last) {
            theGraph.addVertex(aLast);
        }

        StringBuilder sb = new StringBuilder();
        List<List<String>> allPathList = theGraph.mst();
        int total = 0;
        for (List<String> onePath : allPathList) {
            if (sb.length() > 0) {
                sb.delete(0, sb.length());
            }
            for (String node : onePath) {
                sb.append(node).append(",");
            }
            String options = sb.substring(2, sb.length() - 1);
            allLine.add(options);
            total += 1;
        }
        for (String line : allLine) {
            // 先给成绩初始化
            scoreA = new double[4];
            String[] all = line.split(",");
            for (String cg : all) {
                String end = map.get(cg);
                if (end.endsWith("平")) {
                    //两支队伍各加+1分
                    String[] op = end.replace("平", "").split("VS");
                    scoreA[groupA.indexOf(op[0].trim())] += 1;
                    scoreA[groupA.indexOf(op[1].trim())] += 1;
                } else if (end.endsWith("主胜")) {
                    //给主胜队伍加+3分
                    String[] op = end.replace("主胜", "").split("VS");
                    scoreA[groupA.indexOf(op[0].trim())] += 3;
                } else if (end.endsWith("客胜")) {
                    //给客胜队伍加+3分
                    String[] op = end.replace("客胜", "").split("VS");
                    scoreA[groupA.indexOf(op[1].trim())] += 3;
                }
            }
            // 得到整数型分数后需要综合让球指数和进球指数算出最新分数
            for (int i = 0; i < scoreA.length; i++) {
                double v = scoreA[i];
                v *= (groupANum2.get(i) + groupANum1.get(i) + groupANum3.get(i) + groupANum4.get(i));
                scoreA[i] = v;
            }
            List<Integer> topTow = findTopTow(scoreA);
            String f1 = groupA.get(topTow.get(0));
            String f2 = groupA.get(topTow.get(1));
            totalWin(f1, winMap);
            totalWin(f2, winMap);
            //System.out.println(line + "-->" + Arrays.toString(scoreA) + "-->" + f1 + "、" + f2 + "获胜");
        }
        //System.out.println("总共多少个？" + total);
        System.out.println(map);
        System.out.println(winMap);
        int finalTotal = total;
        winMap.forEach((k, v) -> {
            double p = (v * 100.0) / finalTotal;
            DecimalFormat df = new DecimalFormat(".00");
            String winP = df.format(p);
            System.out.println(k + "获胜概率：" + winP + "%");
        });


    }

    /**
     * 统计某支队伍胜利次数
     */
    private static void totalWin(String f1, Map<String, Integer> winMap) {
        if (winMap.containsKey(f1)) {
            Integer count = winMap.get(f1);
            winMap.put(f1, ++count);
        } else {
            winMap.put(f1, 1);
        }
    }

    /**
     * 计算最高分的前两组队伍
     */
    private static List<Integer> findTopTow(double[] scoreA) {
        double maxNumber = Double.MIN_VALUE;
        double secMax = Double.MAX_VALUE;
        for (double aScoreA : scoreA) {
            if (aScoreA > maxNumber) {
                secMax = maxNumber;
                maxNumber = aScoreA;

            } else {
                if (aScoreA > secMax) {
                    secMax = aScoreA;

                }
            }
        }
        List<Double> allScore = Lists.newArrayList();
        for (double s : scoreA) {
            allScore.add(s);
        }
        int fi = allScore.indexOf(maxNumber);
        int si = allScore.indexOf(secMax);
        if (si == fi) {
            allScore.set(fi, -1.0);
        }
        return Lists.newArrayList(fi, allScore.indexOf(secMax));
    }

    /**
     * 添加子节点
     */
    private static void addEdge(Graph theGraph, String[] a1, String[] a2) {
        for (String root : a1) {
            //添加叶子根
            theGraph.addVertex(root);
            //再给叶子根添加子节点
            for (String anA2 : a2) {
                theGraph.addEdge(root, anA2);
            }

        }
    }

    /**
     * 分支事件
     */
    private static void cg(String label, int count, String group) {
        String g1 = group + "W" + count;
        String g2 = group + "P" + count;
        String g3 = group + "S" + count;
        map.put(g1, label + " 主胜");
        map.put(g2, label + " 平");
        map.put(g3, label + " 客胜");
        String[] opArr;
        List<String> opCount = Lists.newArrayList(g1, g2, g3);
        opArr = new String[opCount.size()];
        opArr = opCount.toArray(opArr);
        list.add(opArr);
    }


}
