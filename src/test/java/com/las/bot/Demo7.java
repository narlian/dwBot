package com.las.bot;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.utils.JsonUtils;
import com.utils.StrUtils;
import com.utils.lol.LOLUtils;

public class Demo7 {

    private static String[] tierStr = {"Ⅰ", "Ⅱ", "Ⅲ", "Ⅳ"};

    public static void main(String[] args) {
        String userName = "奶片酱";
        String cookie = "app_env=prod; app_id=10001; app_version=607000; platform=qq; pgv_pvid=2368061076; ts_uid=4088978826; tgp_env=online; ssr=0; pgv_info=ssid=s5983442349; uin=o2547170055; skey=@d37SEu2B2; p_uin=o2547170055; tgp_user_type=0; client_type=1; region=CN; pt4_token=agU3UfnoPuxbwU686TrbP7cN*iMwBBPP*UVulXnLsbA_; p_skey=IWYXe7NHS96yJ6S6GwG0VaorY7BTIuYNbLIe*i4bUQc_; tgp_id=278161555; tgp_ticket=1CCDD5B2D3E1E7B84FC16896CE50DDCA3E15E659AA5BDE21F70803C61D1E3D6507EFD7B720CA221F765B39CEF776B9878817C192538A33E84A8B0BB11DEB2549B7485F6FBFFD1D57ABC305F60FAA2C44C509B620EFBF9A8CEE09C1B22E596B7E2491A61C67020E848E28327F4CDB4EC09457BDC0F93A38E2BC1A0B5E8CEC9792; tgp_biz_ticket=01000000000000000000b7f444ba6cbf49a2831eeb53a6265a6f7219b40296cac81ac65454596655111eda39d8ae8c3ff73f8ac1326bd70e184ecd32c7d0993f559d35d9183c88e3c4; ts_last=www.wegame.com.cn/helper/lol/search/index.html";
        String result = LOLUtils.getInstanceByCookie(cookie).searchPlayer(userName);
        System.out.println(result);
        JSONObject obj = JsonUtils.getJsonObjectByJsonString(result);
        JSONArray players = obj.getJSONArray("players");
        StringBuilder sb = new StringBuilder();
        if (null != players) {
            sb.append("玩家：").append(userName).append("\n");
            int max = 0;
            JSONObject maxObj = null;
            for (int i = 0; i < players.size(); i++) {
                JSONObject player = players.getJSONObject(i);
                int level = player.getIntValue("level");
                if (level > max) {
                    max = level;
                    maxObj = player;
                }
            }
            if (ObjectUtil.isNotNull(maxObj)) {
                String openid = maxObj.getString("openid");
                int area = maxObj.getIntValue("area");
                int level = maxObj.getIntValue("level");
                //LolArea lolArea = areaDao.findById(area);
                //sb.append("大区：").append(lolArea.getAreaName()).append("\n");
                sb.append("等级：").append(level).append("\n");

                String json = LOLUtils.getInstanceByCookie(cookie).getUserInfo(openid, area);
                JSONObject infoObj = JsonUtils.getJsonObjectByJsonString(json);
                if (ObjectUtil.isNotNull(infoObj.getJSONObject("battle_count"))) {
                    JSONObject battle = infoObj.getJSONObject("battle_count");
                    sb.append("匹配对局(胜/负)：").append(battle.getIntValue("total_match_wins")).append("/").append(battle.getIntValue("total_match_losts")).append("\n");
                    sb.append("乱斗对局(胜/负)：").append(battle.getIntValue("total_arm_wins")).append("/").append(battle.getIntValue("total_arm_losts")).append("\n");
                    JSONArray seasonList = infoObj.getJSONArray("season_list");
                    if (CollectionUtil.isNotEmpty(seasonList)) {
                        JSONObject season = seasonList.getJSONObject(0);

                        String tierShow;
                        tierShow = "单双段位" + tierStr[season.getIntValue("queue")];

                        String teamShow;
                        teamShow = "灵活段位" + tierStr[season.getIntValue("team_queue")];
                        sb.append("单双排位(胜/负)：").append(season.getIntValue("wins")).append("/").append(season.getIntValue("losses")).append("\n");
                        sb.append("单双段位：").append(tierShow).append("、胜点：").append(season.getIntValue("win_point")).append("点\n");
                        sb.append("灵活排位(胜/负)：").append(season.getIntValue("team_wins")).append("/").append(season.getIntValue("team_losses")).append("\n");
                        sb.append("灵活段位：").append(teamShow).append("、胜点：").append(season.getIntValue("team_win_point")).append("点\n");

                    }
                }

            }
        }
        if (StrUtils.isNotEmpty(sb.toString())) {
            System.out.println(sb.toString());
        }
    }


}
