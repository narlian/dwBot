package com.las.bot;

import com.google.common.collect.Lists;
import com.utils.worldcup.WorldCupUtils;

import java.util.List;

public class Demo6 {

    public static void main(String[] args) {
        List<String> groupA = Lists.newArrayList("荷兰", "卡塔尔", "厄瓜多尔", "塞内加尔");
        Object[] eventA = new Object[]{"卡塔尔VS厄瓜多尔", "塞内加尔VS荷兰", "卡塔尔VS塞内加尔", "荷兰VS厄瓜多尔", "卡塔尔VS荷兰", "厄瓜多尔VS塞内加尔"};
        List<Double> groupANum1 = Lists.newArrayList(0.95, 0.92, 0.90, 0.87);
        String str = WorldCupUtils.getInstance().getByGroup("队 进16强", "A", groupA, eventA, groupANum1);
        System.out.println(str);
    }


}
