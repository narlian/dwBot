package com.las.bot;

import com.utils.DateUtils;
import com.utils.MailUtil;

import java.math.BigDecimal;
import java.util.Date;

public class Demo9 {

    public static void main(String[] args) {
        String firstWeekByNowTime = DateUtils.getFirstWeekByNowTime();
        Date firstWeekDate = DateUtils.formatToDate(DateUtils.HOUR_24,firstWeekByNowTime);
        Date lastWeekDate = DateUtils.addDay(firstWeekDate, 7);
        String startTime = DateUtils.formatYMD(firstWeekDate) + " 00:00:00";
        String endTime = DateUtils.formatYMD(lastWeekDate) + " 00:00:00";
        System.out.println(startTime + " -- " + endTime);
    }

}
