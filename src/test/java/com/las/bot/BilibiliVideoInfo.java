package com.las.bot;

import com.alibaba.fastjson.JSONObject;
import com.utils.bilibili.BilibiliVideoUtil;

import java.io.IOException;

public class BilibiliVideoInfo {

    public static void main(String[] args) throws IOException {
        String videoUrl = "https://www.bilibili.com/video/BV1914y1U7iJ"; // B站视频链接
        String apiUrl = "https://api.bilibili.com/x/web-interface/view?bvid=" + BilibiliVideoUtil.getBvid(videoUrl); // API接口链接
        String json = BilibiliVideoUtil.getJson(apiUrl); // 获取API返回的JSON数据
        long playCount = BilibiliVideoUtil.getPlayCount(json); // 获取视频播放量
        String coverUrl = BilibiliVideoUtil.getCoverUrl(json); // 获取视频封面URL
        String title = BilibiliVideoUtil.getTitle(json);
        long likeCount = BilibiliVideoUtil.getLikeCount(json); // 获取视频点赞数
        long coinCount = BilibiliVideoUtil.getCoinCount(json);
        System.out.println("封面URL是：" + coverUrl);
        System.out.println("标题是：" + title);
        System.out.println("播放量是：" + playCount);
        System.out.println("点赞数是：" + likeCount);
        System.out.println("硬币数是：" + coinCount);
        JSONObject owner = BilibiliVideoUtil.getOwner(json);
        String author = owner.getString("name");
        System.out.println("作者是：" + author);
    }


}
