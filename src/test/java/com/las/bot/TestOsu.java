package com.las.bot;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.las.bot.dto.OsuMapDTO;
import com.model.OsuMap;
import com.utils.DateUtils;
import com.utils.JsonUtils;
import com.utils.osu.ApiUtil;
import com.utils.osu.Spider;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestOsu {

    public static void main(String[] args) {
        OsuMapDTO osuMapDTO = getMapByBid(24023L);
        System.out.println(JsonUtils.getJsonString(osuMapDTO));
    }

    private static OsuMapDTO getMapByBid(Long bid) {
        Set<String> keys = new HashSet<>();
        keys.add("difficulty_rating");
        String info = Spider.spiderByKey("https://osu.ppy.sh/b/" + bid, keys);
        if (StrKit.notBlank(info)) {
            String jsonString = JsonUtils.getJsonString(JsonUtils.convert(info));
            OsuMap map = JsonUtils.getObjectByJson(jsonString, OsuMap.class);
            if (map.getStatus().contains("rank")) {
                map.setSid(map.getId());
                map.setId(bid);
                List<JSONObject> beatmaps = JsonUtils.getArrayByJson(JsonUtils.getJsonObjectByJsonString(info).getString("beatmaps"), JSONObject.class);
                if (CollectionUtil.isNotEmpty(beatmaps)) {
                    for (JSONObject object : beatmaps) {
                        if (bid.equals(object.getLong("id"))) {
                            OsuMapDTO osuMap = JsonUtils.getObjectByJson(JsonUtils.getJsonString(map), OsuMapDTO.class);
                            osuMap.setBid(object.getLong("id"));
                            osuMap.setMode(object.getInteger("mode_int"));
                            osuMap.setDifficultyRating(object.getDouble("difficulty_rating"));
                            osuMap.setTotalLength(object.getInteger("total_length"));
                            osuMap.setCs(object.getDouble("cs"));
                            osuMap.setDrain(object.getDouble("drain"));
                            osuMap.setAccuracy(object.getDouble("accuracy"));
                            osuMap.setAr(object.getDouble("ar"));
                            String[] split = object.getString("last_updated").split("T");
                            Date date = DateUtils.formatToDate(DateUtils.DAY, split[0]);
                            osuMap.setLastUpdated(date);
                            osuMap.setStatus(object.getString("status"));
                            //再加一个新字段，版本名
                            osuMap.setVersion(object.getString("version"));
                            return osuMap;
                        }
                    }
                }
            }
        }
        return null;
    }

}
