package com.las.bot;

import cn.hutool.core.collection.CollectionUtil;
import com.utils.DateUtils;
import com.utils.JsoupUtil;
import com.las.utils.StrUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Demo3 {

    private static Logger logger = LoggerFactory.getLogger(Demo3.class);

    public static void main(String[] args) throws Exception {
        String ymd = DateUtils.formatYMD(new Date());
        int index = ymd.indexOf("-") + 1;
        String md = ymd.substring(index);
        String url = "https://www.776dm.com/";
        Document document = Jsoup.parse(new URL(url), 30000);
        Element list = document.getElementById("slider");
        Elements li = list.getElementsByTag("li");
        List<Element> todayList = new ArrayList<>();
        for (Element element : li) {
            String font = element.getElementsByTag("font").text();
            font = fillDay(font);
            String mdDay = fillDay(md);
            if (StrUtils.isNotBlank(mdDay) && mdDay.equals(font)) {
                todayList.add(element);
            }
        }
        if (CollectionUtil.isEmpty(todayList)) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        int n = -1;
        if (1 > 0) {
            String number = "1";
            if (StrUtils.isNumeric(number)) {
                n = Integer.parseInt(number);
            }
            if (n <= 0 || n > todayList.size()) {
                return;
            }
            Element element = todayList.get(n - 1);
            try {
                String href = element.getElementsByTag("a").attr("href");
                String downUrl = JsoupUtil.spader776ByNumber(href);
                downUrl = downUrl.replaceAll("下载", "集\n种子");
                sb.append(downUrl).append("\n");
            } catch (Exception ignored) {
            }
        } else {
            sb.append(md).append("今日番剧有：\n");
            for (int i = 0; i < todayList.size(); i++) {
                Element element = todayList.get(i);
                String name = element.getElementsByTag("a").text();
                name = name.replaceAll("下载", "");
                sb.append("番剧").append(i + 1).append("：").append(name).append("\n");
            }
            sb.append("\n输入指令：#今日番剧 1 即可获取编号1的新番种子链接");
        }
        logger.info(sb.toString());


    }

    /**
     * 爬取776动漫的日期是 8-1 需要补充0
     *
     * @param font 8-1
     * @return 08-01
     */
    private static String fillDay(String font) {
        if(StrUtils.isBlank(font)){
            return null;
        }
        String md = null;
        try {
            String[] split = font.split("-");
            int m = Integer.parseInt(split[0]);
            int d = Integer.parseInt(split[1]);
            md = String.format("%02d", m) + "-" + String.format("%02d", d);
        } catch (Exception e) {
            logger.error("转换日期出错：" + e.getMessage());
        }
        return md;
    }

}
