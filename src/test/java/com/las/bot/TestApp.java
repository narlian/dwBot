package com.las.bot;

import com.las.utils.StrUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dullwolf
 */
public class TestApp {

    private static Logger logger = LoggerFactory.getLogger(TestApp.class);

    public static void main(String[] args) {
        String fillDay = fillDay("7-29");
        System.out.println(fillDay);
    }

    /**
     * 爬取776动漫的日期是 8-1 需要补充0
     *
     * @param font 8-1
     * @return 08-01
     */
    private static String fillDay(String font) {
        if(StrUtils.isBlank(font)){
            return null;
        }
        String md = null;
        try {
            String[] split = font.split("-");
            int m = Integer.parseInt(split[0]);
            int d = Integer.parseInt(split[1]);
            md = String.format("%02d", m) + "-" + String.format("%02d", d);
        } catch (Exception e) {
            logger.error("转换日期出错：" + e.getMessage());
        }
        return md;
    }
}
