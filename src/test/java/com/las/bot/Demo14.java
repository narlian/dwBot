package com.las.bot;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.JsonKit;
import com.las.utils.JsonUtils;

import java.util.HashMap;
import java.util.Map;

public class Demo14 {

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();
        Map<String, String> textMap = new HashMap<>();
        textMap.put("text", "hello");
        //图灵接口url
        String url = "http://openapi.turingapi.com/openapi/api/v2";
        try {
            JSONObject userObj = new JSONObject();
            userObj.put("apiKey", "97155e3a22694c549cfbbf3e7f0866c3");
            userObj.put("userId", "-1");
            map.put("userInfo", userObj);
            JSONObject obj = new JSONObject();
            obj.put("inputText", textMap);
            map.put("perception", obj);
            map.put("reqType", 0);
            //调用图灵接口
            String result = HttpKit.post(url, JSONObject.toJSONString(map), null);
            System.out.println(result);
            //推送给前端的消息
            JSONObject resultObJ = JsonUtils.getObjectByJson(result, JSONObject.class);
            JSONObject textObj = resultObJ.getJSONArray("results").getJSONObject(0);
            String info = textObj.getJSONObject("values").getString("text");
            System.out.println(info);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
