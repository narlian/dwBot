package com.las.bot;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;
import com.utils.JsonUtils;

import java.util.HashMap;
import java.util.Map;

public class Demo17 {

    public static void main(String[] args) {
        Map<String,Object> params = new HashMap<>();
        params.put("uId","2547170055");
        params.put("token","8caa39ee-1677-48ad-b770-cc5c73df3a7f");
        params.put("mId","20");
        params.put("rId","0");
        params.put("ar","0.5");
        params.put("domin","0.5");
        params.put("va","0.5");
        params.put("text","[ZH]你好[ZH]");

        String result = HttpKit.post("http://vits.natapp1.cc/sovits/genByText", JsonUtils.getJsonString(params));
        if(StrKit.notBlank(result)){
            JSONObject obj = JsonUtils.getJsonObjectByJsonString(result);
            if(obj.getIntValue("code") == 0){
                String url = obj.getString("data");
                System.out.println(url);
            }

        }
    }

}
