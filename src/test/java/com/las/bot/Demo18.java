package com.las.bot;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.utils.JsonUtils;

public class Demo18 {

    public static void main(String[] args) {
        String backMsg = "";
        String roomId = "455899334";
        String rId = "f35f86f3feba61dfbcd67a060acc93ee";
        String wts = "1686060399";
        String apiUrl = "https://api.bilibili.com/x/space/wbi/acc/info?mid="+roomId+"&token=&platform=web&web_location=1550101&w_rid="+rId+"&wts="+wts;
        String result = HttpKit.get(apiUrl);
        JSONObject roomObj = JsonUtils.getJsonObjectByJsonString(result);
        String maxNum = "";
        if(roomObj.getIntValue("code") == 0){
            JSONObject data = roomObj.getJSONObject("data");
            System.out.println(data);
            String name = data.getString("name");
            JSONObject liveRoom = data.getJSONObject("live_room");
            maxNum = liveRoom.getJSONObject("watched_show").getString("text_large");
            int liveStatus = liveRoom.getIntValue("liveStatus");
            if(liveStatus == 0){
                backMsg = name + "当前还未开播~";
            } else {
                StringBuilder sb = new StringBuilder();
                String url = liveRoom.getString("url");
                String title = liveRoom.getString("title");
                String cover = liveRoom.getString("cover");
                sb.append(name).append("开播啦！").append("\n");
                sb.append("直播间：").append(url).append("\n");
                sb.append("标题：").append(title).append("\n");
                sb.append("封面：").append(cover).append("\n");
                backMsg = sb.toString();
            }

        }
        System.out.println(backMsg);
        System.out.println(maxNum);

    }


}
