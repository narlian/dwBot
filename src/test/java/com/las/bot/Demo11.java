package com.las.bot;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class Demo11 {

    public static void main(String[] args) throws Exception {
        String searchName = "别当欧尼酱";
        String url = "https://www.36dm.club/search.php?keyword=";
        Document document = Jsoup.parse(new URL(url+ URLEncoder.encode(searchName,"UTF-8")), 30000);
        Elements list = document.getElementsByClass("alt1");
        List<Element> todayList = new ArrayList<>();
        for (Element element : list) {
            System.out.println(element);
        }
    }

}
