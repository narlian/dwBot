package com.las.bot;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Sets;
import com.jfinal.kit.StrKit;
import com.las.utils.HttpUtils;
import com.las.utils.StrUtils;
import com.las.utils.mirai.CmdUtil;
import com.utils.DateUtils;
import com.utils.FileBase64Utils;
import com.utils.JsonUtils;
import com.utils.SpiderHtml;

import java.util.Date;
import java.util.Set;

public class Demo16 {

    public static void main(String[] args) throws Exception {
        StringBuilder sb = new StringBuilder();
        String url = "https://www.yxdmgo.com/";
        Set<String> keys = Sets.newHashSet();
        keys.add("今天");
        // 顺便把昨天的也列在里面
        Date now = new Date();
        Date date = DateUtils.addDay(now, -1);
        String formatYMD = DateUtils.formatYMD(date);
        String[] dsp = formatYMD.split("-");
        String day = fillDay(dsp[1] + "-" + dsp[2]);
        keys.add(day);
        String info = SpiderHtml.spiderByKeyWithHtml(url, keys);
        String[] split = info.split("\n");
        for (String str : split) {
            String[] ems = str.split("<a");
            if (ems.length > 0) {
                String name = SpiderHtml.guoHtml(ems[ems.length - 1]);
                String title = name.split("href=")[0];
                String newStr = title.replaceAll("title=", "").replaceAll("target=\"_blank","");
                if(StrKit.isBlank(newStr)){
                    continue;
                }
                title = newStr.substring(1, newStr.length() - 2);
                sb.append(title).append("\n");
            }
        }
        if (StrUtils.isNotBlank(sb.toString())) {
            System.out.println(sb.toString());
        }


    }

    /**
     * 爬取776动漫的日期是 8-1 需要补充0
     *
     * @param font 8-1
     * @return 08-01
     */
    private static String fillDay(String font) {
        if (StrUtils.isBlank(font)) {
            return null;
        }
        String md = null;
        try {
            String[] split = font.split("-");
            int m = Integer.parseInt(split[0]);
            int d = Integer.parseInt(split[1]);
            md = String.format("%02d", m) + "-" + String.format("%02d", d);
        } catch (Exception e) {
        }
        return md;
    }

}
