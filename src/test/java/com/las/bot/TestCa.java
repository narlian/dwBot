package com.las.bot;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.utils.SpiderHtml;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestCa {

    public static void main(String[] args) {
        String search = "202301";

        if (StrKit.notBlank(search)) {
            Set<String> keys = new HashSet<>();
            keys.add(search.trim());
            keys.add("img width=\"180px\"");
            keys.add("title_cn");
            keys.add("type_a");//原创动画
            keys.add("type_b_");//漫画改编
            keys.add("type_c_");//小说改编
            keys.add("type_d_");//游戏改编
            keys.add("type_e_");//衍生动画
            keys.add("class=\"staff_r\"");
            keys.add("<br>");
            keys.add("class=\"cast_r\"");
            keys.add("PV");
            String info = SpiderHtml.spiderByKeyWithHtml("https://yuc.wiki/" + search.trim(), keys);
            //System.out.println(info);
            Set<String> caName = new HashSet<>();
            String[] split = info.split("\\n");
            boolean isAppend = false;
            StringBuilder staff = null;
            int count = 0;
            List<JSONObject> list = new ArrayList<>();
            JSONObject obj = new JSONObject();
            for (String str : split) {
                if (isAppend) {
                    if (str.contains("class=\"cast\"")) {
                        staff.append(str);
                        staff.append("【声优】<br>");
                    } else if (str.contains("动画官网")) {
                        isAppend = false;
                    } else {
                        staff.append(str);
                    }
                } else {
                    if (str.startsWith("<div style=\"float:left\"><img width=\"180px\"") && !caName.contains(str)) {
                        String banner = str.split("src")[1].substring(2);
                        String bannerUrl = banner.split("jpg")[0];
                        obj.put("poster",bannerUrl + "jpg");
                        count++;
                    }
                    if (str.contains("title_cn") && !caName.contains(str)) {
                        obj.put("name",SpiderHtml.guoHtml(str));
                        count++;
                    }
                    if ((str.contains("type_a") || str.contains("type_b_")
                            || str.contains("type_c_") || str.contains("type_d_")
                            || str.contains("type_e_")) && !caName.contains(str)) {
                        String[] tag = SpiderHtml.guoHtml(str).split("动画");
                        obj.put("type",tag[0] + "动画");
                        if (tag.length > 1) {
                            obj.put("tag",tag[1]);
                        }
                        count++;

                    }
                    if (str.startsWith("<tr><td rowspan=\"2\" class=\"staff_r\">")) {
                        staff = new StringBuilder();
                        isAppend = true;
                    }
                    if (null != staff && staff.length() > 0) {
                        obj.put("staff","<tr><td rowspan=\"2\" class=\"staff\">" + staff.toString() + "...<br></td></tr>");
                        staff.delete(0, staff.length());
                        count++;
                    }
                    if (str.contains("PV")) {
                        obj.put("pv",str);
                        count++;
                    }
                    caName.add(str);
                    if (count == 5) {
                        list.add(obj);
                        count = 0;
                        caName.clear();
                        obj = new JSONObject();
                    }
                }
            }
            System.out.println(list);
        }
    }
}
